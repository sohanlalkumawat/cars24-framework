package com.cars24.core.framework.lambda.utils;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.MediaType;

import com.amazonaws.serverless.proxy.model.AwsProxyRequest;
import com.cars24.core.framework.lambda.constants.HttpHeaders;
import com.cars24.core.framework.lambda.constants.LambdaConstants;

public class LambdaUtils {

	private LambdaUtils() {
	}

	public static Map<String, String> getCORSHeaders(AwsProxyRequest request) {
		Map<String, String> headers = getDefaultHeaders();
		headers.put(HttpHeaders.ACCESS_CONTROL_ALLOW_HEADERS, request.getHeaders().get(HttpHeaders.ACCESS_CONTROL_REQUEST_HEADERS));
		headers.put(HttpHeaders.ACCESS_CONTROL_ALLOW_METHODS, "OPTIONS," + request.getHeaders().get(HttpHeaders.ACCESS_CONTROL_REQUEST_METHOD));
		headers.put(HttpHeaders.ALLOW, LambdaConstants.ANY_METHOD);
		headers.put(HttpHeaders.ACCESS_CONTROL_MAX_AGE, LambdaConstants.DEFAULT_AGE);
		headers.put(HttpHeaders.CONTENT_LENGTH, "0");
		return headers;
	}

	public static Map<String, String> getExposeHeaders() {
		Map<String, String> headers = new HashMap<>();
		headers.put(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, LambdaConstants.EXPOSE_HEADERS);
		return headers;
	}

	public static Map<String, String> getDefaultHeaders() {
		Map<String, String> headers = new HashMap<>();
		headers.put(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE);
		headers.put(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, LambdaConstants.ALL_ORIGIN);
		headers.put(HttpHeaders.VARY, LambdaConstants.VARY_HEADER);
		headers.putAll(getExposeHeaders());
		return headers;
	}
}
