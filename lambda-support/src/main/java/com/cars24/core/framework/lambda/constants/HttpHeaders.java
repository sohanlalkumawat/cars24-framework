package com.cars24.core.framework.lambda.constants;

public interface HttpHeaders extends javax.ws.rs.core.HttpHeaders {

	String ACCESS_CONTROL_ALLOW_HEADERS = "access-control-allow-headers";
	String ACCESS_CONTROL_ALLOW_METHODS = "access-control-allow-methods";
	String ALLOW = "allow";
	String ACCESS_CONTROL_MAX_AGE = "access-control-max-age";
	String ACCESS_CONTROL_EXPOSE_HEADERS = "access-control-expose-headers";
	String ACCESS_CONTROL_ALLOW_ORIGIN = "access-control-allow-origin";
	String VARY = "vary";

	String ACCESS_CONTROL_REQUEST_HEADERS = "access-control-request-headers";
	String ACCESS_CONTROL_REQUEST_METHOD = "access-control-request-method";

}
