package com.cars24.core.framework.lambda.handlers;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import com.amazonaws.serverless.proxy.model.AwsProxyRequest;
import com.amazonaws.serverless.proxy.model.AwsProxyResponse;
import com.cars24.core.framework.common.bean.ErrorBean;
import com.cars24.core.framework.common.bean.ErrorResponse;
import com.cars24.core.framework.common.constants.ApplicationProfiles;
import com.cars24.core.framework.common.constants.CommonConstants;
import com.cars24.core.framework.common.constants.CommonConstants.DeploymentType;
import com.cars24.core.framework.lambda.constants.LambdaConstants;
import com.cars24.core.framework.utility.JSONUtils;
import com.cars24.core.framework.utility.StringUtils;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

 public abstract class BaseLambdaHandler {

	private static final Logger _BASE_LOGGER = LoggerFactory.getLogger(BaseLambdaHandler.class);
	public static final ObjectMapper MAPPER = new ObjectMapper();
	public static final ObjectWriter _RESPONSE_WRITER = new ObjectMapper().writerFor(AwsProxyResponse.class);

	public static void setUpLambdaEnvironment() {

		_BASE_LOGGER.info("******************  Initializing Lambda Container ******************");

		String profile = System.getenv(LambdaConstants.LAMBDA_STAGE_NAME);
		String lambdaName = System.getenv(LambdaConstants.LAMBDA_NAME);
		String timezone = System.getenv(LambdaConstants.LAMBDA_TIME_ZONE);

		if (StringUtils.isEmpty(profile)) {
			profile = System.getProperty(LambdaConstants.LAMBDA_STAGE_NAME);
		}

		timezone = StringUtils.isNotBlank(timezone) ? timezone : CommonConstants.TZ_ASIA_CALCUTTA;
		TimeZone.setDefault(TimeZone.getTimeZone(timezone));
		_BASE_LOGGER
				.debug("******************  Initializing Lambda : ON TIMEZONE [ " + timezone + " ] ******************");

		/**
		 * Fallback on test if no profile found
		 */
		if (StringUtils.isEmpty(profile)) {
			profile = ApplicationProfiles.TEST;
		}

		_BASE_LOGGER
				.debug("******************  Initializing Spring : ON PROFILE [ " + profile + " ] ******************");

		createSystemProperties(profile, lambdaName);
	}

	public static void createSystemProperties(String profile, String lambdaName) {
		System.setProperty(CommonConstants.DEPLOYMENT_TYPE_KEY, DeploymentType.LAMBDA.toString());
		System.setProperty(CommonConstants.SPRING_PROFILE_ACTIVE_KEY, profile);
		System.setProperty(CommonConstants.LAMBDA_ID, (profile.equalsIgnoreCase(ApplicationProfiles.TEST)
				|| profile.equalsIgnoreCase(ApplicationProfiles.LOCAL)) ? "test_event_lambda" : lambdaName);
		_BASE_LOGGER.info(System.getProperty(CommonConstants.LAMBDA_ID) + " - Lambda Started on Profile : " + profile);
	}

	public boolean isApiCall(AwsProxyRequest proxyRequest) {
		if (proxyRequest == null || StringUtils.isBlank(proxyRequest.getPath())
				|| StringUtils.isBlank(proxyRequest.getHttpMethod())) {
			return false;
		}
		return true;
	}

	public void sendErrorResponse(OutputStream outputStream, Exception e)
			throws IOException, JsonGenerationException, JsonMappingException {
		AwsProxyResponse response = new AwsProxyResponse();
		response.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
		String errorMessage = "Internal Server Error (" + e.getMessage() + ")";
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setErrors(Arrays.asList(ErrorBean.withError("SERVER_ERROR", errorMessage, "")));
		errorResponse.setMessage(errorMessage);
		response.setBody(JSONUtils.objectToJson(errorResponse));
		_RESPONSE_WRITER.writeValue(outputStream, response);
	}
}
