package com.cars24.core.framework.lambda.handlers;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.serverless.exceptions.ContainerInitializationException;
import com.amazonaws.serverless.proxy.model.AwsProxyRequest;
import com.amazonaws.serverless.proxy.model.AwsProxyResponse;
import com.amazonaws.serverless.proxy.spring.SpringLambdaContainerHandler;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestStreamHandler;
import com.cars24.core.framework.common.exception.ConfigurationException;
import com.cars24.core.framework.common.utils.ContextUtils;
import com.cars24.core.framework.lambda.config.LambdaServletConfig;

/**
 * Supports Spring Component Scan - Features Available will be - Context,
 * Configuration, Cache, JDBC, WebMVC and Scanned beans
 * 
 * @author Sohan
 *
 */
public abstract class StreamLambdaHandler extends BaseLambdaHandler implements RequestStreamHandler {

	private static final Logger _LOGGER = LoggerFactory.getLogger(StreamLambdaHandler.class);
	public static SpringLambdaContainerHandler<AwsProxyRequest, AwsProxyResponse> handler;

	@Override
	public void handleRequest(InputStream inputStream, OutputStream outputStream, Context context) throws IOException {
		_LOGGER.info(" StreamLambdaHandler Request Initiated");
		try {
			initializeSpring();
			handler.proxyStream(inputStream, outputStream, context);
		} catch (Exception e) {
			_LOGGER.warn("Error Processing API Request : " + e.getMessage());
			super.sendErrorResponse(outputStream, e);
		}
	}

	private static void initializeSpring() {
		if (handler == null) {
			try {
				setUpLambdaEnvironment();
				handler = SpringLambdaContainerHandler.getAwsProxyHandler(ContextUtils.class,
						LambdaServletConfig.class);
				_LOGGER.debug("Spring Initialization Complete");
			} catch (ContainerInitializationException e) {
				_LOGGER.error(" Spring Initialization Error : " + e.getMessage(), e);
				throw new ConfigurationException("Could not initialize Spring framework", e);
			}
		}
	}
}