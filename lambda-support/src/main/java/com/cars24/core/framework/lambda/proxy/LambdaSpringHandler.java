package com.cars24.core.framework.lambda.proxy;

import java.beans.Expression;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.annotation.PostConstruct;
import javax.validation.Valid;
import javax.ws.rs.core.HttpHeaders;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.validation.beanvalidation.SpringValidatorAdapter;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.MediaTypeNotSupportedStatusException;

import com.amazonaws.serverless.proxy.model.AwsProxyRequest;
import com.amazonaws.serverless.proxy.model.AwsProxyResponse;
import com.cars24.core.framework.common.bean.ErrorBean;
import com.cars24.core.framework.common.bean.Errors;
import com.cars24.core.framework.common.exception.NoDataException;
import com.cars24.core.framework.common.exception.ServerException;
import com.cars24.core.framework.common.utils.CollectionUtils;
import com.cars24.core.framework.common.utils.ContextUtils;
import com.cars24.core.framework.lambda.proxy.interceptor.LambdaInterceptorConfigurer;
import com.cars24.core.framework.lambda.utils.LambdaUtils;
import com.cars24.core.framework.utility.StringUtils;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class LambdaSpringHandler {

	public static final ObjectMapper MAPPER = new ObjectMapper();
	private static final Logger _LOGGER = LoggerFactory.getLogger(LambdaSpringHandler.class);
	private static Object[] VALID = new Object[] { null };
	public static final String SPRING_PROXY_KEY = "lambda.spring.proxy";

	private Map<String, Object> controllers = new HashMap<>();
	private Map<String, LambdaHandlerConfig> handlerConfig = new HashMap<>();
	private SpringValidatorAdapter validatorAdaptor = null;

	@Autowired
	private LambdaInterceptorConfigurer interceptorConfigurer;

	@PostConstruct
	public void init() {
		if (System.getProperty(SPRING_PROXY_KEY) != null) {
			controllers = ContextUtils.getContext().getBeansWithAnnotation(RestController.class);
			_LOGGER.info("Mapped Controllers.");
			_LOGGER.info("" + controllers.keySet());
			createHandlerConfig();
		}
	}

	public void handleRequest(AwsProxyRequest request, AwsProxyResponse response) {

		if (request.getHttpMethod().equalsIgnoreCase(org.springframework.http.HttpMethod.OPTIONS.toString())) {
			/**
			 * Process OPTIONS Call
			 */
			response.setStatusCode(HttpStatus.OK.value());
			response.setHeaders(LambdaUtils.getCORSHeaders(request));
			_LOGGER.info("Allowed Options on URL : " + request.getPath());
		} else {
			/**
			 * Process Handler Call
			 */
			processNonOptionRequest(request, response);
		}
	}

	private void processNonOptionRequest(AwsProxyRequest request, AwsProxyResponse response) {
		LambdaHandlerConfig handlerConfigObj = null;
		try {
			boolean allowExecution = preHandle(request, response);
			if (!allowExecution) {
				throw new ServerException(
						ErrorBean.withError(Errors.SERVER_EXCEPTION, "Request Processing not allowed", null));
			} else {

				String path = request.getPath();

				handlerConfigObj = handlerConfig.get(path);

				if (handlerConfigObj == null) {
					throw new NoDataException(ErrorBean.withError("404", "Handler Not Found : " + path, null));
				}

				Method handler = handlerConfigObj.getMethod();
				Object controllerBean = controllers.get(handlerConfigObj.getControllerBean());
				Expression entityValueExpr = new Expression(controllerBean, handler.getName(),
						getMethodArgs(request, handler));
				entityValueExpr.execute();
				Object result = entityValueExpr.getValue();

				response.setBody(MAPPER.writeValueAsString(result));
				response.setStatusCode(HttpStatus.OK.value());
			}
		} catch (Exception e) {
			_LOGGER.error(e.getMessage(), e);
			new LambdaExceptionHandler().prepareServerException(response, e);
		}

		response.setHeaders(addHeaders(handlerConfigObj));
		postHandle(request, response);
	}

	private void createHandlerConfig() {
		for (String key : controllers.keySet()) {
			Class<?> controllerClass = controllers.get(key).getClass();
			if (controllerClass.getDeclaredAnnotation(RestController.class) != null) {
				Method[] methods = controllerClass.getDeclaredMethods();
				for (Method m : methods) {
					LambdaHandlerConfig config = new LambdaHandlerConfig();
					Map<String, Object> handlerMethodInfo = getHandlerMethod(m);
					config.setHttpMethod(String.valueOf(handlerMethodInfo.get("method")));
					config.setHandler(String.valueOf(handlerMethodInfo.get("handler")));
					config.setProduces(handlerMethodInfo.get("produces") != null
							? Arrays.asList((String[]) handlerMethodInfo.get("produces"))
							: Arrays.asList(MediaType.APPLICATION_JSON_VALUE));
					config.setMethod(m);
					config.setParameters(m.getParameterTypes());
					config.setReturnType(m.getReturnType());
					config.setControllerBean(key);
					handlerConfig.put(String.valueOf(config.getHandler()), config);
					_LOGGER.info("" + config);
				}
			}
		}
	}

	private Map<String, Object> getHandlerMethod(Method m) {
		Map<String, Object> methodInfo = new HashMap<>();
		String method = HttpMethod.GET.toString();
		String handler = "/";
		String[] produces = null;
		if (m.getDeclaredAnnotation(PostMapping.class) != null) {
			PostMapping ann = m.getDeclaredAnnotation(PostMapping.class);
			method = HttpMethod.POST.toString();
			handler = String.valueOf(ann.value()[0]);
			produces = ann.produces();
		}
		if (m.getDeclaredAnnotation(GetMapping.class) != null) {
			method = HttpMethod.GET.toString();
			handler = String.valueOf(m.getDeclaredAnnotation(GetMapping.class).value()[0]);
			produces = m.getDeclaredAnnotation(GetMapping.class).produces();
		}
		if (m.getDeclaredAnnotation(PutMapping.class) != null) {
			method = HttpMethod.PUT.toString();
			handler = String.valueOf(m.getDeclaredAnnotation(PutMapping.class).value()[0]);
			produces = m.getDeclaredAnnotation(PutMapping.class).produces();
		}
		if (m.getDeclaredAnnotation(DeleteMapping.class) != null) {
			method = HttpMethod.DELETE.toString();
			handler = String.valueOf(m.getDeclaredAnnotation(DeleteMapping.class).value()[0]);
			produces = m.getDeclaredAnnotation(DeleteMapping.class).produces();
		}
		if (m.getDeclaredAnnotation(PatchMapping.class) != null) {
			method = HttpMethod.PATCH.toString();
			handler = String.valueOf(m.getDeclaredAnnotation(PatchMapping.class).value()[0]);
			produces = m.getDeclaredAnnotation(PatchMapping.class).produces();
		}

		methodInfo.put("method", method);
		methodInfo.put("handler", handler);
		methodInfo.put("produces", produces);
		return methodInfo;
	}

	private Object[] getMethodArgs(AwsProxyRequest request, Method m) throws MethodArgumentNotValidException {
		Object[] args = new Object[m.getParameterTypes().length];
		Class<?>[] params = m.getParameterTypes();
		BindingResult errors = null;
		boolean hasErrorArg = false;

		for (int i = 0; i < params.length; i++) {
			Annotation[] annotations = m.getParameterAnnotations()[i];
			Map<String, Object> paramsValues = paramValue(annotations, params[i], m, request);
			args[i] = paramsValues.get("paramValue");
			errors = (BindingResult) paramsValues.get("errors");
			if (params[i].isAssignableFrom(org.springframework.validation.Errors.class)) {
				args[i] = errors;
				hasErrorArg = true;
			}
		}

		if (!hasErrorArg && errors != null && errors.hasErrors()) {
			throw new MethodArgumentNotValidException(new MethodParameter(m, 0), errors);
		}

		return args;
	}

	protected <T> T asObject(String payload, Class<T> clazz) {
		try {
			return MAPPER.readValue(payload, clazz);
		} catch (Exception e) {
			_LOGGER.error(e.getMessage(), e);
			throw new MediaTypeNotSupportedStatusException("Request body is not properly formed");
		}
	}

	public Map<String, Object> paramValue(Annotation[] annotations, Class<?> claszz, Method m,
			AwsProxyRequest request) {
		Map<String, Object> data = new HashMap<>();
		Object paramValue = null;
		org.springframework.validation.Errors errors = null;
		Object payload = null;

		/**
		 * Populate Annotated Parameters
		 */
		for (Annotation ann : annotations) {
			if (ann.annotationType().isAssignableFrom(RequestBody.class)) {
				RequestBody body = AnnotationUtils.getAnnotation(ann, RequestBody.class);

				String requestBody = null;
				if (request.isBase64Encoded() && StringUtils.isNotEmpty(request.getBody())) {
					_LOGGER.info("Request Body is base 64 encoded : " + request.getBody());
					requestBody = new String(Base64.decodeBase64(request.getBody()));
					_LOGGER.info("Decoded Body : " + requestBody);
				} else {
					requestBody = request.getBody();
				}

				if (StringUtils.isNotBlank(requestBody)) {
					paramValue = asObject(requestBody, claszz);
				}
				payload = paramValue;
				if (body != null && body.required() && Objects.isNull(paramValue)) {
					throw new MediaTypeNotSupportedStatusException("Blank request body not allowed");
				}
				if (shouldValidate(annotations)) {
					errors = validateBean(payload);
				}
			}
			if (ann.annotationType().isAssignableFrom(RequestParam.class)) {
				String key = (String) AnnotationUtils.getDefaultValue(ann);
				String value = request.getQueryStringParameters().get(key);
				if (claszz.isAssignableFrom(Integer.class)) {
					paramValue = Integer.valueOf(value);
				} else if (claszz.isAssignableFrom(Long.class)) {
					paramValue = Long.valueOf(value);
				} else if (claszz.isAssignableFrom(Boolean.class)) {
					paramValue = Boolean.valueOf(value);
				} else {
					paramValue = value;
				}
			}
		}

		data.put("paramValue", paramValue);
		data.put("errors", errors);
		return data;
	}

	public boolean shouldValidate(Annotation[] annotations) {
		return Arrays.asList(annotations).stream().anyMatch(a -> a.annotationType().isAssignableFrom(Valid.class));
	}

	private boolean preHandle(AwsProxyRequest request, AwsProxyResponse response) {
		List<Boolean> result = new ArrayList<>();
		interceptorConfigurer.getInterceptors().forEach(h -> result.add(h.preHandle(request, response)));
		return !result.contains(false);
	}

	private void postHandle(AwsProxyRequest request, AwsProxyResponse response) {
		interceptorConfigurer.getInterceptors().forEach(h -> h.postHandle(request, response));
	}

	private Map<String, String> addHeaders(LambdaHandlerConfig handlerConfig) {
		Map<String, String> headers = LambdaUtils.getDefaultHeaders();
		/**
		 * Overwrite Content Type if handler has produces marked
		 */
		if (handlerConfig != null && CollectionUtils.isNotEmpty(handlerConfig.getProduces())) {
			headers.put(HttpHeaders.CONTENT_TYPE, handlerConfig.getProduces().get(0));
		}

		return headers;
	}

	public BindingResult validateBean(Object paramValue) {
		if (paramValue != null) {
			DataBinder dataBinder = new DataBinder(paramValue, paramValue.getClass().getSimpleName());
			dataBinder.addValidators(validatorAdaptor());
			dataBinder.validate(VALID);
			return dataBinder.getBindingResult();
		} else {
			return null;
		}
	}

	private SpringValidatorAdapter validatorAdaptor() {
		return validatorAdaptor == null
				? new SpringValidatorAdapter(ContextUtils.getBean(LocalValidatorFactoryBean.class))
				: validatorAdaptor;
	}

}
