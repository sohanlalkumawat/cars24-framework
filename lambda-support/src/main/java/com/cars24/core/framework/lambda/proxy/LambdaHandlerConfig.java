package com.cars24.core.framework.lambda.proxy;

import java.lang.reflect.Method;
import java.util.List;

public class LambdaHandlerConfig {

	private Method method;
	private String handler;
	private String httpMethod;
	private Class<?>[] parameters;
	private Class<?> returnType;
	private String controllerBean;
	private List<String> produces;

	public Method getMethod() {
		return method;
	}

	public void setMethod(Method method) {
		this.method = method;
	}

	public String getHandler() {
		return handler;
	}

	public void setHandler(String handler) {
		this.handler = handler;
	}

	public String getHttpMethod() {
		return httpMethod;
	}

	public void setHttpMethod(String httpMethod) {
		this.httpMethod = httpMethod;
	}

	public Class<?>[] getParameters() {
		return parameters;
	}

	public void setParameters(Class<?>[] parameters) {
		this.parameters = parameters;
	}

	public Class<?> getReturnType() {
		return returnType;
	}

	public void setReturnType(Class<?> returnType) {
		this.returnType = returnType;
	}

	public String getControllerBean() {
		return controllerBean;
	}

	public void setControllerBean(String controllerBean) {
		this.controllerBean = controllerBean;
	}

	public List<String> getProduces() {
		return produces;
	}

	public void setProduces(List<String> produces) {
		this.produces = produces;
	}

	@Override
	public String toString() {
		return "[" + httpMethod + "] : " + handler + " : LambdaHandlerConfig [method=" + method + ", returnType=" + returnType + ", controllerBean="
				+ controllerBean + "]";
	}

}
