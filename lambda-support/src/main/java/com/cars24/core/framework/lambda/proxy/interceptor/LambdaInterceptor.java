package com.cars24.core.framework.lambda.proxy.interceptor;

import com.amazonaws.serverless.proxy.model.AwsProxyRequest;
import com.amazonaws.serverless.proxy.model.AwsProxyResponse;

public interface LambdaInterceptor {

	public boolean preHandle(AwsProxyRequest request, AwsProxyResponse response);

	public void postHandle(AwsProxyRequest request, AwsProxyResponse response);
	
}
