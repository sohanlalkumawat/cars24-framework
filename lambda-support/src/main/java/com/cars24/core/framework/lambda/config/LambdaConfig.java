package com.cars24.core.framework.lambda.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.cars24.core.framework.common.constants.CommonConstants;
import com.cars24.core.framework.common.utils.ContextUtils;

@Configuration
@ComponentScan(basePackages = { CommonConstants.BASE_PACKAGE })
public class LambdaConfig {

	private static final Logger _LOGGER = LoggerFactory.getLogger(LambdaConfig.class);

	@Bean
	@Primary
	public ContextUtils contextUtils() {
		return new ContextUtils();
	}

}