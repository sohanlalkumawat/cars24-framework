package com.cars24.core.framework.lambda.proxy.interceptor;

import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.serverless.proxy.model.AwsProxyRequest;
import com.amazonaws.serverless.proxy.model.AwsProxyResponse;
import com.cars24.core.framework.common.constants.CommonConstants;
import com.cars24.core.framework.logging.helper.LoggingHelper;

public class LambdaLoggingInterceptor implements LambdaInterceptor {

	private static final Logger _LOGGER = LoggerFactory.getLogger(LambdaLoggingInterceptor.class);

	@Override
	public boolean preHandle(AwsProxyRequest request, AwsProxyResponse response) {
		_LOGGER.info("Pre Handle Logging");
		LoggingHelper.prepareLoggerContext(getClientIp(request), request.getHttpMethod(), request.getPath().toString(),
				getUserAgent(request.getHeaders()), getRequestId(request));
		return true;
	}

	@Override
	public void postHandle(AwsProxyRequest request, AwsProxyResponse response) {
		LoggingHelper.writeLogs();
		LoggingHelper.clearContext();
		_LOGGER.info("Post Handler Logging : Complete");
	}

	private String getRequestId(AwsProxyRequest request) {
		if (StringUtils.isBlank(request.getHeaders().get(CommonConstants.X_REQUEST_ID))) {
			return UUID.randomUUID().toString();
		}
		return request.getHeaders().get(CommonConstants.X_REQUEST_ID);
	}

	private String getClientIp(AwsProxyRequest request) {
		String ip = null;
		if (request.getHeaders().get("X-Forwarded-For") != null) {
			ip = request.getHeaders().get("X-Forwarded-For");
		}
		if (StringUtils.isBlank(ip) && request.getRequestContext() != null
				&& request.getRequestContext().getIdentity() != null
				&& request.getRequestContext().getIdentity().getSourceIp() != null) {
			ip = request.getRequestContext().getIdentity().getSourceIp();
		}
		if (StringUtils.isNotEmpty(ip) && ip.contains(",")) {
			ip = ip.split(",")[0];
		}
		if (StringUtils.isBlank(ip)) {
			ip = "127.0.0.1";
		}
		_LOGGER.info("Client Ip : " + ip);
		return ip;
	}

	private String getUserAgent(Map<String, String> headers) {
		String userAgent = headers.get("user-agent");
		if (StringUtils.isBlank(userAgent)) {
			userAgent = headers.get("User-Agent");
		}
		return userAgent;
	}
}
