package com.cars24.core.framework.lambda.config;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.ws.rs.HttpMethod;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.amazonaws.serverless.proxy.internal.LambdaContainerHandler;
import com.amazonaws.serverless.proxy.internal.testutils.AwsProxyRequestBuilder;
import com.amazonaws.serverless.proxy.internal.testutils.MockLambdaContext;
import com.amazonaws.serverless.proxy.model.AwsProxyResponse;
import com.amazonaws.services.lambda.runtime.Context;
import com.cars24.core.framework.common.utils.CollectionUtils;
import com.cars24.core.framework.lambda.handlers.StreamLambdaHandler;

public class LambdaProxyConfiguration {

	public static StreamLambdaHandler handler;
	public static Context lambdaContext;

	public AwsProxyResponse get(String handler, Map<String, String> headers, Map<String, String> queryParams) {

		AwsProxyRequestBuilder request = new AwsProxyRequestBuilder(handler, HttpMethod.GET)
				.header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON).userAgent("AmazonProxy/Java(TestNG:Test)");

		if (CollectionUtils.isNotEmpty(headers)) {
			for (Entry<String, String> header : headers.entrySet()) {
				request.header(header.getKey(), header.getValue());
			}
		}

		if (CollectionUtils.isNotEmpty(queryParams)) {
			for (Entry<String, String> param : queryParams.entrySet()) {
				request.queryString(param.getKey(), param.getValue());
			}
		}

		InputStream requestStream = request.buildStream();

		ByteArrayOutputStream responseStream = new ByteArrayOutputStream();
		handle(requestStream, responseStream);
		AwsProxyResponse response = readResponse(responseStream);

		assertNotNull(response);
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatusCode());
		assertFalse(response.isBase64Encoded());

		return response;
	}

	public AwsProxyResponse post(String handler, Object paylod, Map<String, String> headers) {

		AwsProxyRequestBuilder request = new AwsProxyRequestBuilder(handler, HttpMethod.POST)
				.header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON).body(paylod)
				.userAgent("AmazonProxy/Java(TestNG:Test)");

		if (CollectionUtils.isNotEmpty(headers)) {
			for (Entry<String, String> header : headers.entrySet()) {
				request.header(header.getKey(), header.getValue());
			}
		}

		InputStream requestStream = request.buildStream();

		ByteArrayOutputStream responseStream = new ByteArrayOutputStream();
		handle(requestStream, responseStream);
		AwsProxyResponse response = readResponse(responseStream);

		assertNotNull(response);
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatusCode());
		assertFalse(response.isBase64Encoded());

		return response;

	}

	public Map<String, String> getAuthHeaders() {
		Map<String, String> authRequest = new HashMap<>();
		return authRequest;
	}

	private void handle(InputStream is, ByteArrayOutputStream os) {
		try {
			handler.handleRequest(is, os, lambdaContext);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private AwsProxyResponse readResponse(ByteArrayOutputStream responseStream) {
		try {
			return LambdaContainerHandler.getObjectMapper().readValue(responseStream.toByteArray(),
					AwsProxyResponse.class);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static InputStream newPostInputStream(String handler, Object paylod, Map<String, String> headers) {
		AwsProxyRequestBuilder request = new AwsProxyRequestBuilder(handler, HttpMethod.POST)
				.header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON).body(paylod)
				.userAgent("AmazonProxy/Java(TestNG:Test)");

		if (CollectionUtils.isNotEmpty(headers)) {
			for (Entry<String, String> header : headers.entrySet()) {
				request.header(header.getKey(), header.getValue());
			}
		}
		return request.buildStream();
	}

	public static InputStream newGetInputStream(String handler, Map<String, String> headers,
			Map<String, String> queryParams) {
		AwsProxyRequestBuilder request = new AwsProxyRequestBuilder(handler, HttpMethod.GET)
				.header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON).userAgent("AmazonProxy/Java(TestNG:Test)");

		if (CollectionUtils.isNotEmpty(headers)) {
			for (Entry<String, String> header : headers.entrySet()) {
				request.header(header.getKey(), header.getValue());
			}
		}

		if (CollectionUtils.isNotEmpty(queryParams)) {
			for (Entry<String, String> param : queryParams.entrySet()) {
				request.queryString(param.getKey(), param.getValue());
			}
		}
		return request.buildStream();
	}

	public static OutputStream newOutputStream() {
		return new ByteArrayOutputStream();
	}

	public static Context newContext() {
		return new MockLambdaContext();
	}
}
