package com.cars24.core.framework.lambda.handlers;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.amazonaws.serverless.exceptions.InvalidRequestEventException;
import com.amazonaws.serverless.proxy.model.AwsProxyRequest;
import com.amazonaws.serverless.proxy.model.AwsProxyResponse;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestStreamHandler;
import com.cars24.core.framework.common.exception.ConfigurationException;
import com.cars24.core.framework.common.utils.ContextUtils;
import com.cars24.core.framework.lambda.config.LambdaConfig;
import com.cars24.core.framework.lambda.proxy.LambdaSpringHandler;

/**
 * Supports Spring Component Scan - Features Available will be - Context,
 * Configuration, Validation JDBC and Scanned beans
 * 
 * @author Sohan
 *
 */
public abstract class SpringProxyHandler extends BaseLambdaHandler implements RequestStreamHandler {

	private static final Logger _LOGGER = LoggerFactory.getLogger(SpringProxyHandler.class);
	public static AnnotationConfigApplicationContext springContext;

	static {
		initializeSpring();
	}

	@Override
	public void handleRequest(InputStream inputStream, OutputStream outputStream, Context context) throws IOException {
		_LOGGER.info(" SpringProxyHandler Request Initiated");
		try {
			/**
			 * Load Spring Context
			 */
			initializeSpring();
			AwsProxyRequest proxyRequest = MAPPER.readValue(inputStream, AwsProxyRequest.class);
			/**
			 * Validate the request if is API or Event Type
			 */
			validateProxyRequest(proxyRequest);

			AwsProxyResponse response = new AwsProxyResponse();
			springContext.getBean(LambdaSpringHandler.class).handleRequest(proxyRequest, response);

			_RESPONSE_WRITER.writeValue(outputStream, response);
		} catch (ConfigurationException e) {
			throw new ConfigurationException("Could not initialize Spring framework", e);
		} catch (Exception e) {
			_LOGGER.warn("Error Processing API Request : " + e.getMessage());
			super.sendErrorResponse(outputStream, e);
		}
	}

	private void validateProxyRequest(AwsProxyRequest proxyRequest) throws InvalidRequestEventException {
		if (!isApiCall(proxyRequest)) {
			throw new InvalidRequestEventException("Invalid API Call Event");
		}
	}

	private static void initializeSpring() {
		System.setProperty(LambdaSpringHandler.SPRING_PROXY_KEY, "Y");
		if (springContext == null) {
			try {
				setUpLambdaEnvironment();
				springContext = new AnnotationConfigApplicationContext(ContextUtils.class, LambdaConfig.class, LambdaSpringHandler.class);
			} catch (Exception e) {
				_LOGGER.error(" Spring Initialization Error : " + e.getMessage(), e);
				throw new ConfigurationException("Could not initialize Spring framework", e);
			}
		}
	}

}
