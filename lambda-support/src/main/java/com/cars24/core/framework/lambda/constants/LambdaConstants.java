
package com.cars24.core.framework.lambda.constants;

/**
 * The Class LambdaConstants.
 *
 * @author Sohan
 */
public class LambdaConstants {
	public static final String LAMBDA_STAGE_NAME = "lambda_stage";
	public static final String LAMBDA_TIME_ZONE = "lambda_tz";
	public static final String LAMBDA_NAME = "lambda_name";

	public static final String ANY_METHOD = "GET, HEAD, POST, PUT, DELETE, OPTIONS, PATCH";
	public static final String DEFAULT_AGE = "1800";
	public static final String EXPOSE_HEADERS = "auth-key, cars24-filename, Content-Disposition, Content-Type, Accept, Accept-Encoding, Accept-Language, Cache-Control, Content-Length, Connection, Host, Origin, Pragma, User-Agent, Referer";
	public static final String ALL_ORIGIN = "*";
	public static final String VARY_HEADER = "Access-Control-Request-Headers";
}
