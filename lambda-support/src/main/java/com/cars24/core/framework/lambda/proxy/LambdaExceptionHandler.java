package com.cars24.core.framework.lambda.proxy;

import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.server.MediaTypeNotSupportedStatusException;

import com.amazonaws.serverless.proxy.model.AwsProxyResponse;
import com.cars24.core.framework.common.bean.ErrorBean;
import com.cars24.core.framework.common.bean.ErrorResponse;
import com.cars24.core.framework.common.bean.Errors;
import com.cars24.core.framework.common.exception.BusinessException;
import com.cars24.core.framework.common.exception.ConfigurationException;
import com.cars24.core.framework.common.exception.NoDataException;
import com.cars24.core.framework.common.exception.ServerException;
import com.cars24.core.framework.common.exception.ValidationException;
import com.cars24.core.framework.common.utils.RequestValidationUtils;
import com.cars24.core.framework.utility.StringUtils;
import com.cars24.core.framework.websupport.exception.ExceptionLogging;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;

public class LambdaExceptionHandler {

	private static final Logger _LOGGER = LoggerFactory.getLogger(LambdaExceptionHandler.class);

	public void prepareServerException(AwsProxyResponse response, Exception ex) {

		ErrorResponse errorResponse = null;
		HttpStatus httpStatus = null;
		_LOGGER.error(ex.getMessage(), ex);
		if (ex instanceof ServerException) {
			ServerException e = (ServerException) ex;
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			errorResponse = ErrorResponse.create().message(e.getMessage()).addError(e.getError());
		} else if (ex instanceof ValidationException) {
			httpStatus = HttpStatus.BAD_REQUEST;
			ValidationException e = (ValidationException) ex;
			errorResponse = RequestValidationUtils.prepareValidationResponse(e);
		} else if (ex instanceof BusinessException) {
			BusinessException e = (BusinessException) ex;
			httpStatus = HttpStatus.BAD_REQUEST;
			errorResponse = ErrorResponse.create().message(e.getMessage()).addError(e.getError());

		} else if (ex instanceof NoDataException) {
			NoDataException e = (NoDataException) ex;
			httpStatus = HttpStatus.NOT_FOUND;
			errorResponse = ErrorResponse.create().message(e.getMessage()).addError(e.getError());

		}

		else if (ex instanceof ConfigurationException) {
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			errorResponse = ErrorResponse.create().message("Configuration Error")
					.addError(ErrorBean.withError(Errors.SERVER_EXCEPTION));

		} else if (ex instanceof IllegalArgumentException) {
			IllegalArgumentException e = (IllegalArgumentException) ex;
			httpStatus = HttpStatus.BAD_REQUEST;
			errorResponse = ErrorResponse.create().message(e.getMessage())
					.addError(ErrorBean.withError(Errors.UNSUPPORTED_DB_OPERATION));

		} else if (ex instanceof NumberFormatException) {
			NumberFormatException e = (NumberFormatException) ex;
			httpStatus = HttpStatus.BAD_REQUEST;
			errorResponse = ErrorResponse.create().message(e.getMessage())
					.addError(ErrorBean.withError(Errors.INVALID_NUMBER));

		} else if (ex instanceof JsonMappingException || ex instanceof InvalidFormatException) {
			JsonMappingException e = (JsonMappingException) ex;
			String message = StringUtils.isBlank(e.getOriginalMessage())
					? ErrorBean.withError(Errors.INVALID_REQUEST_PAYLOAD).getMessage()
					: e.getOriginalMessage();
			errorResponse = ErrorResponse.create()
					.message(ErrorBean.withError(Errors.INVALID_REQUEST_PAYLOAD).getMessage())
					.addError(ErrorBean.withError(Errors.INVALID_REQUEST_PAYLOAD, message, e.getPathReference()));
			httpStatus = HttpStatus.BAD_REQUEST;
		} else if (ex instanceof JsonParseException) {
			JsonParseException e = (JsonParseException) ex;
			httpStatus = HttpStatus.BAD_REQUEST;
			errorResponse = ErrorResponse.create()
					.message(ErrorBean.withError(Errors.INVALID_REQUEST_PAYLOAD).getMessage())
					.addError(ErrorBean.withError(Errors.INVALID_REQUEST_PAYLOAD, e.getMessage()));

		} else if (ex instanceof BadSqlGrammarException) {
			httpStatus = HttpStatus.BAD_REQUEST;
			errorResponse = ErrorResponse.create().message(ErrorBean.withError(Errors.JDBC_GRAMMER_ERROR).getMessage())
					.addError(ErrorBean.withError(Errors.JDBC_GRAMMER_ERROR, ex.getMessage()));
		} else if (ex instanceof MediaTypeNotSupportedStatusException) {
			MediaTypeNotSupportedStatusException e = (MediaTypeNotSupportedStatusException) ex;
			httpStatus = HttpStatus.UNSUPPORTED_MEDIA_TYPE;
			errorResponse = ErrorResponse.create().message("Unsupported Media Type")
					.addError(ErrorBean.withError(Errors.UNSUPPORTED_MEDIA_TYPE, e.getMessage(), null));
		} else if (ex instanceof MethodArgumentNotValidException) {
			MethodArgumentNotValidException e = (MethodArgumentNotValidException) ex;
			httpStatus = HttpStatus.BAD_REQUEST;
			errorResponse = ErrorResponse.create().message("Validation Errors")
					.errors(e.getBindingResult().getFieldErrors().parallelStream()
							.map(error -> RequestValidationUtils.prepareError(error)).collect(Collectors.toList()));
		} else {
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			response.setBody(getBody(ErrorResponse.create().message("Internal Server Error").message(ex.getMessage())));
		}

		response.setStatusCode(httpStatus.value());
		response.setBody(getBody(errorResponse));

		logError(errorResponse, ex);
	}

	private String getBody(Object obj) {
		try {
			return LambdaSpringHandler.MAPPER.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			_LOGGER.error(e.getMessage(), e);
			return null;
		}
	}

	public ErrorResponse logError(ErrorResponse response, Exception ex) {
		try {
			ExceptionLogging.logAndReturn(response, ex);
		} catch (Exception e) {
			_LOGGER.warn(e.getMessage());
		}
		return response;
	}
}
