package com.cars24.core.framework.lambda.proxy.interceptor;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

@Component
public class LambdaInterceptorConfigurer {

	List<LambdaInterceptor> interceptors = new ArrayList<>();

	@PostConstruct
	public void init() {
		interceptors.add(new LambdaLoggingInterceptor());
	}

	public List<LambdaInterceptor> getInterceptors() {
		return interceptors;
	}
}
