package com.cars24.core.framework.lambda.handlers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.cars24.core.framework.common.exception.ConfigurationException;
import com.cars24.core.framework.common.utils.ContextUtils;
import com.cars24.core.framework.lambda.config.LambdaConfig;

/**
 * Supports Spring Component Scan - Features Available will be - Context,
 * Configuration, Cache, JDBC and Scanned beans
 * 
 * @author Sohan
 *
 */
@SuppressWarnings("unchecked")
public abstract class LambdaEventHandler<I, O> extends BaseLambdaHandler implements RequestHandler<I, O> {

	private static final Logger _LOGGER = LoggerFactory.getLogger(LambdaEventHandler.class);

	public static ApplicationContext springContext;
	static {
		loadSpringContext();
	}

	@Override
	public O handleRequest(I event, Context context) {
		_LOGGER.info("ScheduledEventHandler request initiated");
		loadSpringContext();
		return (O) "Spring Loaded";
	}

	private static void loadSpringContext() {
		if (springContext == null) {
			try {
				setUpLambdaEnvironment();
				springContext = new AnnotationConfigApplicationContext(ContextUtils.class, LambdaConfig.class);
			} catch (Exception e) {
				_LOGGER.error(" Spring Initialization Error : " + e.getMessage(), e);
				throw new ConfigurationException("Could not initialize Spring framework", e);
			}
		}
	}

}
