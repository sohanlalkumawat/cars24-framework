package com.cars24.core.framework.utility;

import java.io.InputStream;
import java.util.Map;

import org.apache.commons.text.StringSubstitutor;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Utility Class JSONUtils.
 *
 * @author Sohan
 * @version $Id: $Id
 */
public class JSONUtils {

	private static final String LOG_TAG = "[ OBJECT MAPPING ERROR ] ";

	public static <T> T jsonToObject(String payload, Class<T> clazz) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			return mapper.readValue(payload, clazz);
		} catch (Exception e) {
			System.out.println(LOG_TAG + e.getMessage());
			return null;
		}
	}

	public static <T> T streamToObject(InputStream stream, Class<T> clazz) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			return mapper.readValue(stream, clazz);
		} catch (Exception e) {
			System.out.println(LOG_TAG + e.getMessage());
			return null;
		}
	}

	public static <T> T objectToClass(Object object, Class<T> clazz) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			return mapper.convertValue(object, clazz);
		} catch (Exception e) {
			System.out.println(LOG_TAG + e.getMessage());
			return null;
		}
	}

	public static String objectToJson(Object object) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			String value = mapper.writeValueAsString(object);
			return value;
		} catch (Exception e) {
			System.out.println(LOG_TAG + e.getMessage());
			return null;
		}
	}

	public static String objectToPrettyJson(Object object) {
		try {
			Gson mapper = new GsonBuilder().setPrettyPrinting().create();
			String value = mapper.toJson(object);
			return value;
		} catch (Exception e) {
			System.out.println(LOG_TAG + e.getMessage());
			return null;
		}
	}

	public static String stringSubstitutor(String json, Map<String, String> valueMap) {
		StringSubstitutor stringSubstitutor = new StringSubstitutor(valueMap);
		return stringSubstitutor.replace(json);
	}
}
