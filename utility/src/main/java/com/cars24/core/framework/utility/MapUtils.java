
package com.cars24.core.framework.utility;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

/*
 * @author Sohan
 */
public class MapUtils {

	public static Map<String, Object> stringToMap(String jsonString) {
		Map<String, Object> map = new HashMap<>();

		if (StringUtils.isNotBlank(jsonString)) {
			try {
				ObjectMapper mapper = new ObjectMapper();
				map = mapper.readValue(jsonString, new TypeReference<Map<String, Object>>() {
				});
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
		}

		return map;
	}

	public static String mapToString(Map<String, Object> map) {
		String json = "";

		if (map != null) {
			try {
				ObjectMapper mapper = new ObjectMapper();
				json = mapper.writeValueAsString(map);
				json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(map);
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
		}
		return json;
	}
}
