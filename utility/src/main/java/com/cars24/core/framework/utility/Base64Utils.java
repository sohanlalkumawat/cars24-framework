package com.cars24.core.framework.utility;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Base64;

import org.apache.tika.Tika;


public class Base64Utils {

	private Base64Utils() {
	}

	public static String encode(File file) {
		String encodedString = null;
		try {
			encodedString = Base64.getEncoder().encodeToString(Files.readAllBytes(file.toPath()));
		} catch (IOException e) {
			e.printStackTrace();
		}

		return encodedString;
	}

	/**
	 * <p>
	 * docode.
	 * </p>
	 *
	 * @param base64File a {@link java.lang.String} object.
	 * @return an array of {@link byte} objects.
	 */
	public static byte[] docode(String base64File) {
		return Base64.getDecoder().decode(base64File);
	}

	/**
	 * <p>
	 * getFileType.
	 * </p>
	 *
	 * @param base64File a {@link java.lang.String} object.
	 * @return a {@link java.lang.String} object.
	 */

	public static String getFileType(String base64File) {

		byte[] base64Bytes = docode(base64File);

		Tika tika = new Tika();
		String fileType = tika.detect(base64Bytes);
		return fileType;
	}

}
