
package com.cars24.core.framework.utility;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.text.WordUtils;

/**
 * Utility Class StringUtils.
 *
 * @author Sohan
 * @version $Id: $Id
 */
public class StringUtils {

	private StringUtils() {
	}

	public static String toCamelCase(String s) {
		return WordUtils.capitalize(normalizeSpace(s));
	}

	public static String normalizeSpace(String s) {
		return org.apache.commons.lang3.StringUtils.normalizeSpace(s);
	}

	public static boolean hasMatchingPattern(String pattern, String value) {
		return hasMatchingPattern(pattern, value, -1);
	}

	public static boolean hasMatchingPattern(String pattern, String value, int isCaseSensitive) {
		boolean isMatchingPattern;

		Pattern p;
		if (isCaseSensitive == -1) {
			p = Pattern.compile(pattern);
		} else {
			p = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
		}
		final Matcher m = p.matcher(value);
		isMatchingPattern = m.find();
		return isMatchingPattern;
	}

	public static Boolean isEmpty(String str) {
		return org.springframework.util.StringUtils.isEmpty(str);
	}

	public static Boolean isNotEmpty(String str) {
		return !isEmpty(str);
	}

	public static Boolean isBlank(String str) {
		return org.apache.commons.lang3.StringUtils.isBlank(str);
	}

	public static Boolean isNotBlank(String str) {
		return !isBlank(str);
	}

	public static boolean isBlank(String[] strs) {
		return strs == null || strs.length == 0;
	}

	public static boolean isNotBlank(String[] strs) {
		return !isBlank(strs);
	}

	public static boolean isBlank(Collection<String> strs) {
		return strs == null || strs.size() == 0;
	}

	public static boolean isNotBlank(Collection<String> strs) {
		return !isBlank(strs);
	}

	@SafeVarargs
	public static <T> String join(final T... elements) {
		return org.apache.commons.lang3.StringUtils.join(elements);
	}

	@SafeVarargs
	public static <T> String join(char separator, final T... elements) {
		return org.apache.commons.lang3.StringUtils.join(elements, separator);
	}

	@SafeVarargs
	public static <T> String csvJoin(final T... elements) {
		return org.apache.commons.lang3.StringUtils.join(elements, ',');
	}

	public static String[] split(String textToSplitOnWhiteSpace) {
		return org.apache.commons.lang3.StringUtils.split(textToSplitOnWhiteSpace);
	}

	public static String[] split(char separator, String textToSplit) {
		return org.apache.commons.lang3.StringUtils.split(textToSplit, separator);
	}

	public static String[] split(String separator, String textToSplit) {
		return org.apache.commons.lang3.StringUtils.split(textToSplit, separator);
	}

	public static String[] csvSplit(String cvsText) {
		return org.apache.commons.lang3.StringUtils.split(cvsText, ',');
	}

	public static String rightPad(String data, int size, char padChar) {
		return org.apache.commons.lang3.StringUtils.rightPad(data, size, padChar);
	}

	public static String leftPad(String data, int size, char padChar) {
		return org.apache.commons.lang3.StringUtils.leftPad(data, size, padChar);
	}

	public static String convertCamelCaseToStatement(String camelCase) {
		try {
			final StringBuilder builder = new StringBuilder();
			for (final Character c : camelCase.toCharArray()) {
				if (Character.isUpperCase(c)) {
					builder.append(" ").append(c);
				} else {
					builder.append(c);
				}
			}
			return builder.toString();
		} catch (final Exception e) {
			return camelCase;
		}
	}

	public static int compareVersion(String version1, String version2) {
		final char[] version1Value = version1.toCharArray();
		final char[] version2Value = version2.toCharArray();

		final StringBuilder actualVersion1 = new StringBuilder();
		final StringBuilder actualVersion2 = new StringBuilder();

		for (final char c : version1Value) {
			actualVersion1.append((int) c + "");
		}
		for (final char c : version2Value) {
			actualVersion2.append((int) c + "");
		}
		return Long.valueOf(actualVersion1.toString()).compareTo(Long.valueOf(actualVersion2.toString()));
	}

	@SuppressWarnings("unchecked")
	public static Map<String, Object> getFreemarkerTokens(String text) {
		final Map<String, Object> tokenMap = new HashMap<>();
		try {
			final Pattern pt = Pattern.compile("\\$\\{([^}]*)\\}");
			final Matcher m = pt.matcher(text);
			while (m.find()) {
				final String token = m.group(1).split("\\?")[0];
				if (token.contains(".")) {
					tokenMap.computeIfAbsent(token.split("\\.")[0], v -> new HashMap<>());
					((Map<String, Object>) tokenMap.get(token.split("\\.")[0])).put(token.split("\\.")[1], null);
				} else {
					tokenMap.put(token, null);
				}
			}
		} catch (final Exception e) {
		}
		return tokenMap;
	}

}
