package com.cars24.core.framework.utility;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Days;
import org.joda.time.Months;
import org.joda.time.Years;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.util.Assert;

/**
 * The Class DateUtils.
 *
 * @author Sohan
 * @version $Id: $Id
 */
public class DateUtils {

	/** The Constant INVALID. */
	public static final long INVALID = -99999999;

	/** The Constant SHORT_DATE_FMT. */
	public static final String SHORT_DATE_FMT = "yyyy-MM-dd";

	public static final String DATE_FMT_WITH_TIME = "yyyy-MM-dd HH:mm:ss";

	public static final String TIME_FORMAT_WITHOUT_COLON = "HHmm";

	public static final DateTimeZone APPL_TZ = getTimeZone();

	private static DateTimeZone getTimeZone() {
		return DateTimeZone.forID(TimeZone.getDefault().getID());
	}

	/**
	 * Adds the days.
	 *
	 * @param firstDate the first date
	 * @param days      the days
	 * @return the date
	 */
	public static Date addDays(Date date, int days) {
		Date addedDate = null;
		if (date != null) {
			final DateTime addedDateTime = new DateTime(date, APPL_TZ).plusDays(days);
			addedDate = addedDateTime.toDate();
		}
		return addedDate;
	}

	/**
	 * <p>
	 * nextDateAsString.
	 * </p>
	 *
	 * @param format a {@link java.lang.String} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String nextDateAsString(String format) {
		return DateUtils.toString(format, DateUtils.addDays(new Date(), 1));
	}

	/**
	 * <p>
	 * previousDateAsString.
	 * </p>
	 *
	 * @param format a {@link java.lang.String} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String previousDateAsString(String format) {
		return DateUtils.toString(format, DateUtils.addDays(new Date(), -1));
	}

	/**
	 * <p>
	 * nextDateAsString.
	 * </p>
	 *
	 * @param format a {@link java.lang.String} object.
	 * @param date   a {@link java.lang.String} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String nextDateAsString(String format, String date) {
		return DateUtils.toString(format, DateUtils.addDays(date, 1));
	}

	/**
	 * <p>
	 * nextDateAsString.
	 * </p>
	 *
	 * @param format a {@link java.lang.String} object.
	 * @param date   a {@link java.util.Date} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String nextDateAsString(String format, Date date) {
		return DateUtils.toString(format, DateUtils.addDays(date, 1));
	}

	/**
	 * <p>
	 * nextDate.
	 * </p>
	 *
	 * @param format a {@link java.lang.String} object.
	 * @return a {@link java.util.Date} object.
	 */
	public static Date nextDate(String format) {
		return DateUtils.getDate(format, DateUtils.toString(format, DateUtils.addDays(new Date(), 1)));
	}

	/**
	 * <p>
	 * nextDate.
	 * </p>
	 *
	 * @param format a {@link java.lang.String} object.
	 * @param date   a {@link java.lang.String} object.
	 * @return a {@link java.util.Date} object.
	 */
	public static Date nextDate(String format, String date) {
		return DateUtils.getDate(format, DateUtils.toString(format, DateUtils.addDays(getDate(format, date), 1)));
	}

	/**
	 * <p>
	 * nextDayOfWeek.
	 * </p>
	 *
	 * @param format  a {@link java.lang.String} object.
	 * @param date    a {@link java.util.Date} object.
	 * @param nextDay a int.
	 * @return a {@link java.util.Date} object.
	 */
	public static Date nextDayOfWeek(String format, Date date, int nextDay) {
		final Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);

		final int weekday = calendar.get(Calendar.DAY_OF_WEEK);
		int days = nextDay - weekday;
		if (days < 0) {
			days += 7;
		}
		calendar.add(Calendar.DAY_OF_YEAR, days);

		return getDate(format, toString(format, calendar.getTime()));
	}

	/**
	 * <p>
	 * nextDayOfWeek.
	 * </p>
	 *
	 * @param format  a {@link java.lang.String} object.
	 * @param date    a {@link java.lang.String} object.
	 * @param nextDay a int.
	 * @return a {@link java.util.Date} object.
	 */
	public static Date nextDayOfWeek(String format, String date, int nextDay) {
		return nextDayOfWeek(format, getDate(format, date), nextDay);
	}

	/**
	 * <p>
	 * nextDayOfWeekAsString.
	 * </p>
	 *
	 * @param format  a {@link java.lang.String} object.
	 * @param date    a {@link java.lang.String} object.
	 * @param nextDay a int.
	 * @return a {@link java.lang.String} object.
	 */
	public static String nextDayOfWeekAsString(String format, String date, int nextDay) {
		return toString(format, nextDayOfWeek(format, getDate(format, date), nextDay));
	}

	/**
	 * Provide day between two days.
	 *
	 * @param firstDate the first date
	 * @param secDate   First Date
	 * @return days between two dates. If first date is 01/01/2018 and secord date
	 *         is 02/01/2018 (Date in dd/mm/yyyy), it will return 1
	 */
	public static int daysBetween(Date firstDate, Date secDate) {
		int retvalue = -99999999;
		if (firstDate != null && secDate != null) {
			retvalue = Days.daysBetween(new DateTime(firstDate, APPL_TZ).toLocalDate(),
					new DateTime(secDate, APPL_TZ).toLocalDate()).getDays();
		}
		return retvalue;
	}

	/**
	 * Adds the days.
	 *
	 * @param firstDate the first date
	 * @param days      the days
	 * @return the date
	 */
	public static Date addDays(String firstDate, int days) {
		return addDays(getDate(firstDate), days);
	}

	/**
	 * Adds the days.
	 *
	 * @param format    the format
	 * @param firstDate the first date
	 * @param days      the days
	 * @return the date
	 */
	public static Date addDays(String format, String firstDate, int days) {
		return addDays(getDate(format, firstDate), days);
	}

	/**
	 * Adds the months.
	 *
	 * @param firstDate the first date
	 * @param months    the months
	 * @return the date
	 */
	public static Date addMonths(Date firstDate, int months) {
		Date addedDate = null;
		if (firstDate != null) {
			final DateTime addedDateTime = new DateTime(firstDate, APPL_TZ).plusMonths(months);
			addedDate = addedDateTime.toDate();
		}
		return addedDate;
	}

	/**
	 * Adds the months.
	 *
	 * @param firstDate the first date
	 * @param months    the months
	 * @return the date
	 */
	public static Date addMonths(String firstDate, int months) {
		return addMonths(getDate(firstDate), months);
	}

	/**
	 * Adds the months.
	 *
	 * @param format    the format
	 * @param firstDate the first date
	 * @param months    the months
	 * @return the date
	 */
	public static Date addMonths(String format, String firstDate, int months) {
		return addMonths(getDate(format, firstDate), months);
	}

	/**
	 * Adds the year month day.
	 *
	 * @param firstDate the first date
	 * @param years     the years
	 * @param months    the months
	 * @param days      the days
	 * @return the date
	 */
	public static Date addYearMonthDay(Date firstDate, int years, int months, int days) {
		Date addedDate = null;
		if (firstDate != null) {
			final DateTime addedDateTime = new DateTime(firstDate, APPL_TZ).plusYears(years).plusMonths(months)
					.plusDays(days);
			addedDate = addedDateTime.toDate();
		}
		return addedDate;
	}

	/**
	 * Adds the year month day.
	 *
	 * @param firstDate the first date
	 * @param years     the years
	 * @param months    the months
	 * @param days      the days
	 * @return the date
	 */
	public static Date addYearMonthDay(String firstDate, int years, int months, int days) {
		return addYearMonthDay(getDate(firstDate), years, months, days);
	}

	/**
	 * Adds the year month day.
	 *
	 * @param format    the format
	 * @param firstDate the first date
	 * @param years     the years
	 * @param months    the months
	 * @param days      the days
	 * @return the date
	 */
	public static Date addYearMonthDay(String format, String firstDate, int years, int months, int days) {
		return addYearMonthDay(getDate(format, firstDate), years, months, days);
	}

	/**
	 * Adds the years.
	 *
	 * @param firstDate the first date
	 * @param years     the years
	 * @return the date
	 */
	public static Date addYears(Date firstDate, int years) {
		Date addedDate = null;
		if (firstDate != null) {
			final DateTime addedDateTime = new DateTime(firstDate, APPL_TZ).plusYears(years);
			addedDate = addedDateTime.toDate();
		}
		return addedDate;
	}

	/**
	 * Adds the years.
	 *
	 * @param firstDate the first date
	 * @param years     the years
	 * @return the date
	 */
	public static Date addYears(String firstDate, int years) {
		return addYears(getDate(firstDate), years);
	}

	/**
	 * Adds the years.
	 *
	 * @param format    the format
	 * @param firstDate the first date
	 * @param years     the years
	 * @return the date
	 */
	public static Date addYears(String format, String firstDate, int years) {
		return addYears(getDate(format, firstDate), years);
	}

	/**
	 * Gets the calendar.
	 *
	 * @param date the date
	 * @return the calendar
	 */
	public static Calendar getCalendar(Date date) {
		final Calendar cal = Calendar.getInstance(Locale.ENGLISH);
		cal.setTime(date);
		return cal;
	}

	/**
	 * Gets the date.
	 *
	 * @param dateStr the date str
	 * @return the date
	 */
	public static Date getDate(String dateStr) {
		return getDate(SHORT_DATE_FMT, dateStr);
	}

	/**
	 * Gets the date.
	 *
	 * @param format  the format
	 * @param dateStr the date str
	 * @return the date
	 */
	public static Date getDate(String format, String dateStr) {
		Date date = null;
		final DateFormat dateFormat = new SimpleDateFormat(format);
		dateFormat.setLenient(false);
		try {
			date = dateFormat.parse(dateStr);
		} catch (final Exception e) {
			// do nothing
		}
		return date;
	}

	/**
	 * <p>
	 * getDateWithTime.
	 * </p>
	 *
	 * @param dateStr a {@link java.lang.String} object.
	 * @return a {@link java.util.Date} object.
	 */
	public static Date getDateWithTime(String dateStr) {
		return getDate(DATE_FMT_WITH_TIME, dateStr);
	}

	/**
	 * <p>
	 * getTime.
	 * </p>
	 *
	 * @param format a {@link java.lang.String} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String getTime(String format) {
		if (StringUtils.isBlank(format)) {
			format = TIME_FORMAT_WITHOUT_COLON;
		}
		return new SimpleDateFormat(format).format(new Date());
	}

	/**
	 * Gets the date diff.
	 *
	 * @param firstDate  the first date
	 * @param secondDate the second date
	 * @return the date diff
	 */
	public static long getDateDiff(Date firstDate, Date secondDate) {
		long diff = -99999999;
		if (firstDate != null && secondDate != null) {
			diff = secondDate.getTime() - firstDate.getTime();
		}
		return diff;
	}

	/**
	 * Gets the date diff.
	 *
	 * @param firstDate  the first date
	 * @param secondDate the second date
	 * @return the date diff
	 */
	public static long getDateDiff(String firstDate, String secondDate) {
		return getDateDiff(getDate(firstDate), getDate(secondDate));
	}

	/**
	 * Gets the date diff.
	 *
	 * @param format     the format
	 * @param firstDate  the first date
	 * @param secondDate the second date
	 * @return the date diff
	 */
	public static long getDateDiff(String format, String firstDate, String secondDate) {
		return getDateDiff(getDate(format, firstDate), getDate(format, secondDate));
	}

	/**
	 * Gets the day diff.
	 *
	 * @param firstDate  the first date
	 * @param secondDate the second date
	 * @return days difference between two dates. If first date is Jan first 2018
	 *         and secord date is Jan second 2018, it will return 1
	 */
	public static int getDayDiff(Date firstDate, Date secondDate) {
		int diff = -99999999;
		if (firstDate != null && secondDate != null) {
			final DateTime firstDateTime = new DateTime(firstDate, APPL_TZ);
			final DateTime secondDateTime = new DateTime(secondDate, APPL_TZ);
			diff = Days.daysBetween(firstDateTime, secondDateTime).getDays();
		}
		return diff;
	}

	/**
	 * Gets the day diff.
	 *
	 * @param firstDate  the first date
	 * @param secondDate the second date
	 * @return the day diff
	 */
	public static int getDayDiff(String firstDate, String secondDate) {
		return getDayDiff(getDate(firstDate), getDate(secondDate));
	}

	/**
	 * Gets the day diff.
	 *
	 * @param format     the format
	 * @param firstDate  the first date
	 * @param secondDate the second date
	 * @return the day diff
	 */
	public static int getDayDiff(String format, String firstDate, String secondDate) {
		return getDayDiff(getDate(format, firstDate), getDate(format, secondDate));
	}

	/**
	 * <p>
	 * getDayDiff.
	 * </p>
	 *
	 * @param format     a {@link java.lang.String} object.
	 * @param firstDate  a {@link java.util.Date} object.
	 * @param secondDate a {@link java.util.Date} object.
	 * @return a int.
	 */
	public static int getDayDiff(String format, Date firstDate, Date secondDate) {
		return getDayDiff(toString(format, firstDate), toString(format, secondDate));
	}

	/**
	 * Gets the month diff.
	 *
	 * @param firstDate  the first date
	 * @param secondDate the second date
	 * @return the month diff
	 */
	public static int getMonthDiff(Date firstDate, Date secondDate) {
		int diff = -99999999;
		if (firstDate != null && secondDate != null) {
			final DateTime firstDateTime = new DateTime(firstDate, APPL_TZ);
			final DateTime secondDateTime = new DateTime(secondDate, APPL_TZ);
			diff = Months.monthsBetween(firstDateTime, secondDateTime).getMonths();
		}
		return diff;
	}

	/**
	 * Gets the month diff.
	 *
	 * @param firstDate  the first date
	 * @param secondDate the second date
	 * @return the month diff
	 */
	public static int getMonthDiff(String firstDate, String secondDate) {
		return getMonthDiff(getDate(firstDate), getDate(secondDate));
	}

	/**
	 * Gets the month diff.
	 *
	 * @param format     the format
	 * @param firstDate  the first date
	 * @param secondDate the second date
	 * @return the month diff
	 */
	public static int getMonthDiff(String format, String firstDate, String secondDate) {
		return getMonthDiff(getDate(format, firstDate), getDate(format, secondDate));
	}

	/**
	 * Gets the year diff.
	 *
	 * @param firstDate  the first date
	 * @param secondDate the second date
	 * @return the year diff
	 */
	public static int getYearDiff(Date firstDate, Date secondDate) {
		int diff = -99999999;
		if (firstDate != null && secondDate != null) {
			final DateTime firstDateTime = new DateTime(firstDate, APPL_TZ);
			final DateTime secondDateTime = new DateTime(secondDate, APPL_TZ);
			diff = Years.yearsBetween(firstDateTime, secondDateTime).getYears();
		}
		return diff;
	}

	/**
	 * Gets the year diff.
	 *
	 * @param firstDate  the first date
	 * @param secondDate the second date
	 * @return the year diff
	 */
	public static int getYearDiff(String firstDate, String secondDate) {
		return getYearDiff(getDate(firstDate), getDate(secondDate));
	}

	/**
	 * Gets the year diff.
	 *
	 * @param format     the format
	 * @param firstDate  the first date
	 * @param secondDate the second date
	 * @return the year diff
	 */
	public static int getYearDiff(String format, String firstDate, String secondDate) {
		return getYearDiff(getDate(format, firstDate), getDate(format, secondDate));
	}

	/**
	 * Checks if is same day.
	 *
	 * @param date1 the date 1
	 * @param date2 the date 2
	 * @return a boolean.
	 */
	public static boolean isSameDay(Date date1, Date date2) {
		return org.apache.commons.lang3.time.DateUtils.isSameDay(date1, date2);
	}

	/**
	 * <p>
	 * getTimeInSeconds.
	 * </p>
	 *
	 * @return a long.
	 */
	public static long getTimeInSeconds() {
		return new Date().getTime() / 1000;
	}

	/**
	 * <p>
	 * getTimeInSeconds.
	 * </p>
	 *
	 * @param date a {@link java.util.Date} object.
	 * @return a long.
	 */
	public static long getTimeInSeconds(Date date) {
		return date.getTime() / 1000;
	}

	/**
	 * To string.
	 *
	 * @param dt the dt
	 * @return the string
	 */
	public static String toString(Date dt) {
		return toString(SHORT_DATE_FMT, dt);
	}

	/**
	 * Get system date without time.
	 *
	 * @return the date
	 */
	public static Date getDate() {
		return getDate(toString(new Date()));
	}

	/**
	 * Get system date with time.
	 *
	 * @return the date
	 */
	public static Date getDateTime() {
		return getDate(toString(DATE_FMT_WITH_TIME, new Date()));
	}

	/**
	 * To string.
	 *
	 * @param format the format
	 * @param dt     the dt
	 * @return the string
	 */
	public static String toString(String format, Date dt) {
		final DateFormat dateFormat = new SimpleDateFormat(format);
		return dateFormat.format(dt);
	}

	/**
	 * <p>
	 * isValidMillitartTime.
	 * </p>
	 *
	 * @param time a {@link java.lang.Long} object.
	 * @return a boolean.
	 */
	public static boolean isValidMillitartTime(@NonNull Long time) {
		Assert.notNull(time, "Time should not be null");
		return time <= 2400 && time >= 0;
	}

	/**
	 * <p>
	 * getMillitaryTimeFormat.
	 * </p>
	 *
	 * @param time a {@link java.lang.Long} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String getMillitaryTimeFormat(@NonNull Long time) {
		Assert.notNull(time, "Time should not be null");
		return StringUtils.leftPad(String.valueOf(time), 4, '0');
	}

	/**
	 * <p>
	 * getTimeFromMillitaryTime.
	 * </p>
	 *
	 * @param millitaryTime a {@link java.lang.String} object.
	 * @param timeFormat    a {@link java.lang.String} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String getTimeFromMillitaryTime(@NonNull String millitaryTime, @Nullable String timeFormat) {
		Assert.notNull(millitaryTime, "Time should not be null");
		Assert.isTrue(millitaryTime.length() == 4, "Millitary Time Invalid Format. Expected ####");
		Assert.isTrue(isValidMillitartTime(Long.valueOf(millitaryTime)),
				"Invalid Millitary Time - Should be between 0000 - 2400");

		timeFormat = StringUtils.isBlank(timeFormat) ? "hh:mm a" : timeFormat;
		final String hr = millitaryTime.substring(0, 2);
		final String min = millitaryTime.substring(2, 4);

		final Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, Integer.valueOf(hr));
		cal.set(Calendar.MINUTE, Integer.valueOf(min));
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return toString(timeFormat, cal.getTime());
	}

	/**
	 * <p>
	 * addMinutes.
	 * </p>
	 *
	 * @param beforeTime a {@link java.util.Date} object.
	 * @param minutes    a int.
	 * @return a {@link java.util.Date} object.
	 */
	public static Date addMinutes(Date beforeTime, int minutes) {
		final Date dateToAdd = beforeTime == null ? new Date() : beforeTime;
		final long ONE_MINUTE_IN_MILLIS = 60000;// millisecs
		final long curTimeInMs = dateToAdd.getTime();
		final Date afterAddingMins = new Date(curTimeInMs + minutes * ONE_MINUTE_IN_MILLIS);
		return afterAddingMins;
	}

	/**
	 * <p>
	 * getDayNameOfWeek.
	 * </p>
	 *
	 * @param date a {@link java.util.Date} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String getDayNameOfWeek(@NonNull Date date) {
		try {
			final Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			final int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
			return getDayNameOfWeek(dayOfWeek);
		} catch (final Exception e) {
			return "";
		}
	}

	/**
	 * <p>
	 * getDayNameOfWeek.
	 * </p>
	 *
	 * @param calendayDayOfWeek a int.
	 * @return a {@link java.lang.String} object.
	 */
	public static String getDayNameOfWeek(int calendayDayOfWeek) {
		String day = "";
		switch (calendayDayOfWeek) {
		case 1:
			day = "Sunday";
			break;
		case 2:
			day = "Monday";
			break;
		case 3:
			day = "Tuesday";
			break;
		case 4:
			day = "Wednesday";
			break;
		case 5:
			day = "Thursday";
			break;
		case 6:
			day = "Friday";
			break;
		case 7:
			day = "Saturday";
			break;
		default:
			day = "";
		}
		return day;
	}

	/**
	 * <p>
	 * addToDate.
	 * </p>
	 *
	 * @param date          a {@link java.util.Date} object.
	 * @param dateAttribute a int.
	 * @param value         a int.
	 * @return a {@link java.util.Date} object.
	 */
	public static Date addToDate(Date date, int dateAttribute, int value) {
		if (date != null) {
			final Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			cal.add(dateAttribute, value);
			return cal.getTime();
		}
		return date;
	}

	/**
	 * <p>
	 * getFormattedDate to get day in ordinal number like August, 23rd 2018
	 * </p>
	 *
	 * @param date a {@link java.util.Date} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String getFormattedDate(Date date) {
		final Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		final int day = cal.get(Calendar.DATE);

		if (!(day > 10 && day < 19)) {
			switch (day % 10) {
			case 1:
				return new SimpleDateFormat("MMMM d'st', yyyy").format(date);
			case 2:
				return new SimpleDateFormat("MMMM d'nd', yyyy").format(date);
			case 3:
				return new SimpleDateFormat("MMMM d'rd', yyyy").format(date);
			default:
				return new SimpleDateFormat("MMMM d'th', yyyy").format(date);
			}
		}
		return new SimpleDateFormat("MMMM d'th', yyyy").format(date);
	}

	/**
	 * <p>
	 * getDatesInRange.
	 * </p>
	 *
	 * @param startDate a {@link java.util.Date} object.
	 * @param endDate   a {@link java.util.Date} object.
	 * @return a {@link java.util.List} object.
	 */
	public static List<Date> getDatesInRange(Date startDate, Date endDate) {
		final List<Date> period = new ArrayList<>();

		if (getDayDiff(startDate, endDate) <= 0) {
			return period;
		}

		Date currentDate = startDate;
		period.add(currentDate);
		while (true) {
			if (getDayDiff(currentDate, endDate) <= 0) {
				break;
			}

			currentDate = addDays(currentDate, 1);
			period.add(currentDate);
		}

		return period;
	}

	/**
	 * <p>
	 * isDateInBetween.
	 * </p>
	 *
	 * @param checkDate a {@link java.util.Date} object.
	 * @param fromDate  a {@link java.util.Date} object.
	 * @param toDate    a {@link java.util.Date} object.
	 * @return a boolean.
	 */
	public static boolean isDateInBetween(Date checkDate, Date fromDate, Date toDate) {
		boolean isDateInBetween = false;
		final long fromDateDiff = getDateDiff(fromDate, checkDate);
		final long toDateDiff = getDateDiff(checkDate, toDate);

		if (fromDateDiff >= 0 && toDateDiff >= 0) {
			isDateInBetween = true;
		}

		return isDateInBetween;
	}

	/**
	 * <p>
	 * getLastDayOfMonth.
	 * </p>
	 *
	 * @param yearMonthDate a {@link java.lang.String} date in yyyy-MM-dd format.
	 * @return a {@link java.util.Date} object. It will be null date for invalid
	 *         date passed.
	 */
	public static Date getLastDayOfMonth(String yearMonthDate) {
		final Date dt = DateUtils.getDate(yearMonthDate);
		if (dt != null) {
			return getLastDayOfMonth(dt);
		}
		return null;

	}

	/**
	 * <p>
	 * getLastDayOfMonth.
	 * </p>
	 *
	 * @param checkDate a {@link java.util.Date} object. If check date is null,
	 *                  current date will be picked.
	 * @return a {@link java.util.Date} object
	 */
	public static Date getLastDayOfMonth(Date checkDate) {
		final Calendar calendar = Calendar.getInstance();
		if (checkDate == null) {
			calendar.setTime(DateUtils.getDate());
		} else {
			calendar.setTime(checkDate);
		}
		calendar.add(Calendar.MONTH, 1);
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		calendar.add(Calendar.DATE, -1);
		return calendar.getTime();
	}

	public static Date getDateWithZeroTime(Date date) {
		return DateUtils.getDate(DateUtils.DATE_FMT_WITH_TIME, DateUtils.toString(date) + " 00:00:00");
	}

	public static Date getDateWithMidNightTime(Date date) {
		return DateUtils.getDate(DateUtils.DATE_FMT_WITH_TIME, DateUtils.toString(date) + " 23:59:59");
	}

	/**
	 * Instantiates a new date utils.
	 */
	private DateUtils() {
	}

}
