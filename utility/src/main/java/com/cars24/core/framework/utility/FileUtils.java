package com.cars24.core.framework.utility;

import org.apache.tika.Tika;
import org.springframework.web.multipart.MultipartFile;

/**
 * <p>
 * FileUtils class.
 * </p>
 * 
 *
 * @author Sohan
 */
public class FileUtils {

	public static boolean getFileContentType(MultipartFile file) {
		Tika tika = new Tika();
		String detectMime = "";
		try {
			detectMime = tika.detect(file.getBytes());
		} catch (Exception e) {
		}
		return detectMime.equalsIgnoreCase(file.getContentType());

	}

	public static String getFileContentType(byte[] bytes) {
		Tika tika = new Tika();
		return tika.detect(bytes);
	}

	public static boolean validMimeType(byte[] bytes, String currentFileContent) {
		Tika tika = new Tika();
		String detectMime = "";
		detectMime = tika.detect(bytes);
		return detectMime.equalsIgnoreCase(currentFileContent);

	}
}
