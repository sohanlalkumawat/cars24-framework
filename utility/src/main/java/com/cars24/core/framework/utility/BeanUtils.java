
package com.cars24.core.framework.utility;

import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;

/**
 * The Class BeanUtils.
 * @author Sohan
 */
public class BeanUtils {

    /**
     * Instantiates a new bean utils.
     */
    private BeanUtils() {}

    /**
     * Populate the JavaBeans properties of the specified bean, based on the specified name/value pairs.
     *
     * @param bean the bean
     * @param properties the properties
     * @throws java.lang.IllegalAccessException if any.
     * @throws java.lang.reflect.InvocationTargetException if any.
     */
    public static void populate(Object bean, Map<String, ? extends Object> properties) throws IllegalAccessException, InvocationTargetException {
        org.apache.commons.beanutils.BeanUtils.populate(bean, properties);
    }

    /**
     * Populate the JavaBeans properties of the specified bean, based on the specified name/value pairs.
     *
     * @param src the src
     * @param target the target
     * @param properties the properties
     * @throws java.lang.IllegalAccessException if any.
     * @throws java.lang.reflect.InvocationTargetException if any.
     */
    public static void copyProperties(Object src, Object target, Set<String> properties) throws IllegalAccessException, InvocationTargetException {
        String[] ignorePropertyNames = Arrays.stream(org.springframework.beans.BeanUtils.getPropertyDescriptors(src.getClass()))
                .map(PropertyDescriptor::getName).filter((key) -> {
                    return properties == null || !properties.contains(key);
                }).toArray(String[]::new);

        org.springframework.beans.BeanUtils.copyProperties(src, target, ignorePropertyNames);
    }
    
    /**
     * <p>copyProperties.</p>
     *
     * @param src a {@link java.lang.Object} object.
     * @param target a {@link java.lang.Object} object.
     * @throws java.lang.IllegalAccessException if any.
     * @throws java.lang.reflect.InvocationTargetException if any.
     */
    public static void copyProperties(Object src, Object target) throws IllegalAccessException, InvocationTargetException {
        org.springframework.beans.BeanUtils.copyProperties(src, target, "");
    }
}
