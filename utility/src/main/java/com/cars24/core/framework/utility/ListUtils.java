/*
 * @author Sohan
 * Copyright cars24.com
 */
package com.cars24.core.framework.utility;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Utility Class ListUtils.
 *
 * @author Sohan
 * @version $Id: $Id
 */
public class ListUtils {

	/**
	 * Instantiates a new list utils.
	 */
	private ListUtils() {

	}

	/**
	 * List to string.
	 *
	 * @param list
	 *            the list
	 * @return the string
	 */
	public static String listToString(List<String> list) {

		String returnValue = "";
		if (list != null) {
			String[] data = new String[list.size()];
			list.toArray(data);
			returnValue = StringUtils.csvJoin(data);
		}
		return returnValue;
	}

	/**
	 * <p>longListToString.</p>
	 *
	 * @param list a {@link java.util.List} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String longListToString(List<Long> list) {

		String returnValue = "";
		if (list != null) {
			String[] data = new String[list.size()];

			for (int cnt = 0; cnt < list.size(); cnt++) {
				data[cnt] = String.valueOf(list.get(cnt));
			}
			returnValue = StringUtils.csvJoin(data);
		}
		return returnValue;
	}

	/**
	 * String to list.
	 *
	 * @param strings
	 *            the strings
	 * @return the list
	 */
	public static List<String> stringToList(String strings) {
		if (strings != null) {
			return new ArrayList<>(Arrays.asList(StringUtils.csvSplit(strings)));
		} else {
			return null;
		}
	}

	/**
	 * <p>stringToLongList.</p>
	 *
	 * @param strings a {@link java.lang.String} object.
	 * @return a {@link java.util.List} object.
	 */
	public static List<Long> stringToLongList(String strings) {
		if (strings != null) {
			String[] strArr = StringUtils.csvSplit(strings);
			Long[] longArr = new Long[strArr.length];
			for (int cnt = 0; cnt < strArr.length; cnt++) {
				longArr[cnt] = Long.valueOf(strArr[cnt]);
			}

			return new ArrayList<>(Arrays.asList(longArr));
		} else {
			return null;
		}
	}
}
