
package com.cars24.core.framework.websupport.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.validation.Errors;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.validation.beanvalidation.SpringValidatorAdapter;

import com.cars24.core.framework.common.constants.CommonConstants;
import com.cars24.core.framework.common.exception.ValidationException;
import com.cars24.core.framework.common.utils.ContextUtils;
import com.cars24.core.framework.utility.StringUtils;

/**
 * The Interface BaseController.
 *
 * @author Sohan
 * @version $Id: $Id
 */
public interface BaseController {

	/**
	 * <p>
	 * getSpringErrors.
	 * </p>
	 *
	 * @param paramValue a {@link java.lang.Object} object.
	 * @return a {@link org.springframework.validation.BindingResult} object.
	 */
	default BindingResult getSpringErrors(Object paramValue) {
		DataBinder dataBinder = new DataBinder(paramValue, paramValue.getClass().getSimpleName());
		dataBinder.addValidators(new SpringValidatorAdapter(ContextUtils.getBean(LocalValidatorFactoryBean.class)));
		dataBinder.validate(CommonConstants.NO_ARGS);
		return dataBinder.getBindingResult();
	}

	default List<Long> idsStringToList(String idStrs) {
		List<Long> ids = new ArrayList<>();
		try {
			String[] idArr = idStrs.split(",");
			ids = Arrays.asList(idArr).stream().map(id -> Long.valueOf(id)).collect(Collectors.toList());
		} catch (Exception ex) {
			throw new ValidationException(CommonConstants.ILLEGAL_PARAM);
		}
		return ids;
	}

	default void checkError(Errors errors) {
		if (errors.hasErrors()) {
			throw new ValidationException(CommonConstants.VALIDATION_ERROR_MSG, errors);
		}
	}

	default List<String> resolveStringToList(String childObjects) {
		List<String> childObjectList = new ArrayList<>();
		if (StringUtils.isNotBlank(childObjects)) {
			String[] childObjectArr = childObjects.split(",");
			childObjectList = Arrays.asList(childObjectArr);
		}
		return childObjectList;
	}
}
