
package com.cars24.core.framework.websupport.swagger;

import java.io.Serializable;

import lombok.Data;

/**
 * The Class SwaggerApiInfo.
 *
 * @author Sohan
 */
@Data
public class SwaggerApiInfo implements Serializable {

	private static final long serialVersionUID = -3839350191005781366L;

	public static final String SPRING_APPLICATION_NAME = "spring.application.name";

	private String version;

	private String title = System.getProperty(SPRING_APPLICATION_NAME) == null ? "Cars24 Service"
			: System.getProperty(SPRING_APPLICATION_NAME);

	private String description = "Swagger api";

	private String termsOfServiceUrl = "http://cars24.com";

	private String license = "@cars24.com";

	private String licenseUrl = "http://cars24.com";

	private SwaggerContact contact = new SwaggerContact();

	@Data
	public static class SwaggerContact implements Serializable {

		private static final long serialVersionUID = 4322223407299505306L;

		private String name = "Cars24";

		private String url = "http://cars24.com";

		private String email = "it.support@cars24.com";

	}

}
