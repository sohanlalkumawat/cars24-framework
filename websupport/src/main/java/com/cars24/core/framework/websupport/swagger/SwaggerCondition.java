package com.cars24.core.framework.websupport.swagger;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

import com.cars24.core.framework.common.constants.CommonConstants;

public class SwaggerCondition implements Condition {

	@Override
	public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
		String swaggerCondition = context.getEnvironment().getProperty(CommonConstants.SWAGGER_ENABLE_CONDITION);
		return swaggerCondition == null ? true : swaggerCondition.equalsIgnoreCase("y");
	}

}
