
package com.cars24.core.framework.websupport.exception;

import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.cars24.core.framework.common.bean.ErrorBean;
import com.cars24.core.framework.common.bean.ErrorResponse;
import com.cars24.core.framework.common.bean.Errors;
import com.cars24.core.framework.common.exception.BusinessException;
import com.cars24.core.framework.common.exception.ConfigurationException;
import com.cars24.core.framework.common.exception.NoDataException;
import com.cars24.core.framework.common.exception.ServerException;
import com.cars24.core.framework.common.exception.ValidationException;
import com.cars24.core.framework.common.utils.RequestValidationUtils;
import com.cars24.core.framework.utility.StringUtils;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;

/**
 * The Class RestExceptionAdvice.
 *
 * @author Sohan
 * @version $Id: $Id
 */
@RestControllerAdvice
public class RestExceptionAdvice {

	/** The Constant _LOGGER. */
	private static final Logger _LOGGER = LoggerFactory.getLogger(RestExceptionAdvice.class);

	/**
	 * Prepare server exception.
	 *
	 * @param ex the ex
	 * @return the error response
	 */
	@ExceptionHandler(ServerException.class)
	@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
	public ErrorResponse prepareServerException(ServerException ex) {
		_LOGGER.error(ex.getMessage(), ex);
		ErrorResponse response = ErrorResponse.create().message(ex.getMessage()).addError(ex.getError());
		return logAndReturn(response, ex);
	}

	/**
	 * Prepare validation exception.
	 *
	 * @param ex the ex
	 * @return the error response
	 */
	@ExceptionHandler(ValidationException.class)
	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	public ErrorResponse prepareValidationException(ValidationException ex) {
		_LOGGER.error(ex.getMessage(), ex);
		ErrorResponse response = RequestValidationUtils.prepareValidationResponse(ex);
		return logAndReturn(response, ex);
	}

	/**
	 * Prepare business exception.
	 *
	 * @param ex the ex
	 * @return the error response
	 */
	@ExceptionHandler(BusinessException.class)
	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	public ErrorResponse prepareBusinessException(BusinessException ex) {
		_LOGGER.error(ex.getMessage(), ex);
		ErrorResponse response = ErrorResponse.create().message(ex.getMessage()).addError(ex.getError());
		return logAndReturn(response, ex);
	}

	/**
	 * Prepare no data exception.
	 *
	 * @param ex the ex
	 * @return the error response
	 */
	@ExceptionHandler(NoDataException.class)
	@ResponseStatus(code = HttpStatus.NOT_FOUND)
	public ErrorResponse prepareNoDataException(NoDataException ex) {
		_LOGGER.error(ex.getMessage(), ex);
		ErrorResponse response = ErrorResponse.create().message(ex.getMessage()).addError(ex.getError());
		return logAndReturn(response, ex);
	}

	/**
	 * Handle exception.
	 *
	 * @param exception the exception
	 * @return the error response
	 */
	@ExceptionHandler(ConfigurationException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public ErrorResponse handleException(ConfigurationException exception) {
		_LOGGER.error(exception.getMessage(), exception);
		ErrorResponse response = ErrorResponse.create().message("Configuration Error")
				.addError(ErrorBean.withError(Errors.SERVER_EXCEPTION));
		return logAndReturn(response, exception);
	}

	/**
	 * Handle exception.
	 *
	 * @param exception the exception
	 * @return the error response
	 */
	@ExceptionHandler(IllegalArgumentException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ErrorResponse handleException(IllegalArgumentException exception) {
		_LOGGER.error(exception.getMessage(), exception);
		ErrorResponse response = ErrorResponse.create().message(exception.getMessage())
				.addError(ErrorBean.withError(Errors.UNSUPPORTED_DB_OPERATION));
		return logAndReturn(response, exception);
	}

	/**
	 * Handle exception.
	 *
	 * @param exception the exception
	 * @return the error response
	 */
	@ExceptionHandler(NumberFormatException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ErrorResponse handleException(NumberFormatException exception) {
		_LOGGER.error(exception.getMessage(), exception);
		ErrorResponse response = ErrorResponse.create().message(exception.getMessage())
				.addError(ErrorBean.withError(Errors.INVALID_NUMBER));
		return logAndReturn(response, exception);
	}

	/**
	 * Handle exception.
	 *
	 * @param exception the exception
	 * @return the error response
	 */
	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ErrorResponse handleException(MethodArgumentNotValidException exception) {
		_LOGGER.error(exception.getMessage(), exception);
		ErrorResponse response = ErrorResponse.create().message("Validation Errors")
				.errors(exception.getBindingResult().getFieldErrors().parallelStream()
						.map(error -> RequestValidationUtils.prepareError(error)).collect(Collectors.toList()));
		return logAndReturn(response, exception);
	}

	/**
	 * Handle exception.
	 *
	 * @param exception the exception
	 * @return the error response
	 */
	@ExceptionHandler({ InvalidFormatException.class, JsonMappingException.class })
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ErrorResponse handleException(JsonMappingException exception) {
		_LOGGER.error(exception.getMessage(), exception);
		String message = StringUtils.isBlank(exception.getOriginalMessage())
				? ErrorBean.withError(Errors.INVALID_REQUEST_PAYLOAD).getMessage()
				: exception.getOriginalMessage();
		ErrorResponse response = ErrorResponse.create()
				.message(ErrorBean.withError(Errors.INVALID_REQUEST_PAYLOAD).getMessage())
				.addError(ErrorBean.withError(Errors.INVALID_REQUEST_PAYLOAD, message, exception.getPathReference()));
		return logAndReturn(response, exception);
	}

	/**
	 * Handle exception.
	 *
	 * @param exception the exception
	 * @return the error response
	 */
	@ExceptionHandler({ JsonParseException.class })
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ErrorResponse handleException(JsonParseException exception) {
		_LOGGER.error(exception.getMessage(), exception);
		ErrorResponse response = ErrorResponse.create()
				.message(ErrorBean.withError(Errors.INVALID_REQUEST_PAYLOAD).getMessage())
				.addError(ErrorBean.withError(Errors.INVALID_REQUEST_PAYLOAD, exception.getMessage()));
		return logAndReturn(response, exception);
	}

	public ErrorResponse logAndReturn(ErrorResponse response, Exception ex) {
		try {
			ExceptionLogging.logAndReturn(response, ex);
		} catch (Exception e) {
			_LOGGER.warn(e.getMessage());
		}
		return response;
	}
}
