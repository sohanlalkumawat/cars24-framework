
package com.cars24.core.framework.websupport.exception;

import org.hibernate.JDBCException;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.exception.JDBCConnectionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.jdbc.UncategorizedSQLException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.cars24.core.framework.common.bean.ErrorBean;
import com.cars24.core.framework.common.bean.ErrorResponse;
import com.cars24.core.framework.common.bean.Errors;

/**
 * The Class RestExceptionAdvice.
 *
 * @author Sohan
 * @version $Id: $Id
 */
@RestControllerAdvice
public class JDBCExceptionAdvice extends RestExceptionAdvice {

	/** The Constant _LOGGER. */
	private static final Logger _LOGGER = LoggerFactory.getLogger(JDBCExceptionAdvice.class);

	/**
	 * Handle exception.
	 *
	 * @param exception the exception
	 * @return the error response
	 */
	@ExceptionHandler(JDBCConnectionException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ErrorResponse handleException(JDBCConnectionException exception) {
		_LOGGER.error(exception.getMessage(), exception);
		ErrorResponse response = ErrorResponse.create().message(exception.getMessage())
				.addError(new ErrorBean(Errors.JDBC_CONNECTION_ERROR, Errors.JDBC_CONNECTION_ERROR));
		return logAndReturn(response, exception.getSQLException());
	}

	/**
	 * Handle exception.
	 *
	 * @param exception the exception
	 * @return the error response
	 */
	@ExceptionHandler(JDBCException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ErrorResponse handleException(JDBCException exception) {
		_LOGGER.error(exception.getMessage() + " SQL : " + exception.getSQL(), exception);
		ErrorResponse response = ErrorResponse.create()
				.message(exception.getMessage() + ". SQL : " + exception.getSQL())
				.addError(new ErrorBean(Errors.JDBC_GRAMMER_ERROR, Errors.JDBC_GRAMMER_ERROR));
		return logAndReturn(response, exception.getSQLException());
	}

	/**
	 * <p>
	 * handleException.
	 * </p>
	 *
	 * @param exception a
	 *                  {@link org.hibernate.exception.ConstraintViolationException}
	 *                  object.
	 * @return a {@link com.cars24.core.framework.common.bean.ErrorResponse} object.
	 */
	@ExceptionHandler(ConstraintViolationException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ErrorResponse handleException(ConstraintViolationException exception) {
		_LOGGER.error(exception.getMessage(), exception);
		ErrorResponse response = ErrorResponse.create().message(exception.getMessage()).addError(ErrorBean.withError(
				Errors.JDBC_CONSTRAINT_ERROR,
				exception.getSQLException() != null ? exception.getSQLException().getMessage() : exception.getMessage(),
				null));
		return logAndReturn(response, exception);
	}

	@ExceptionHandler({ BadSqlGrammarException.class })
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ErrorResponse handleException(BadSqlGrammarException exception) {
		_LOGGER.error(exception.getMessage(), exception);
		ErrorResponse response = ErrorResponse.create()
				.message(ErrorBean.withError(Errors.JDBC_GRAMMER_ERROR).getMessage())
				.addError(ErrorBean.withError(Errors.JDBC_GRAMMER_ERROR, exception.getMessage()));
		return logAndReturn(response, exception);
	}

	@ExceptionHandler({ UncategorizedSQLException.class })
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ErrorResponse uncategorizedSQLException(UncategorizedSQLException exception) {
		_LOGGER.error(exception.getMessage(), exception);
		ErrorResponse response = ErrorResponse.create()
				.message(ErrorBean.withError(Errors.JDBC_GRAMMER_ERROR).getMessage())
				.addError(ErrorBean.withError(Errors.JDBC_GRAMMER_ERROR, exception.getMessage()));
		return logAndReturn(response, exception);
	}

	@ExceptionHandler({ DataAccessException.class })
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ErrorResponse uncategorizedSQLException(DataAccessException exception) {
		_LOGGER.error(exception.getMessage(), exception);
		ErrorResponse response = ErrorResponse.create().message(exception.getMessage())
				.addError(ErrorBean.withError(Errors.JDBC_DATA_ACCESS_ERROR, exception.getMessage()));
		return logAndReturn(response, exception);
	}
}
