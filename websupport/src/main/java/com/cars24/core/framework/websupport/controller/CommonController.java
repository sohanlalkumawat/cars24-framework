/*
 * @author Sohan
 * Copyright cars24.com
 */
package com.cars24.core.framework.websupport.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

/**
 * The Class CommonController.
 *
 * @author Sohan
 * @version $Id: $Id
 */
@RestController
@ApiIgnore
@Api(tags = "Common Services (Internal Only)", description = "Internal set of services inherited by default and available for all service nodes")
public class CommonController {

	/**
	 * Health.
	 *
	 * @return the string
	 */
	@ApiOperation(hidden = true, value = "Global health check handler", nickname = "checkHealth")
	@GetMapping(value = "/status")
	public String health() {
		return "HEALTHY";
	}

}
