package com.cars24.core.framework.websupport.exception;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.cars24.core.framework.common.bean.ErrorResponse;
import com.cars24.core.framework.common.constants.CommonConstants;
import com.cars24.core.framework.common.utils.ContextUtils;
import com.cars24.core.framework.logging.bean.ActionErrorLog;
import com.cars24.core.framework.logging.utils.LoggingUtils;
import com.cars24.core.framework.utility.JSONUtils;

@Component
public class ExceptionLogging {

	private static final Logger _LOGGER = LoggerFactory.getLogger(ExceptionLogging.class);

	public static boolean loggingStatus = false;

	public static ErrorResponse logAndReturn(ErrorResponse response, Exception ex) {
		try {
			if (loggingStatus) {
				ActionErrorLog errorLog = ActionErrorLog.newInstance();
				errorLog.setErrors(response.getErrors());
				errorLog.setErrorMessage(response.getMessage());
				LoggingUtils.addActionError(errorLog);
			} else {
				_LOGGER.info("ExceptionLogging - " + JSONUtils.objectToPrettyJson(response.getErrors()));
			}
		} catch (Exception e) {
			_LOGGER.warn(e.getMessage());
		}
		return response;
	}

	@PostConstruct
	private void postInit() {
		String loggingCondition = ContextUtils.getEnvironment().getProperty(CommonConstants.LOGGING_ENABLE_CONDITION);
		loggingStatus = loggingCondition == null ? true : loggingCondition.equalsIgnoreCase("y");
	}

}
