package com.cars24.core.framework.websupport.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.cars24.core.framework.common.constants.CommonConstants;

/**
 * The Class RestCorsConfig.
 *
 * @author Sohan
 */
@Configuration
public class RestCorsConfig implements WebMvcConfigurer {

	@Autowired
	Environment env;

	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/**")
				.allowedOrigins((env.getProperty(CommonConstants.APPLICATION_ALLOW_ORIGIN) == null
						|| env.getProperty(CommonConstants.APPLICATION_ALLOW_ORIGIN).trim().length() == 0) ? "*"
								: env.getProperty(CommonConstants.APPLICATION_ALLOW_ORIGIN))
				.allowedHeaders("*").allowedMethods("*");
	}

}
