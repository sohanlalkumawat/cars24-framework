/*
 * @author Sohan
 * Copyright cars24.com
 */
package com.cars24.core.framework.validator.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

import com.cars24.core.framework.validator.provider.IsPhoneValidator;

/**
 * The Interface IsPhone.
 *
 * @author Sohan
 * @version $Id: $Id
 */
@Documented
@Constraint(validatedBy = IsPhoneValidator.class)
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface IsPhone {

	/**
	 * Message.
	 *
	 * @return the string
	 */
	String message() default "Invalid phone format/number";

	/**
	 * Groups.
	 *
	 * @return the class[]
	 */
	Class<?>[] groups() default {};

	/**
	 * Payload.
	 *
	 * @return the Payload[]
	 */
	Class<? extends Payload>[] payload() default {};

	/**
	 * Pattern.
	 *
	 * @return the string
	 */
	String pattern();
}
