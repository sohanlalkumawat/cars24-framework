package com.cars24.core.framework.validator.provider.utils;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;

import org.springframework.core.io.ByteArrayResource;

import com.cars24.core.framework.common.constants.CommonFormats;
import com.cars24.core.framework.utility.CollectionUtils;
import com.cars24.core.framework.utility.DateUtils;
import com.cars24.core.framework.utility.StringUtils;

/**
 * The Class ValidationUtils.
 *
 * @author Sohan
 */
public class ValidationUtils {

	private static final NumberFormat NO_EXPONENT_NUMBER_FORMAT = getNoExponentNumberFormat();

	private static final NumberFormat getNoExponentNumberFormat() {
		final NumberFormat numberFormat = NumberFormat.getInstance();
		numberFormat.setMaximumFractionDigits(Integer.MAX_VALUE);
		numberFormat.setGroupingUsed(false);
		return numberFormat;
	}

	/**
	 * Instantiates a new validation utils.
	 */
	private ValidationUtils() {
	}

	/**
	 * Checks if is alphanumeric.
	 *
	 * @param value     the value
	 * @param withChars the with chars
	 * @return a boolean.
	 */
	public static boolean isAlphanumeric(String value, char[] withChars) {
		if (withChars.length > 0) {
			final StringBuilder regex = new StringBuilder("A-Z0-9a-z");
			for (final char c : withChars) {
				regex.append(c);
			}
			final String pattern = "^[" + regex.toString() + "]+$";
			return StringUtils.isBlank(value) || Pattern.matches(pattern, value);
		} else {
			return StringUtils.isBlank(value) || Pattern.matches(CommonFormats.ALPHANUMERIC, value);
		}
	}

	/**
	 * Checks if is valid date format.
	 *
	 * @param value   the value
	 * @param pattern the pattern
	 * @return a boolean.
	 */
	public static boolean isValidDateFormat(String value, String pattern) {
		try {
			if (!StringUtils.isBlank(value)) {
				final Date dt = DateUtils.getDate(pattern, value);

				if (dt == null) {
					return false;
				} else {
					return DateUtils.toString(pattern, dt).equals(value);
				}
			}
			return true;
		} catch (final Exception e) {
			return false;
		}
	}

	/**
	 * Checks if is valid decimal format.
	 *
	 * @param value   the value
	 * @param pattern the pattern
	 * @return a boolean.
	 */
	public static boolean isValidDecimalFormat(Number value, String pattern) {
		boolean returnValue = true;
		try {
			if (value != null) {
				final String[] patternSplit = pattern.split("[.]");
				final String numValue = NO_EXPONENT_NUMBER_FORMAT.format(value.doubleValue());
				final String patternCompile = String.format("[0-9]{0,%s}[.]{0,1}[0-9]{0,%s}", patternSplit[0].length(),
						patternSplit[1].length());
				returnValue = Pattern.compile(patternCompile).matcher(numValue).matches();
			}
			return returnValue;
		} catch (final Exception e) {
			return false;
		}
	}

	/**
	 * Checks if is valid emails.
	 *
	 * @param values      the values
	 * @param emailFormat the email format
	 * @return a boolean.
	 */
	public static boolean isValidEmails(Collection<String> values, String emailFormat) {
		if (CollectionUtils.isEmpty(values)) {
			return true;
		}

		boolean isValid = true;
		for (final String value : values) {
			isValid = isValidEmail(value, emailFormat);

			if (isValid == false) {
				return isValid;
			}
		}
		return true;
	}

	/**
	 * Checks if is valid email.
	 *
	 * @param value       the value
	 * @param emailFormat the email format
	 * @return a boolean.
	 */
	public static boolean isValidEmail(String value, String emailFormat) {
		if (StringUtils.isEmpty(emailFormat) || StringUtils.isEmpty(value)) {
			return false;
		}
		return Pattern.matches(emailFormat, value);
	}

	public static boolean isGeneralPancard(String value, String generalPancard) {
		if (StringUtils.isEmpty(generalPancard) || StringUtils.isEmpty(value)) {
			return false;
		}
		return Pattern.matches(generalPancard, value);
	}

	public static boolean isPersonPancard(String value, String personPancard) {
		if (StringUtils.isEmpty(personPancard) || StringUtils.isEmpty(value)) {
			return false;
		}
		return Pattern.matches(personPancard, value);
	}

	/**
	 * Checks if is valid file extension.
	 *
	 * @param value   the value
	 * @param formats the formats
	 * @return a boolean.
	 */
	public static boolean isValidFileExtension(String value, String[] formats) {
		if (StringUtils.isBlank(value)) {
			return true;
		}

		boolean hasMatch = false;
		if (formats.length > 0) {
			hasMatch = Arrays.asList(formats).stream()
					.anyMatch(v -> value.toLowerCase().endsWith("." + v.toLowerCase()));
		}
		return hasMatch;
	}

	/**
	 * Checks if is valid file extension.
	 *
	 * @param formats  the formats
	 * @param fileName a {@link java.lang.String} object.
	 * @return a boolean.
	 */
	public static boolean isValidFileType(String fileName, String[] formats) {
		if (StringUtils.isBlank(fileName)) {
			return true;
		}

		boolean hasMatch = false;
		if (formats.length > 0) {
			hasMatch = Arrays.asList(formats).stream()
					.anyMatch(v -> fileName.toLowerCase().endsWith("." + v.toLowerCase()));
		}
		return hasMatch;
	}

	/**
	 * Checks if is valid file size.
	 *
	 * @param file    a {@link org.springframework.core.io.ByteArrayResource}
	 *                object.
	 * @param minSize a int.
	 * @param maxSize a int.
	 * @return a boolean.
	 */
	public static boolean isValidFileSize(ByteArrayResource file, int minSize, int maxSize) {
		boolean hasMatch = false;

		if (file == null) {
			return true;
		}

		final int minSizeN = minSize;
		final int maxSizeN = maxSize * 1024;

		// _LOGGER.info("File Size: " + file.contentLength());
		if (file.contentLength() >= minSizeN && file.contentLength() <= maxSizeN) {
			hasMatch = true;
		}

		return hasMatch;
	}

	/**
	 * Checks if is valid file dimension.
	 *
	 * @param file a {@link org.springframework.core.io.ByteArrayResource} object.
	 * @throws java.io.IOException
	 * @param minWidth  a int.
	 * @param minHeight a int.
	 * @param maxWidth  a int.
	 * @param maxHeight a int.
	 * @return a boolean.
	 */
	public static boolean isValidFileDimension(ByteArrayResource file, int minWidth, int minHeight, int maxWidth,
			int maxHeight) throws IOException {
		boolean hasMatch = false;

		if (file == null) {
			return true;
		}

		final BufferedImage image = ImageIO.read(file.getInputStream());
		if (image.getWidth() >= minWidth && image.getWidth() <= maxWidth && image.getHeight() >= minHeight
				&& image.getHeight() <= maxHeight) {
			hasMatch = true;
		}

		return hasMatch;
	}

	/**
	 * Checks if is valid file dimension.
	 *
	 * @param file a {@link org.springframework.core.io.ByteArrayResource} object.
	 * @throws java.io.IOException
	 * @param minWidth  a int.
	 * @param minHeight a int.
	 * @return a boolean.
	 */
	public static boolean isValidFileDimension(ByteArrayResource file, int minWidth, int minHeight) throws IOException {
		boolean hasMatch = false;

		if (file == null) {
			return true;
		}

		final BufferedImage image = ImageIO.read(file.getInputStream());
		if (image.getWidth() >= minWidth && image.getHeight() >= minHeight) {
			hasMatch = true;
		}

		return hasMatch;
	}

	/**
	 * Checks if is valid boolean.
	 *
	 * @param value the value
	 * @return a boolean.
	 */
	public static boolean isValidBoolean(String value) {
		return StringUtils.isBlank(value) || "true".equalsIgnoreCase(value) || "false".equalsIgnoreCase(value);
	}

	/**
	 * Checks if is valid number.
	 *
	 * @param value the value
	 * @return a boolean.
	 */
	public static boolean isValidNumber(String value) {
		return StringUtils.isBlank(value) || Pattern.matches("\\d+", value);
	}

	/**
	 * Checks if is valid flag.
	 *
	 * @param value the value
	 * @return a boolean.
	 */
	public static boolean isValidFlag(Integer value) {
		return value == null || value == 1 || value == 0;
	}

	/**
	 * Checks if is valid phones.
	 *
	 * @param values the values
	 * @return a boolean.
	 */
	public static boolean isValidPhones(List<String> values, String pattern) {
		boolean isValid = true;
		for (final String value : values) {
			isValid = isValidPhone(value, pattern);

			if (isValid == false) {
				return isValid;
			}
		}
		return isValid;
	}

	/**
	 * Checks if is valid phone.
	 *
	 * @param value the value
	 * @return a boolean.
	 */
	public static boolean isValidPhone(String value, String pattern) {
		return StringUtils.isBlank(value) || Pattern.matches(pattern, value);
	}

	/**
	 * Checks if is length equals.
	 *
	 * @param value  the value
	 * @param length the length
	 * @return a boolean.
	 */
	public static boolean isLengthEquals(String value, int length) {
		return StringUtils.isBlank(value) || value.trim().length() == length;
	}

	/**
	 * Checks if is mandatory.
	 *
	 * @param value the value
	 * @return a boolean.
	 */
	public static boolean isMandatory(Object value) {
		if (Objects.isNull(value)) {
			return false;
		} else if (value instanceof String) {
			return StringUtils.isNotBlank((String) value);
		} else if (value instanceof Collection) {
			return !CollectionUtils.isEmpty((Collection<?>) value);
		} else if (value instanceof Map) {
			return !CollectionUtils.isEmpty((Map<?, ?>) value);
		} else {
			return true;
		}
	}

	/**
	 * Checks if is valid max length.
	 *
	 * @param value  the value
	 * @param length the length
	 * @return a boolean.
	 */
	public static boolean isValidMaxLength(String value, int length) {
		return StringUtils.isBlank(value) || value.trim().length() <= length;
	}

	/**
	 * Checks if is valid min length.
	 *
	 * @param value  the value
	 * @param length the length
	 * @return a boolean.
	 */
	public static boolean isValidMinLength(String value, int length) {
		return value == null || value.trim().length() >= length;
	}

	/**
	 * Checks if is valid no speacial char.
	 *
	 * @param value the value
	 * @return a boolean.
	 */
	public static boolean isValidNoSpeacialChar(String value) {
		return StringUtils.isBlank(value) || Pattern.matches(CommonFormats.ALPHANUMERIC, value);
	}

	/**
	 * Checks if is valid some special.
	 *
	 * @param value   the value
	 * @param special the special
	 * @return a boolean.
	 */
	public static boolean isValidSomeSpecial(String value, char[] special) {
		if (StringUtils.isBlank(value)) {
			return true;
		}

		boolean isValid = true;

		value = value.trim();
		final char[] chars = value.toCharArray();
		final Set<Character> valSpecial = new HashSet<>();
		for (final char c : chars) {
			if (!new String(new char[] { c }).matches("[0-9A-Za-z]")) {
				valSpecial.add(c);
			}
		}
		for (final Character c : valSpecial) {
			if (!isValidCharacter(c, special)) {
				isValid = false;
			}
		}
		return isValid;
	}

	/**
	 * Checks if is valid character.
	 *
	 * @param expectedChar the expected char
	 * @param givenChars   the given chars
	 * @return a boolean.
	 */
	public static boolean isValidCharacter(Character expectedChar, char[] givenChars) {
		boolean found = false;
		for (final char c : givenChars) {
			if (c == expectedChar) {
				found = true;
			}
		}
		return found;
	}

	/**
	 * Checks if is valid time format.
	 *
	 * @param value   the value
	 * @param pattern the pattern
	 * @return a boolean.
	 */
	public static boolean isValidTimeFormat(String value, String pattern) {
		boolean match = true;
		try {
			if (!StringUtils.isEmpty(value)) {
				match = Pattern.matches(pattern, value);
			}
		} catch (final Exception e) {
			match = false;
		}

		return match;
	}

	/**
	 * <p>
	 * isFutureDay.
	 * </p>
	 *
	 * @param date a {@link java.lang.String} object.
	 * @return a boolean.
	 */
	public static boolean isFutureDay(String date) {
		if (com.cars24.core.framework.utility.StringUtils.isNotBlank(date)) {
			try {
				final Date suppliedDate = DateUtils.getDate(CommonFormats.DATE_FORMAT, date);
				final Calendar tomorrow = Calendar.getInstance();
				tomorrow.set(Calendar.HOUR, 0);
				tomorrow.set(Calendar.MINUTE, 0);
				tomorrow.set(Calendar.SECOND, 0);
				tomorrow.set(Calendar.MILLISECOND, 0);
				tomorrow.add(Calendar.DATE, 1);

				return suppliedDate.getTime() >= DateUtils.getDate(CommonFormats.DATE_FORMAT,
						DateUtils.toString(CommonFormats.DATE_FORMAT, tomorrow.getTime())).getTime();
			} catch (final Exception e) {
				// _LOGGER.error("Error in evaluating future date : " + date);
			}
		}
		return false;
	}

	/**
	 * <p>
	 * isPastDay.
	 * </p>
	 *
	 * @param date a {@link java.lang.String} object.
	 * @return a boolean.
	 */
	public static boolean isPastDay(String date) {
		if (com.cars24.core.framework.utility.StringUtils.isNotBlank(date)) {
			try {
				final Date suppliedDate = DateUtils.getDate(CommonFormats.DATE_FORMAT, date);
				final Calendar yesterday = Calendar.getInstance();
				yesterday.set(Calendar.HOUR, 23);
				yesterday.set(Calendar.MINUTE, 59);
				yesterday.set(Calendar.SECOND, 59);
				yesterday.set(Calendar.MILLISECOND, 999);
				yesterday.add(Calendar.DATE, -1);

				return suppliedDate.getTime() <= yesterday.getTimeInMillis();
			} catch (final Exception e) {
				// _LOGGER.error("Error in evaluating future date : " + date);
			}
		}
		return false;
	}

	/**
	 * <p>
	 * isFutureTime.
	 * </p>
	 *
	 * @param date a {@link java.lang.String} object.
	 * @return a boolean.
	 */
	public static boolean isFutureTime(String date) {
		if (com.cars24.core.framework.utility.StringUtils.isNotBlank(date)) {
			try {
				final Date suppliedDate = DateUtils.getDate(CommonFormats.DATE_FORMAT_WITH_TIME, date);
				return suppliedDate.getTime() > new Date().getTime();
			} catch (final Exception e) {
				// _LOGGER.error("Error in evaluating future date time : " + date);
			}
		}
		return false;
	}
}
