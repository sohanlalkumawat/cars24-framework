package com.cars24.core.framework.validator.provider;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.cars24.core.framework.validator.annotation.IsValidEnum;
/**
 * The Class EnumValueValidator.
 * @author Sohan
 */
public class EnumValueValidator implements ConstraintValidator<IsValidEnum, String> {

	List<String> valueList = null;

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		if (!valueList.contains(value.toUpperCase())) {
			return false;
		}
		return true;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void initialize(IsValidEnum constraintAnnotation) {
		valueList = new ArrayList<String>();
		Class<? extends Enum<?>> enumClass = constraintAnnotation.enumClazz();

		Enum[] enumValArr = enumClass.getEnumConstants();

		for (Enum enumVal : enumValArr) {
			valueList.add(enumVal.toString().toUpperCase());
		}

	}

}
