/*
 * @author Sohan
 * Copyright cars24.com
 */
package com.cars24.core.framework.validator.provider;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.cars24.core.framework.validator.annotation.FloatMinLength;

/**
 * The Class FloatMinLengthValidator.
 *
 * @author Sohan
 * @version $Id: $Id
 */
public class FloatMinLengthValidator implements ConstraintValidator<FloatMinLength, Float> {

	/** The length. */
	private float length;

	/** {@inheritDoc} */
	@Override
	public void initialize(FloatMinLength minLength) {
		length = minLength.length();
		ConstraintValidator.super.initialize(minLength);
	}

	/** {@inheritDoc} */
	@Override
	public boolean isValid(Float value, ConstraintValidatorContext context) {
		return value == null || value >= length;
	}

}
