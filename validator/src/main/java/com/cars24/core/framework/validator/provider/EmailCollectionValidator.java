/*
 * @author Sohan
 * Copyright cars24.com
 */
package com.cars24.core.framework.validator.provider;

import java.util.Collection;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.cars24.core.framework.validator.annotation.EmailCollection;
import com.cars24.core.framework.validator.provider.utils.ValidationUtils;

/**
 * The Class EmailCollectionValidator.
 *
 * @author Sohan
 * @version $Id: $Id
 */
public class EmailCollectionValidator implements ConstraintValidator<EmailCollection, Collection<String>> {

	/** The pattern. */
	private String pattern;

	/** {@inheritDoc} */
	@Override
	public void initialize(EmailCollection emailFormat) {
		pattern = emailFormat.pattern();
		ConstraintValidator.super.initialize(emailFormat);
	}

	/** {@inheritDoc} */
	@Override
	public boolean isValid(Collection<String> values, ConstraintValidatorContext context) {
		return ValidationUtils.isValidEmails(values, pattern);
	}

}
