package com.cars24.core.framework.validator.provider;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.cars24.core.framework.validator.annotation.GeneralPancard;
import com.cars24.core.framework.validator.provider.utils.ValidationUtils;

public class GeneralPancardValidator implements ConstraintValidator<GeneralPancard, String> {
	
	private String pattern;

	@Override
	public void initialize(GeneralPancard generalPancard) {
		pattern = generalPancard.pattern();
		ConstraintValidator.super.initialize(generalPancard);
	}

	@Override
	public boolean isValid(String values, ConstraintValidatorContext context) {
		return ValidationUtils.isGeneralPancard(values, pattern);
	}

}