/*
 * @author Sohan
 * Copyright cars24.com
 */
package com.cars24.core.framework.validator.provider;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.cars24.core.framework.validator.annotation.TimeFormat;
import com.cars24.core.framework.validator.provider.utils.ValidationUtils;

/**
 * The Class TimeFormatValidator.
 *
 * @author Sohan
 * @version $Id: $Id
 */
public class TimeFormatValidator implements ConstraintValidator<TimeFormat, String> {

	/** The pattern. */
	private String pattern;

	/** {@inheritDoc} */
	@Override
	public void initialize(TimeFormat timeFormat) {
		pattern = timeFormat.pattern();
		ConstraintValidator.super.initialize(timeFormat);
	}

	/** {@inheritDoc} */
	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		return ValidationUtils.isValidTimeFormat(value, pattern);
	}

}
