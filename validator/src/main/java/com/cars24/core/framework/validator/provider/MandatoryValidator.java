/*
 * @author Sohan
 * Copyright cars24.com
 */
package com.cars24.core.framework.validator.provider;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.cars24.core.framework.validator.annotation.Mandatory;
import com.cars24.core.framework.validator.provider.utils.ValidationUtils;

/**
 * The Class MandatoryValidator.
 *
 * @author Sohan
 * @version $Id: $Id
 */
public class MandatoryValidator implements ConstraintValidator<Mandatory, Object> {

	/** {@inheritDoc} */
	@Override
	public boolean isValid(Object value, ConstraintValidatorContext context) {
		return ValidationUtils.isMandatory(value);
	}

}
