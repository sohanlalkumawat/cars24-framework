/*
 * @author Sohan
 * Copyright cars24.com
 */
package com.cars24.core.framework.validator.provider;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.core.io.ByteArrayResource;

import com.cars24.core.framework.validator.annotation.FileSize;
import com.cars24.core.framework.validator.provider.utils.ValidationUtils;

/**
 * The Class FileExtensionValidator.
 *
 * @author Sohan
 * @version $Id: $Id
 */
public class FileSizeValidator implements ConstraintValidator<FileSize, String> {

	/** The min size. */
	private int minSize;

	/** The max size. */
	private int maxSize;

	/** {@inheritDoc} */
	@Override
	public void initialize(FileSize fileSize) {
		minSize = fileSize.minSize();
		maxSize = fileSize.maxSize();
		ConstraintValidator.super.initialize(fileSize);
	}

	/** {@inheritDoc} */
	@Override
	public boolean isValid(String base64FileString, ConstraintValidatorContext context) {
		ByteArrayResource bar =null;// new ByteArrayResource(Base64Utils.docode(base64FileString));

		return ValidationUtils.isValidFileSize(bar, minSize, maxSize);
	}
}
