/*
 * @author Sohan
 * Copyright cars24.com
 */
package com.cars24.core.framework.validator.provider;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.cars24.core.framework.validator.annotation.SomeSpecial;
import com.cars24.core.framework.validator.provider.utils.ValidationUtils;

/**
 * The Class SomeSpecialValidator.
 *
 * @author Sohan
 * @version $Id: $Id
 */
public class SomeSpecialValidator implements ConstraintValidator<SomeSpecial, String> {

	/** The special. */
	private char[] special;

	/** {@inheritDoc} */
	@Override
	public void initialize(SomeSpecial constraintAnnotation) {
		special = constraintAnnotation.special();
		ConstraintValidator.super.initialize(constraintAnnotation);
	}

	/** {@inheritDoc} */
	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		return ValidationUtils.isValidSomeSpecial(value, special);
	}

}
