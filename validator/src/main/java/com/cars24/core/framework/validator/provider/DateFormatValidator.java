/*
 * @author Sohan
 * Copyright cars24.com
 */
package com.cars24.core.framework.validator.provider;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.cars24.core.framework.validator.annotation.DateFormat;
import com.cars24.core.framework.validator.provider.utils.ValidationUtils;

/**
 * The Class DateFormatValidator.
 *
 * @author Sohan
 * @version $Id: $Id
 */
public class DateFormatValidator implements ConstraintValidator<DateFormat, String> {

	/** The pattern. */
	private String pattern;

	/** {@inheritDoc} */
	@Override
	public void initialize(DateFormat dateFormat) {
		pattern = dateFormat.pattern();
		ConstraintValidator.super.initialize(dateFormat);
	}

	/** {@inheritDoc} */
	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		return ValidationUtils.isValidDateFormat(value, pattern);
	}
}
