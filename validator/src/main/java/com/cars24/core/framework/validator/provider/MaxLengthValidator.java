/*
 * @author Sohan
 * Copyright cars24.com
 */
package com.cars24.core.framework.validator.provider;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.cars24.core.framework.validator.annotation.MaxLength;
import com.cars24.core.framework.validator.provider.utils.ValidationUtils;

/**
 * The Class MaxLengthValidator.
 *
 * @author Sohan
 * @version $Id: $Id
 */
public class MaxLengthValidator implements ConstraintValidator<MaxLength, String> {

	/** The length. */
	private int length;

	/** {@inheritDoc} */
	@Override
	public void initialize(MaxLength maxLength) {
		length = maxLength.length();
		ConstraintValidator.super.initialize(maxLength);
	}

	/** {@inheritDoc} */
	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		return ValidationUtils.isValidMaxLength(value, length);
	}

}
