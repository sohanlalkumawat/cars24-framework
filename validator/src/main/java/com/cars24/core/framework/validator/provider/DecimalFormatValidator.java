/*
 * @author Sohan
 * Copyright cars24.com
 */
package com.cars24.core.framework.validator.provider;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.cars24.core.framework.validator.annotation.DecimalFormat;
import com.cars24.core.framework.validator.provider.utils.ValidationUtils;

/**
 * The Class DecimalFormatValidator.
 *
 * @author Sohan
 * @version $Id: $Id
 */
public class DecimalFormatValidator implements ConstraintValidator<DecimalFormat, Number> {

	/** The pattern. */
	private String pattern;

	/** {@inheritDoc} */
	@Override
	public void initialize(DecimalFormat decimalFormat) {
		pattern = decimalFormat.pattern();
		ConstraintValidator.super.initialize(decimalFormat);
	}

	/** {@inheritDoc} */
	@Override
	public boolean isValid(Number value, ConstraintValidatorContext context) {
		return ValidationUtils.isValidDecimalFormat(value, pattern);
	}

}
