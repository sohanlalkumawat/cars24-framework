/*
 * @author Sohan
 * Copyright cars24.com
 */
package com.cars24.core.framework.validator.provider;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.cars24.core.framework.validator.annotation.MinLength;
import com.cars24.core.framework.validator.provider.utils.ValidationUtils;

/**
 * The Class MinLengthValidator.
 *
 * @author Sohan
 * @version $Id: $Id
 */
public class MinLengthValidator implements ConstraintValidator<MinLength, String> {

	/** The length. */
	private int length;

	/** {@inheritDoc} */
	@Override
	public void initialize(MinLength minLength) {
		length = minLength.length();
		ConstraintValidator.super.initialize(minLength);
	}

	/** {@inheritDoc} */
	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		return ValidationUtils.isValidMinLength(value, length);
	}

}
