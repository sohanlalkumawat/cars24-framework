
package com.cars24.core.framework.validator.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.cars24.core.framework.validator.provider.FileSizeValidator;

/**
 * The Interface FileExtension.
 *
 * @author Sohan
 */
@Documented
@Constraint(validatedBy = FileSizeValidator.class)
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface FileSize {

	/**
	 * Message.
	 *
	 * @return the string
	 */
	String message() default "File Size Error";

	/**
	 * Groups.
	 *
	 * @return the class[]
	 */
	Class<?>[] groups() default {};

	/**
	 * Payload.
	 *
	 * @return the Payload[]
	 */
	Class<? extends Payload>[] payload() default {};

	/**
	 * Min size for file.
	 *
	 * @return the int
	 */
	int minSize();

	/**
	 * Max size for file.
	 *
	 * @return the int
	 */
	int maxSize();
}
