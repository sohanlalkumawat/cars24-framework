/*
 * @author Sohan
 * Copyright cars24.com
 */
package com.cars24.core.framework.validator.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

import com.cars24.core.framework.validator.provider.MaxLengthValidator;

/**
 * The Interface MaxLength.
 *
 * @author Sohan
 * @version $Id: $Id
 */
@Documented
@Constraint(validatedBy = MaxLengthValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface MaxLength {

  /**
   * Message.
   *
   * @return the string
   */
  String message() default "Maximum Length Error";

  /**
   * Groups.
   *
   * @return the class[]
   */
  Class<?>[] groups() default {};

  /**
   * Payload.
   *
   * @return the Payload[]
   */
  Class<? extends Payload>[] payload() default {};

  /**
   * Length.
   *
   * @return the int
   */
  int length();

}
