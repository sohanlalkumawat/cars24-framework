package com.cars24.core.framework.validator.provider;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.cars24.core.framework.validator.annotation.IsPersonPancard;
import com.cars24.core.framework.validator.provider.utils.ValidationUtils;

public class PersonPancardValidator implements ConstraintValidator<IsPersonPancard, String> {

	private String pattern;

	@Override
	public void initialize(IsPersonPancard personPancard) {
		pattern = personPancard.pattern();
		ConstraintValidator.super.initialize(personPancard);
	}

	@Override
	public boolean isValid(String values, ConstraintValidatorContext context) {
		return ValidationUtils.isPersonPancard(values, pattern);
	}

}
