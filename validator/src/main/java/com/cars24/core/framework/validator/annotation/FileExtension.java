/*
 * @author Sohan
 * Copyright cars24.com
 */
package com.cars24.core.framework.validator.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.cars24.core.framework.validator.provider.FileExtensionValidator;

/**
 * The Interface FileExtension.
 *
 * @author Sohan
 * @version $Id: $Id
 */
@Documented
@Constraint(validatedBy = FileExtensionValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface FileExtension {

    /**
     * Message.
     *
     * @return the string
     */
    String message() default "File Extension Error";

    /**
     * Groups.
     *
     * @return the class[]
     */
    Class<?>[] groups() default {};

    /**
     * Payload.
     *
     * @return the Payload[]
     */
    Class<? extends Payload>[] payload() default {};

    /**
     * Formats.
     *
     * @return the string[]
     */
    String[] formats();

}
