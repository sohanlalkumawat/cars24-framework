/*
 * @author Sohan
 * Copyright cars24.com
 */
package com.cars24.core.framework.validator.provider;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.cars24.core.framework.validator.annotation.LengthEquals;
import com.cars24.core.framework.validator.provider.utils.ValidationUtils;

/**
 * The Class LengthEqualsValidator.
 *
 * @author Sohan
 * @version $Id: $Id
 */
public class LengthEqualsValidator implements ConstraintValidator<LengthEquals, String> {

	/** The length. */
	private int length;

	/** {@inheritDoc} */
	@Override
	public void initialize(LengthEquals lengthEquals) {
		length = lengthEquals.length();
		ConstraintValidator.super.initialize(lengthEquals);
	}

	/** {@inheritDoc} */
	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		return ValidationUtils.isLengthEquals(value, length);
	}

}
