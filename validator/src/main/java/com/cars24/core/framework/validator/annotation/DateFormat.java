
package com.cars24.core.framework.validator.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

import com.cars24.core.framework.validator.provider.DateFormatValidator;

/**
 * The Interface DateFormat.
 *
 * @author Sohan
 */
@Documented
@Constraint(validatedBy = DateFormatValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface DateFormat {

    /**
     * Message.
     *
     * @return the string
     */
    String message() default "Date Format Error";

    /**
     * Groups.
     *
     * @return the class[]
     */
    Class<?>[] groups() default {};

    /**
     * Payload.
     *
     * @return the Payload[]
     */
    Class<? extends Payload>[] payload() default {};

    /**
     * Pattern.
     *
     * @return the string
     */
    String pattern();

}
