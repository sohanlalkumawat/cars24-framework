
package com.cars24.core.framework.validator.provider;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.cars24.core.framework.validator.annotation.AlphaNumeric;
import com.cars24.core.framework.validator.provider.utils.ValidationUtils;

/**
 * The Class AlphaNumericValidator.
 *
 * @author Sohan
 */
public class AlphaNumericValidator implements ConstraintValidator<AlphaNumeric, String> {

	/** The with chars. */
	private char[] withChars;

	/** {@inheritDoc} */
	@Override
	public void initialize(AlphaNumeric a) {
		withChars = a.withchars();
		ConstraintValidator.super.initialize(a);
	}

	/** {@inheritDoc} */
	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		return ValidationUtils.isAlphanumeric(value, withChars);
	}

}
