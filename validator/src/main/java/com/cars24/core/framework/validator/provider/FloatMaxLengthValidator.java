/*
 * @author Sohan
 * Copyright cars24.com
 */
package com.cars24.core.framework.validator.provider;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.cars24.core.framework.validator.annotation.FloatMaxLength;

/**
 * The Class FloatMaxLengthValidator.
 *
 * @author Sohan
 * @version $Id: $Id
 */
public class FloatMaxLengthValidator implements ConstraintValidator<FloatMaxLength, Float> {

	/** The length. */
	private float length;

	/** {@inheritDoc} */
	@Override
	public void initialize(FloatMaxLength maxLength) {
		length = maxLength.length();
		ConstraintValidator.super.initialize(maxLength);
	}

	/** {@inheritDoc} */
	@Override
	public boolean isValid(Float value, ConstraintValidatorContext context) {
		return value == null || value <= length;
	}

}
