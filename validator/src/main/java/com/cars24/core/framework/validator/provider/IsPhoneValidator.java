
package com.cars24.core.framework.validator.provider;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.cars24.core.framework.validator.annotation.IsPhone;
import com.cars24.core.framework.validator.provider.utils.ValidationUtils;

/**
 * The Class IsPhoneValidator.
 *
 * @author Sohan
 */
public class IsPhoneValidator implements ConstraintValidator<IsPhone, String> {

	private String pattern;

	@Override
	public void initialize(IsPhone phoneNumber) {
		pattern = phoneNumber.pattern();
		ConstraintValidator.super.initialize(phoneNumber);
	}

	/** {@inheritDoc} */
	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		return ValidationUtils.isValidPhone(value, pattern);
	}
}
