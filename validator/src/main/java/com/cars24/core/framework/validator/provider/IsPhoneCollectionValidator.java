
package com.cars24.core.framework.validator.provider;

import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.cars24.core.framework.validator.annotation.IsPhoneCollection;
import com.cars24.core.framework.validator.provider.utils.ValidationUtils;

/**
 * The Class IsPhoneCollectionValidator.
 *
 * @author Sohan
 */
public class IsPhoneCollectionValidator implements ConstraintValidator<IsPhoneCollection, List<String>> {

	private String pattern;

	@Override
	public void initialize(IsPhoneCollection phoneCollection) {
		pattern = phoneCollection.pattern();
		ConstraintValidator.super.initialize(phoneCollection);
	}

	@Override
	public boolean isValid(List<String> values, ConstraintValidatorContext context) {
		return ValidationUtils.isValidPhones(values, pattern);
	}

}
