/*
 * @author Sohan
 * Copyright cars24.com
 */
package com.cars24.core.framework.validator.provider;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.cars24.core.framework.validator.annotation.IsFlag;
import com.cars24.core.framework.validator.provider.utils.ValidationUtils;

/**
 * The Class IsFlagValidator.
 *
 * @author Sohan
 * @version $Id: $Id
 */
public class IsFlagValidator implements ConstraintValidator<IsFlag, Integer> {

	/** {@inheritDoc} */
	@Override
	public boolean isValid(Integer value, ConstraintValidatorContext context) {
		return ValidationUtils.isValidFlag(value);
	}
}
