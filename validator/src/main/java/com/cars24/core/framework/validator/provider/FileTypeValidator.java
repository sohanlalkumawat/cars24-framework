
package com.cars24.core.framework.validator.provider;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.cars24.core.framework.validator.annotation.FileType;
import com.cars24.core.framework.validator.provider.utils.ValidationUtils;

/**
 * The Class FileTypeValidator.
 *
 * @author Sohan
 */
public class FileTypeValidator implements ConstraintValidator<FileType, String> {

	/** The formats. */
	private String[] formats;

	/** {@inheritDoc} */
	@Override
	public void initialize(FileType fileType) {
		formats = fileType.formats();
		ConstraintValidator.super.initialize(fileType);
	}

	/** {@inheritDoc} */
	@Override
	public boolean isValid(String fileName, ConstraintValidatorContext context) {
		return ValidationUtils.isValidFileType(fileName, formats);
	}
}
