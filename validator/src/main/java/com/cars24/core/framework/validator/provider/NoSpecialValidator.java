/*
 * @author Sohan
 * Copyright cars24.com
 */
package com.cars24.core.framework.validator.provider;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.cars24.core.framework.validator.annotation.NoSpecial;
import com.cars24.core.framework.validator.provider.utils.ValidationUtils;

/**
 * The Class NoSpecialValidator.
 *
 * @author Sohan
 * @version $Id: $Id
 */
public class NoSpecialValidator implements ConstraintValidator<NoSpecial, String> {

	/** {@inheritDoc} */
	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		return ValidationUtils.isValidNoSpeacialChar(value);
	}
}
