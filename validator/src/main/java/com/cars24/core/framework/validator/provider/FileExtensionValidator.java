/*
 * @author Sohan
 * Copyright cars24.com
 */
package com.cars24.core.framework.validator.provider;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.cars24.core.framework.validator.annotation.FileExtension;
import com.cars24.core.framework.validator.provider.utils.ValidationUtils;

/**
 * The Class FileExtensionValidator.
 *
 * @author Sohan
 * @version $Id: $Id
 */
public class FileExtensionValidator implements ConstraintValidator<FileExtension, String> {

	/** The formats. */
	private String[] formats;

	/** {@inheritDoc} */
	@Override
	public void initialize(FileExtension fileExtension) {
		formats = fileExtension.formats();
		ConstraintValidator.super.initialize(fileExtension);
	}

	/** {@inheritDoc} */
	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		return ValidationUtils.isValidFileExtension(value, formats);
	}
}
