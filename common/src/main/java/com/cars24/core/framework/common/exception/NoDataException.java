/*
 * @author Sohan
 * Copyright cars24.com
 */
package com.cars24.core.framework.common.exception;

import com.cars24.core.framework.common.bean.ErrorBean;

/**
 * The Class NoDataException.
 *
 * @author Sohan
 */
public class NoDataException extends RuntimeException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5318591489806278624L;

	/** The error. */
	private ErrorBean error; 

	/**
	 * Instantiates a new no data exception.
	 *
	 * @param error the error
	 */
	public NoDataException(ErrorBean error) {
		super(error.getMessage());
		this.error = error;
		// _LOGGER.error(error.getMessage(), this);
	}

	/**
	 * Instantiates a new no data exception.
	 *
	 * @param message the message
	 * @param error   the error
	 */
	public NoDataException(String message, ErrorBean error) {
		super(message);
		this.error = error;
		// _LOGGER.error(message, this);
	}

	/**
	 * Instantiates a new no data exception.
	 *
	 * @param message the message
	 * @param error   the error
	 * @param cause   the cause
	 */
	public NoDataException(String message, ErrorBean error, Throwable cause) {
		super(message, cause);
		this.error = error;
		// _LOGGER.error(message, cause);
	}

	/**
	 * Gets the error.
	 *
	 * @return the error
	 */
	public ErrorBean getError() {
		return error;
	}

}
