
package com.cars24.core.framework.common.bean;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.cars24.core.framework.common.exception.JSONMappingException;
import com.cars24.core.framework.utility.CollectionUtils;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Data;

/**
 * The Class ErrorResponse.
 *
 * @author Sohan
 */
@Data
public class ErrorResponse implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4416805417925350195L;

	private String message;

	private List<ErrorBean> errors;

	public ErrorResponse() {
	}

	public static ErrorResponse create() {
		return new ErrorResponse();
	}

	public ErrorResponse message(String message) {
		this.message = message;
		return this;
	}

	public ErrorResponse errors(List<ErrorBean> errors) {
		this.errors = errors;
		return this;
	}

	public ErrorResponse addError(ErrorBean error) {
		if (this.errors == null) {
			this.errors = new ArrayList<>();
		}
		this.errors.add(error);
		return this;
	}

	@JsonCreator
	public ErrorResponse(@JsonProperty("message") String message,
			@JsonProperty("errors") List<Map<String, Object>> errors) {
		this.message = message;
		if (CollectionUtils.isNotEmpty(errors)) {
			this.errors = new ArrayList<>();
			ObjectMapper mapper = new ObjectMapper();
			for (Map<String, Object> error : errors) {
				try {
					ErrorBean bean = mapper.readValue(mapper.writeValueAsString(error), ErrorBean.class);
					this.errors.add(bean);
				} catch (IOException e) {
					throw new JSONMappingException("Error Creating Bean Error Response : " + e.getMessage());
				}
			}
		}
	}

}
