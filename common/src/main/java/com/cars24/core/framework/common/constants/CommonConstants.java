
package com.cars24.core.framework.common.constants;

/**
 * The Class CommonConstants.
 * 
 * @author Sohan
 */
public class CommonConstants {

	/**
	 * The Enum Protocol.
	 */
	public enum Protocol {

	HTTP, HTTPS
	}

	/**
	 * Instantiates a new common constants.
	 */
	private CommonConstants() {
	}

	/** The Constant NO_ARGS. */
	public static final Object[] NO_ARGS = new Object[] {};

	/** The Constant NO_CLASS. */
	public static final Class<?>[] NO_CLASS = new Class[] {};

	/** The Constant BASE_PACKAGE. */
	public static final String BASE_PACKAGE = "com.cars24";

	/** The Constant EXTERNAL_CONFIG_PROPERTY. */
	public static final String EXTERNAL_CONFIG_PROPERTY = "cars24.external.config";

	/** The Constant REDIS_TEMPLATE_QUALIFIER. */
	public static final String CACHE_TEMPLATE_QUALIFIER = "cacheByteArrayTemplate";

	/** The Constant TABLE_ANNOTATION. */
	public static final String TABLE_ANNOTATION = "javax.persistence.Table";

	/** The Constant SPRNG_APPLICATION_NAME_KEY. */
	public static final String SPRNG_APPLICATION_NAME_KEY = "spring.application.name";

	/** The Constant SPRING_PROFILE_ACTIVE_KEY. */
	public static final String SPRING_PROFILE_ACTIVE_KEY = "spring.profiles.active";

	/** The Constant DEFAULT_PAGE_NO. */
	public static final String DEFAULT_PAGE_NO = "DEFAULT_PAGE_NO";

	/** The Constant DEFAULT_PAGE_SIZE. */
	public static final String DEFAULT_PAGE_SIZE = "DEFAULT_PAGE_SIZE";

	/** The Constant MAX_PAGE_SIZE. */
	public static final String MAX_PAGE_SIZE = "MAX_PAGE_SIZE";

	public static final String X_REQUEST_ID = "x-request-id";

	public static final String AUTH_HEADER = "auth-key";

	public static final String VALIDATION_ERROR_MSG = "Validation Error(s)";

	public static final String ILLEGAL_PARAM = "Illegal params";

	/** The Constant GET_MAX_RECORDS_LIMIT. */
	public static final String GET_MAX_RECORDS_LIMIT = "MAX_RECORDS_LIMIT";

	/** The Constant AUTOCOMPLETE_MAX_RECORDS_LIMIT. */
	public static final String AUTOCOMPLETE_MAX_RECORDS_LIMIT = "AC_MAX_RECORDS_LIMIT";

	/** The Constant UTILITY_TOKEN. */

	/** The Constant AUTH_TOKEN_VALIDITY. */
	public static final String AUTH_TOKEN_VALIDITY = "AUTH_TOKEN_VALIDITY";

	/** The Constant DEFAULT_COUNTRY_CODE. */
	public static final String DEFAULT_COUNTRY_CODE = "+91";

	/** The Constant APPLICATION_TIME_ZONE. */
	public static final String APPLICATION_TIME_ZONE = "cars24.application.timezone";

	/** The Constant TZ_ASIA_CALCUTTA. */
	public static final String TZ_ASIA_CALCUTTA = "Asia/Calcutta";

	/** Constant <code>MAX_RECORD_LIMIT_EXPORT="MAX_RECORD_LIMIT_EXPORT"</code> */
	public static final String MAX_RECORD_LIMIT_EXPORT = "MAX_RECORD_LIMIT_EXPORT";

	public static final String LOGGING_ENABLE_CONDITION = "application.logging.enabled";

	public static final String SWAGGER_ENABLE_CONDITION = "swagger.api.enabled";

	public static final String LAMBDA_ID = "lambda_id";

	public static final String DEPLOYMENT_TYPE_KEY = "cars24.deployment.type";

	public static final String REDIS_REPOSITORY = "execution(* com.cars24.core.framework.caching.repository.CacheRepository.*(..))";

	public static final String APPLICATION_ALLOW_ORIGIN = "application.allow.origin";

	public enum DeploymentType {
		BOOT, LAMBDA, BATCH
	}

}
