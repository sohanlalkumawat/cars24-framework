package com.cars24.core.framework.common.constants;

import java.text.DecimalFormat;

/**
 * The Interface CommonFormats.
 *
 * @author Sohan
 * @version $Id: $Id
 */
public interface CommonFormats {

	/** The email format. */
	String EMAIL_FORMAT = "^[0-9A-Za-z_.]+[@][0-9A-Za-z_]+[.][0-9A-Za-z_.]+$";

	/** The time format. */
	String TIME_FORMAT = "^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$";

	/** The time format. */
	String TIME_FORMAT_WITHOUT_COLON = "^([0-9]|0[0-9]|1[0-9]|2[0-3])[0-5][0-9]$";

	/** The alphanumeric. */
	String ALPHANUMERIC = "^[A-Z0-9a-z]+$";

	/** The date format with time. */
	String DATE_FORMAT_WITH_TIME = "yyyy-MM-dd HH:mm:ss";

	/** The date format. */
	String DATE_FORMAT = "yyyy-MM-dd";

	/** The date format. */
	String DATE_FORMAT_DAY_NUMBER_OF_WEEK = "u";

	/** Constant <code>TIME_FORMAT_WITH_12="hh:mm a"</code> */
	String TIME_FORMAT_WITH_12 = "hh:mm a";

	/** The date format. */
	String DATE_FORMAT_DAY_MONTH = "d MMMM";

	/** The default mysql date time format. */
	String TIME_WITHOUT_COLON = "HHmm";

	String PHONE_NUMBER_FORMAT_10_DIGIT="^\\d{10}$";
	
	String GENERAL_PHONE_NUMBER_FORMAT="^(\\+0?1\\s)?\\(?\\d{3}\\)?[\\s.-]\\d{3}[\\s.-]\\d{4}$";
	
	/** The default mysql date time format. */
	String DEFAULT_MYSQL_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";

	String PERSON_PANCARD_FORMAT = "^[a-zA-Z]{3}[pP][a-zA-Z][0-9]{4}[a-zA-Z]{1}$";

	String GENERAL_PANCARD_FORMAT = "^[a-zA-Z]{3}[abcfghljptABCFGHLJPT][a-zA-Z][0-9]{4}[a-zA-Z]{1}$";

	/** Constant <code>AMOUNT_FORMAT</code> */
	DecimalFormat AMOUNT_FORMAT = new DecimalFormat("#0.00");
}
