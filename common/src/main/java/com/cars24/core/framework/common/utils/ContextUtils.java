
package com.cars24.core.framework.common.utils;

import java.util.Arrays;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import com.cars24.core.framework.common.constants.ApplicationProfiles;
import com.cars24.core.framework.common.constants.CommonConstants;
import com.cars24.core.framework.utility.StringUtils;

/**
 * The Class ContextUtils.
 *
 * @author Sohan
 */
@Configuration
@SuppressWarnings("unused")
public class ContextUtils implements ApplicationContextAware {

	/** The context. */
	private static ApplicationContext context;

	/** The application name. */
	private static String applicationName;

	/** The local cache identifier */
	private static String localCacheIdentifier;

	/** The active profile. */
	private static String activeProfile;

	public static <T> T getBean(Class<T> requiredType) {
		return getContext().getBean(requiredType);
	}

	public static Object getBean(String name) {
		return getContext().getBean(name);
	}

	public static Object getBean(String name, Object... args) {
		return getContext().getBean(name, args);
	}

	public static <T> T getBean(Class<T> requiredType, Object... args) {
		return getContext().getBean(requiredType, args);
	}

	public static ApplicationContext getContext() {
		return context;
	}

	public static String getBootName() {
		if (applicationName == null) {
			applicationName = getEnvironment().getProperty(CommonConstants.SPRNG_APPLICATION_NAME_KEY);
		}
		return applicationName;
	}

	public static Environment getEnvironment() {
		return context.getBean(Environment.class);
	}

	public static String getActiveProfile() {
		return getEnvironment().getActiveProfiles()[0];
	}

	public static boolean isProfileActive(String profile) {

		return Arrays.stream(getEnvironment().getActiveProfiles())
				.anyMatch(envProfile -> envProfile.equalsIgnoreCase(profile));

	}

	public static boolean isProfileTest() {
		return isProfileActive(ApplicationProfiles.TEST);
	}

	public static boolean isProfileDev() {
		return isProfileActive(ApplicationProfiles.DEV);
	}

	public static boolean isProfileQA() {
		return isProfileActive(ApplicationProfiles.QA);
	}

	public static boolean isProfileLocal() {
		return isProfileActive(ApplicationProfiles.LOCAL);
	}

	public static boolean isProfileUat() {
		return isProfileActive(ApplicationProfiles.UAT);
	}

	public static boolean isProfileProd() {
		return isProfileActive(ApplicationProfiles.PRODUCTION);
	}

	/** {@inheritDoc} */
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		ContextUtils.context = applicationContext;
	}

	public static String getLocalCacheId() {
		if (StringUtils.isEmpty(localCacheIdentifier)) {
			localCacheIdentifier = System.getProperty(CommonConstants.LAMBDA_ID);
		}
		if (StringUtils.isEmpty(localCacheIdentifier) && getEnvironment() != null) {
			localCacheIdentifier = getEnvironment().getProperty(CommonConstants.SPRNG_APPLICATION_NAME_KEY);
		}

		return localCacheIdentifier;
	}
}
