
package com.cars24.core.framework.common.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;

/**
 * The Class ErrorBean.
 * 
 * @author Sohan
 */
@Data
public class ErrorBean implements Serializable {

	private static final long serialVersionUID = -3174610163626734324L;

	private String code;

	private String message;

	private String property;

	/**
	 * Instantiates a new error bean.
	 */
	public ErrorBean() {
	}

	public static ErrorBean withError(String code) {
		return new ErrorBean(code, Errors.error(code).getMessage());
	}

	public static ErrorBean withError(String code, String message, String property) {
		return new ErrorBean(code, message, property);
	}

	public static ErrorBean withError(String code, String message) {
		return new ErrorBean(code, message);
	}

	public ErrorBean(String code, String message) {
		super();
		this.code = code;
		this.message = message;
	}

	public ErrorBean(String code, String message, String field) {
		super();
		this.code = code;
		this.property = field;
		this.message = message;
	}

	public static ErrorBuilder builder() {
		return new ErrorBuilder();
	}

	public static class ErrorBuilder {

		List<ErrorBean> errors = new ArrayList<>();

		public ErrorBuilder add(ErrorBean error) {
			errors.add(error);
			return this;
		}

		public List<ErrorBean> build() {
			return this.errors;
		}
	}
}
