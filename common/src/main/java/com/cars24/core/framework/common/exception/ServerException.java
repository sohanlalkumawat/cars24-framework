/*
 * @author Sohan
 * Copyright cars24.com
 */
package com.cars24.core.framework.common.exception;

import com.cars24.core.framework.common.bean.ErrorBean;

/**
 * The Class ServerException.
 *
 * @author Sohan
 * @version $Id: $Id
 */
public class ServerException extends RuntimeException {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -5318591489806278624L;
    
    /** The error. */
    private ErrorBean error;

    /**
     *
     * @param error the error
     */
    public ServerException(ErrorBean error) {
        super(error.getMessage());
        this.error = error;
    }

    /**
     * Instantiates a new server exception.
     *
     * @param message the message
     * @param error the error
     */
    public ServerException(String message, ErrorBean error) {
        super(message);
        this.error = error;
    }

    /**
     * Instantiates a new server exception.
     *
     * @param message the message
     * @param error the error
     * @param throwable the throwable
     */
    public ServerException(String message, ErrorBean error, Throwable throwable) {
        super(message, throwable);
        this.error = error;
    }

    /**
     * Gets the error.
     *
     * @return the error
     */
    public ErrorBean getError() {
        return error;
    }

}
