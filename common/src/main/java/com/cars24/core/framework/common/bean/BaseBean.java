
package com.cars24.core.framework.common.bean;

import java.io.Serializable;

import javax.validation.constraints.Null;

import lombok.Data;

/**
 * The Class BaseBean.
 *
 * @author Sohan
 */
@Data
public class BaseBean implements Serializable {

	private static final long serialVersionUID = 7111344873483109021L;

	@Null
	private String createdAt;

	@Null
	private Long createdBy;

	@Null
	private String modifiedAt;

	@Null
	private Long modifiedBy;

}
