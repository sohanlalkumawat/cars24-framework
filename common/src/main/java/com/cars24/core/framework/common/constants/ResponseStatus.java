/*
 * @author Sohan
 * Copyright cars24.com
 */
package com.cars24.core.framework.common.constants;

/**
 * The Enum ResponseStatus.
 *
 * @author Sohan
 * @version $Id: $Id
 */
public enum ResponseStatus {

    /** The success. */
    SUCCESS(200, "SUCCESS", "Successful Request"),

    /** The validation error. */
    VALIDATION_ERROR(400, "VALIDATION_ERROR", "Validation Failed"),

    /** The server error. */
    SERVER_ERROR(500, "SERVER_ERROR", "Server Error"),

    /** The business error. */
    BUSINESS_ERROR(401, "BUSINESS_ERROR", "Business Validation Failure"),

    /** The config error. */
    CONFIG_ERROR(402, "CONFIG_ERROR", "Configuration Error"),

    /** The auth error. */
    AUTH_ERROR(403, "AUTH_ERROR", "Authorization Error"),

    /** The not found. */
    NOT_FOUND(404, "NOT_FOUND", "Resource/Endpoint not found"),

    /** The db uk error. */
    DB_UK_ERROR(410, "DB_UK_ERROR", "Unique Constraint Error"),

    /** The db pk error. */
    DB_PK_ERROR(411, "DB_PK_ERROR", "Primary Key Error"),

    /** The db connection error. */
    DB_CONNECTION_ERROR(412, "DB_CONNECTION_ERROR", "Database Connection Error"),

    /** The db mode error. */
    DB_MODE_ERROR(413, "DB_MODE_ERROR", "Database Mode Error");

    /** The value. */
    private final int value;

    /** The code. */
    private final String code;

    /** The status name. */
    private final String statusName;

    /**
     * Instantiates a new response status.
     *
     * @param value the value
     * @param code the code
     * @param statusName the status name
     */
    ResponseStatus(int value, String code, String statusName) {
        this.value = value;
        this.code = code;
        this.statusName = statusName;
    }

    /**
     * Value.
     *
     * @return the int
     */
    public int value() {
        return this.value;
    }

    /**
     * Code.
     *
     * @return the string
     */
    public String code() {
        return this.code;
    }

    /**
     * Description.
     *
     * @return the string
     */
    public String description() {
        return this.statusName;
    }
}
