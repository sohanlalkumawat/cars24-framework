/*
 * @author Sohan
 * Copyright cars24.com
 */
package com.cars24.core.framework.common.constants;

/**
 * The Class ApplicationProfiles.
 *
 * @author Sohan
 * @version $Id: $Id
 */
public class ApplicationProfiles {

	/**
	 * Instantiates a new application profiles.
	 */
	private ApplicationProfiles() {
	}

	/** The Constant LOCAL. */
	public static final String LOCAL = "local";

	/** The Constant DEVELOPMENT. */
	public static final String DEV = "dev";

	/** The Constant QA. */
	public static final String QA = "qa";

	/** The Constant UAT. */
	public static final String UAT = "uat";

	/** The Constant STAGE. */
	public static final String STAGE = "stage";

	/** The Constant PRODUCTION. */
	public static final String PRODUCTION = "prod";

	/** The Constant TEST. */
	public static final String TEST = "test";

}
