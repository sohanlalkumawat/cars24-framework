package com.cars24.core.framework.common.exception;

import com.cars24.core.framework.common.bean.ErrorBean;

/**
 * The Class BusinessException.
 *
 * @author Sohan
 */
public class BusinessException extends RuntimeException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5318591489806278624L;

	/** The error. */
	private ErrorBean error;

	/**
	 * Instantiates a new business exception.
	 *
	 * @param error the error
	 */
	public BusinessException(ErrorBean error) {
		super(error.getMessage());
		this.error = error;
	}

	/**
	 * Instantiates a new business exception.
	 *
	 * @param message the message
	 * @param error   the error
	 */
	public BusinessException(String message, ErrorBean error) {
		super(message);
		this.error = error;
	}

	/**
	 * Instantiates a new business exception.
	 *
	 * @param message the message
	 * @param error   the error
	 * @param cause   the cause
	 */
	public BusinessException(String message, ErrorBean error, Throwable cause) {
		super(message, cause);
		this.error = error;
	}

	/**
	 * Gets the error.
	 *
	 * @return the error
	 */
	public ErrorBean getError() {
		return error;
	}

}
