
package com.cars24.core.framework.logging.config;

import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.cars24.core.framework.logging.interceptor.LoggingInterceptor;

/**
 * The Class LoggingInterceptorConfig.
 *
 * @author Sohan
 */
@Configuration
@Conditional(LoggingCondition.class)
public class LoggingInterceptorConfig implements WebMvcConfigurer {

	/** {@inheritDoc} */
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new LoggingInterceptor());
	}

}
