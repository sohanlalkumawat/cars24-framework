package com.cars24.core.framework.logging.helper;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cars24.core.framework.logging.bean.ActionRequestInfo;
import com.cars24.core.framework.logging.bean.LoggerContext;

public class LoggingHelper {

	private static final Logger _LOGGER = LoggerFactory.getLogger(LoggingHelper.class);

	private static ThreadLocal<LoggerContext> loggerContext = new ThreadLocal<>();

	private LoggingHelper() {
	}

	public static LoggerContext getContext() {
		return loggerContext.get();
	}

	public static void setContext(LoggerContext ctx) {
		loggerContext.set(ctx);
	}

	public static void clearContext() {
		if (loggerContext != null) {
			loggerContext.remove();
		}
	}

	public static void prepareLoggerContext(String ip, String method, String url, String userAgent, String requestId) {
		ActionRequestInfo requestInfo = ActionRequestInfo.newInstance();
		requestInfo.setIp(ip);
		requestInfo.setRequestMethod(method);
		requestInfo.setUrl(url);
		requestInfo.setUserAgent(userAgent);
		// requestLog.setUserId(); getting using session
		// requestLog.setUserName(); getting using session
		requestInfo.setThreadName(Thread.currentThread().getName());
		requestInfo.setRequestId(requestId);
		LoggerContext context = new LoggerContext();
		context.setRequestStartDateTime(new Date());
		context.setActionRequestInfo(requestInfo);
		LoggingHelper.setContext(context);
	}

	public static void writeLogs() {
		_LOGGER.info(" Request logs" + loggerContext.get().toString());
	}

}
