package com.cars24.core.framework.logging.bean;

import java.io.Serializable;

import lombok.Data;

/**
 * @author Sohan
 */
@Data
public class ActionRequestInfo implements Serializable {

	private static final long serialVersionUID = -4189515021206217295L;

	private Long userId;
	private String url;
	private String ip;
	private String userAgent;
	private String referer;
	private String requestMethod;
	private String threadName;
	private String userName;
	private String requestId;

	public static ActionRequestInfo newInstance() {
		return new ActionRequestInfo();
	}
}
