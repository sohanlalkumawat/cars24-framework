package com.cars24.core.framework.logging.utils;

import com.cars24.core.framework.logging.bean.ActionErrorLog;
import com.cars24.core.framework.logging.bean.ActionLog;
import com.cars24.core.framework.logging.bean.LoggerContext;
import com.cars24.core.framework.logging.helper.LoggingHelper;

public class LoggingUtils {

	private LoggingUtils() {
	}

	public static LoggerContext getContext() {
		return LoggingHelper.getContext();
	}

	public static void addActionLog(String action, Object data) {
		if (getContext() != null) {
			getContext().addActionLog(ActionLog.newInstance(action, data));
		}
	}

	public static void addActionError(ActionErrorLog errorLog) {
		if (getContext() != null) {
			getContext().setActionErrorLog(errorLog);
		}
	}

}
