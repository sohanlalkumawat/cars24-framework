package com.cars24.core.framework.logging.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.Data;

@Data
public class LoggerContext implements Serializable {

	private static final long serialVersionUID = 2247434825790987630L;

	private Date requestStartDateTime;

	private Date requestEndDateTime;

	private Long totalRequestTime;

	private List<ActionLog> actionLog;

	private ActionErrorLog actionErrorLog;

	private ActionRequestInfo actionRequestInfo;

	public void addActionLog(ActionLog log) {
		actionLog = actionLog == null ? new ArrayList<>() : actionLog;
		actionLog.add(log);
	}
}
