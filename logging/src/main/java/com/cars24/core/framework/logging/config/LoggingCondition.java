package com.cars24.core.framework.logging.config;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

import com.cars24.core.framework.common.constants.CommonConstants;

public class LoggingCondition implements Condition {

	@Override
	public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
		String loggingCondition = context.getEnvironment().getProperty(CommonConstants.LOGGING_ENABLE_CONDITION);
		return loggingCondition == null ? true : loggingCondition.equalsIgnoreCase("y");
	}
}
