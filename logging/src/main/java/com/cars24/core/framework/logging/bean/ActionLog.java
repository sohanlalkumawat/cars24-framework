package com.cars24.core.framework.logging.bean;

import java.io.Serializable;

import lombok.Data;

/**
 * <p>
 * ActionLog class.
 * </p>
 *
 * @author Sohan
 * @version $Id: $Id
 */

@Data
public class ActionLog implements Serializable {

	private static final long serialVersionUID = -4041730845656467576L;

	private Object data;
	private String actionName;

	public ActionLog(String actionName, Object data) {
		this.actionName = actionName;
		this.data = data;
	}

	public static ActionLog newInstance(String actionName, Object data) {
		return new ActionLog(actionName, data);
	}
}
