package com.cars24.core.framework.logging.bean;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import lombok.Data;

@Data
public class ActionErrorLog implements Serializable {

	private static final long serialVersionUID = -959651240832182863L;

	static String EXCEPTION_LOG_TAG = "[ ** EXCEPTION IN LOGGING ** ] ";

	private List<String> errorStack;
	private String errorMessage;
	private Object errors;

	public static ActionErrorLog newInstance() {
		return new ActionErrorLog();
	}

	public static ActionErrorLog newInstance(Throwable t) {
		ActionErrorLog log = ActionErrorLog.newInstance();
		if (t != null) {
			List<String> stackTrace = Arrays.asList(t.getStackTrace()).stream().map(s -> s.toString())
					.collect(Collectors.toList());
			log.setErrorMessage(t.getMessage());
			log.setErrorStack(stackTrace);
		}
		return log;
	}

}