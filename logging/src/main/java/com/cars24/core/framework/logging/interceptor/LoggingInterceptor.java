package com.cars24.core.framework.logging.interceptor;

import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;

import com.cars24.core.framework.logging.helper.LoggingHelper;
import com.cars24.core.framework.utility.StringUtils;

/**
 * The Class LoggingInterceptor.
 *
 * @author Sohan
 */
public class LoggingInterceptor implements HandlerInterceptor {

	private static final Logger _LOGGER = LoggerFactory.getLogger(LoggingInterceptor.class);

	/** {@inheritDoc} */
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		LoggingHelper.prepareLoggerContext(getClientIpAddr(request), request.getMethod(),
				request.getRequestURL().toString(), getUserAgent(request), getRequestId(request));
		return true;
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {

		LoggingHelper.writeLogs();

		LoggingHelper.clearContext();

	}

	private static String getClientIpAddr(HttpServletRequest request) {
		String ip = request.getHeader("X-Forwarded-For");

		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_X_FORWARDED");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_X_CLUSTER_CLIENT_IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_CLIENT_IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_FORWARDED_FOR");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_FORWARDED");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_VIA");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("REMOTE_ADDR");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}

		try {
			if (StringUtils.isNotEmpty(ip) && ip.contains(",")) {
				ip = ip.split(",")[0];
			}
		} catch (Exception e) {
			_LOGGER.warn("Error in obtaining client ip address." + e.getMessage());
		}

		if (StringUtils.isNotBlank(ip) && ip.equalsIgnoreCase("0:0:0:0:0:0:0:1")) {
			ip = "127.0.0.1";
		}
		return ip;
	}

	private String getUserAgent(HttpServletRequest request) {
		String userAgent = request.getHeader("user-agent");
		if (StringUtils.isBlank(userAgent)) {
			userAgent = request.getHeader("User-Agent");
		}
		return userAgent;
	}

	private String getRequestId(HttpServletRequest request) {
		return UUID.randomUUID().toString();
	}
}
