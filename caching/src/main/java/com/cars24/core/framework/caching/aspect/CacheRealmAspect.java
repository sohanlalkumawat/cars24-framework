
package com.cars24.core.framework.caching.aspect;

import java.util.ArrayList;
import java.util.List;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import com.cars24.core.framework.caching.utils.CacheUtils;
import com.cars24.core.framework.common.constants.CommonConstants;
import com.cars24.core.framework.common.exception.ServerException;

/**
 * The Class RedisRealmAspect.
 *
 * @author Sohan
 */
@Aspect
@Component
@SuppressWarnings("unchecked")
public class CacheRealmAspect {

    /**
     * Before redis call.
     *
     * @param joinPoint the join point
     */
    @Before(CommonConstants.REDIS_REPOSITORY)
    public void beforeRedisCall(JoinPoint joinPoint) {
        List<String> keys = new ArrayList<>();
        if (joinPoint.getArgs()[0] instanceof String) {
            keys.add((String) joinPoint.getArgs()[0]);
        }
        if (joinPoint.getArgs()[0] instanceof List) {
            keys.addAll((List<String>) joinPoint.getArgs()[0]);
        }

        String localRealm = CacheUtils.Keys.getLocalCacheKeySpace();
        String globalRealm = CacheUtils.Keys.getSharedCacheKeySpace();

        for (String key : keys) {

            if (!(key.startsWith(localRealm) || key.startsWith(globalRealm) /* || key.startsWith(rowCache) */)) {
                throw new ServerException("Invalid Key Pattern. Expected keys to start with " + localRealm + ", " + globalRealm + " but found " + key,
                        null);
            }
        }
    }
}
