
package com.cars24.core.framework.caching.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.SerializationUtils;
import org.springframework.util.StringUtils;

import com.cars24.core.framework.common.constants.CommonConstants;

/**
 * The Class RedisRepositoryImpl.
 * @author Sohan
 */
@Component
@SuppressWarnings("unchecked")
public class CacheRepositoryImpl<T> implements CacheRepository<T> {

	/** The redis template. */
	@Autowired
	@Qualifier(CommonConstants.CACHE_TEMPLATE_QUALIFIER)
	private RedisTemplate<String, byte[]> redisTemplate;

	/** {@inheritDoc} */
	@Override
	public T find(String key) {
		if (!StringUtils.isEmpty(key)) {
			return (T) SerializationUtils.deserialize(redisTemplate.opsForValue().get(key));
		}
		return null;
	}

	/** {@inheritDoc} */
	@Override
	public List<T> findAll(String keyPattern) {
		if (!StringUtils.isEmpty(keyPattern)) {
			Set<String> keys = redisTemplate.keys("*" + keyPattern + "*");
			if (keys != null)
				return keys.stream().map(k -> (T) SerializationUtils.deserialize(redisTemplate.opsForValue().get(k))).collect(Collectors.toList());
		}
		return new ArrayList<>();
	}

	/** {@inheritDoc} */
	@Override
	public void save(String key, T object) {
		if (!StringUtils.isEmpty(key) && !ObjectUtils.isEmpty(object) && redisTemplate != null) {
			ValueOperations<String, byte[]> valueOps = redisTemplate.opsForValue();
			/**
			 * @FB :
			 *     RCN_REDUNDANT_NULLCHECK_OF_NONNULL_VALUE/NP_NULL_ON_SOME_PATH_FROM_RETURN_VALUE:LOW
			 */
			if (valueOps != null) {
				byte[] value = SerializationUtils.serialize(object);
				if (value != null)
					valueOps.set(key, value);
			}
		}
	}

	/** {@inheritDoc} */
	@Override
	public void save(String key, T object, Long timeout, TimeUnit timeunit) {
		if (!StringUtils.isEmpty(key) && !ObjectUtils.isEmpty(object) && redisTemplate != null) {
			ValueOperations<String, byte[]> valueOps = redisTemplate.opsForValue();
			if (valueOps != null) {
				/**
				 * @FB :
				 *     RCN_REDUNDANT_NULLCHECK_OF_NONNULL_VALUE/NP_NULL_ON_SOME_PATH_FROM_RETURN_VALUE:LOW
				 */
				byte[] value = SerializationUtils.serialize(object);
				if (value != null)
					valueOps.set(key, value, timeout, timeunit);
			}
		}
	}

	/** {@inheritDoc} */
	@Override
	public void update(String key, T entity) {
		if (!StringUtils.isEmpty(key) && !ObjectUtils.isEmpty(entity))
			save(key, entity);
	}

	/** {@inheritDoc} */
	@Override
	public void update(String key, T entity, Long timeout, TimeUnit timeunit) {
		if (!StringUtils.isEmpty(key) && !ObjectUtils.isEmpty(entity))
			save(key, entity, timeout, timeunit);
	}

	/** {@inheritDoc} */
	@Override
	public void remove(String key) {
		if (!StringUtils.isEmpty(key))
			redisTemplate.delete(key);
	}

	/** {@inheritDoc} */
	@Override
	public void remove(List<String> keys) {
		if (!CollectionUtils.isEmpty(keys))
			redisTemplate.delete(keys);
	}

	/** {@inheritDoc} */
	@Override
	public void removePattern(String keysPattern) {
		if (!StringUtils.isEmpty(keysPattern)) {
			Set<String> keys = redisTemplate.keys("*" + keysPattern + "*");
			if (!CollectionUtils.isEmpty(keys)) {
				redisTemplate.delete(keys);
			}
		}
	}

	/** {@inheritDoc} */
	@Override
	public List<T> find(List<String> keys) {
		if (!CollectionUtils.isEmpty(keys))
			return keys.stream().map(this::find).filter(o -> !ObjectUtils.isEmpty(o)).collect(Collectors.toList());
		return new ArrayList<>();
	}

	/** {@inheritDoc} */
	@Override
	public void saveBytes(String key, byte[] object) {
		redisTemplate.opsForValue().set(key, object);
	}

	/** {@inheritDoc} */
	@Override
	public byte[] findBytes(String key) {
		return redisTemplate.opsForValue().get(key);
	}

	/** {@inheritDoc} */
	@Override
	public boolean exists(String key) {
		if (!Objects.isNull(key)) {
			Boolean hasKey = redisTemplate.hasKey(key);
			return Objects.isNull(hasKey) ? false : hasKey;
		}
		return false;
	}

	/** {@inheritDoc} */
	@Override
	public void setTimeout(String key, Long timeout, TimeUnit timeunit) {
		redisTemplate.expire(key, timeout, timeunit);
	}

	/** {@inheritDoc} */
	@Override
	public List<String> keys(String pattern) {
		Set<String> keys = redisTemplate.keys("*" + pattern + "*");
		return !CollectionUtils.isEmpty(keys) ? new ArrayList<>(keys) : new ArrayList<>();
	}

	// @Override
	// public void removeAllRows(List<T> objects) {
	// if (!CollectionUtils.isEmpty(objects)) {
	// remove(objects.stream().map(o ->
	// RedisUtils.Keys.getDbCacheRowKey(o.getClass(),
	// RedisUtils.getEntityId(o)))
	// .collect(Collectors.toList()));
	// }
	// }

	// @Override
	// public void saveEntities(List<T> object) {
	// if (!CollectionUtils.isEmpty(object))
	// object.stream().forEach(o ->
	// save(RedisUtils.Keys.getDbCacheRowKey(o.getClass(),
	// RedisUtils.getEntityId(o)), o));
	// }

	// @Override
	// public void saveEntities(List<T> object, Long timeout, TimeUnit timeunit) {
	// if (!CollectionUtils.isEmpty(object))
	// object.stream()
	// .forEach(o -> save(RedisUtils.Keys.getDbCacheRowKey(o.getClass(),
	// RedisUtils.getEntityId(o)), o,
	// timeout, timeunit));
	// }

	// @Override
	// public void removeAllByEntity(Class<?> type) {
	// String entityKeyPattern = RedisUtils.Keys.getDbCacheEntityKeyPattern(type);
	// StringBuilder pattern = new
	// StringBuilder().append("*").append(entityKeyPattern).append("*");
	// Set<String> keys = redisTemplate.keys(pattern.toString());
	// if (!CollectionUtils.isEmpty(keys))
	// keys.stream().forEach(this::remove);
	// }

	// @Override
	// public List<T> findEntities(Class<T> type, List<ID> ids) {
	// if (!CollectionUtils.isEmpty(ids) && type != null)
	// return find(ids.stream().map(id ->
	// RedisUtils.Keys.getDbCacheRowKey(type,
	// id)).collect(Collectors.toList()));
	// return new ArrayList<>();
	//
	// }

	// @Override
	// public void removeByEntityId(Class<?> type, Long id) {
	// remove(RedisUtils.Keys.getDbCacheRowKey(type, id));
	// removePattern(RedisUtils.Keys.getDbCacheAggregateKeyPattern(type));
	// }

}
