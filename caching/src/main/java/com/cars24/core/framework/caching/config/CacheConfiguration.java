
package com.cars24.core.framework.caching.config;

import java.time.Duration;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisClientConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.util.Assert;

import com.cars24.core.framework.caching.repository.CacheRepository;
import com.cars24.core.framework.caching.repository.CacheRepositoryImpl;
import com.cars24.core.framework.common.constants.CommonConstants;

/**
 * The Class RedisConfiguration.
 * @author Sohan
 */
@Configuration
public class CacheConfiguration {

	private static final Logger _LOGGER = LoggerFactory.getLogger(CacheConfiguration.class);

	/** The Constant DEFAULT_APP_NAME. */
	private static final String DEFAULT_APP_NAME = "default_app";

	/** The external config. */

	/** The env. */
	@Autowired
	private Environment env;

	/**
	 * Redis template.
	 *
	 * @return the redis template
	 */
	@Bean(CommonConstants.CACHE_TEMPLATE_QUALIFIER)
	public RedisTemplate<String, byte[]> redisTemplate() {

		Assert.notNull(env.getProperty(CommonConstants.SPRNG_APPLICATION_NAME_KEY, DEFAULT_APP_NAME),
				"RedisConfiguration Error. " + CommonConstants.SPRNG_APPLICATION_NAME_KEY
						+ " property must be specified in application.properties for the boot application to start using redis template");

		RedisTemplate<String, byte[]> template = new RedisTemplate<>();
		template.setConnectionFactory(jedisConnectionFactory());
		template.setKeySerializer(new StringRedisSerializer());
		return template;
	}

	@Bean
	@SuppressWarnings("rawtypes")
	public JedisConnectionFactory jedisConnectionFactory() {

		RedisCacheConfig redisConfig = new RedisCacheConfig();
		_LOGGER.info("" + redisConfig.getHost());
		_LOGGER.info("" + redisConfig.getPort());

		RedisStandaloneConfiguration config = new RedisStandaloneConfiguration();
		config.setHostName(redisConfig.getHost());
		config.setPort(redisConfig.getPort());

		if (redisConfig.getUsePooling()) {

			GenericObjectPoolConfig poolConfig = new GenericObjectPoolConfig();
			poolConfig.setMinIdle(redisConfig.getMinIdle());
			JedisClientConfiguration clientConfig = JedisClientConfiguration.builder()
					.readTimeout(Duration.ofSeconds(redisConfig.getReadTimeout()))
					.connectTimeout(Duration.ofSeconds(redisConfig.getConnectTimeout())).clientName("jedis-client")
					.usePooling().poolConfig(poolConfig).build();
			return new JedisConnectionFactory(config, clientConfig);
		} else {
			return new JedisConnectionFactory(config);
		}
	}

	@Bean
	@Primary
	public <T> CacheRepository<T> cacheRepository() {
		return new CacheRepositoryImpl<>();
	}
}
