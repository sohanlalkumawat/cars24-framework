package com.cars24.core.framework.caching.config;

import lombok.Data;

@Data
public class RedisCacheConfig {

	/** The timeout. */
	Long timeou = 1000L;

	/** The timeout unit. */
	String timeoutUnit;

	/** The host. */
	String host = "127.0.0.1";

	/** The port. */
	Integer port = 6379;

	/** The password. */
	String password;

	Long readTimeout = 20L;

	Long connectTimeout = 30L;

	Integer minIdle = 1;
	Boolean usePooling = Boolean.FALSE;

}
