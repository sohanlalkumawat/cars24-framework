
package com.cars24.core.framework.caching.repository;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * The Interface RedisRepository.
 * @author Sohan
 */
public interface CacheRepository<T> {

    /**
     * Find.
     *
     * @param key the key
     * @return the t
     */
    T find(String key);

    /**
     * Find.
     *
     * @param keys the keys
     * @return the list
     */
    List<T> find(List<String> keys);

    /**
     * Find all.
     *
     * @param keyPattern the key pattern
     * @return the list
     */
    List<T> findAll(String keyPattern);

    /**
     * Find bytes.
     *
     * @param key the key
     * @return the byte[]
     */
    byte[] findBytes(String key);

    /**
     * Save.
     *
     * @param key the key
     * @param object the object
     */
    void save(String key, T object);

    /**
     * Save.
     *
     * @param key the key
     * @param object the object
     * @param timeout the timeout
     * @param timeunit the timeunit
     */
    void save(String key, T object, Long timeout, TimeUnit timeunit);

    /**
     * Save bytes.
     *
     * @param key the key
     * @param object the object
     */
    void saveBytes(String key, byte[] object);

    /**
     * Update.
     *
     * @param key the key
     * @param object the object
     */
    void update(String key, T object);

    /**
     * Update.
     *
     * @param key the key
     * @param object the object
     * @param timeout the timeout
     * @param timeunit the timeunit
     */
    void update(String key, T object, Long timeout, TimeUnit timeunit);

    /**
     * Removes the.
     *
     * @param key the key
     */
    void remove(String key);

    /**
     * Removes the.
     *
     * @param keys the keys
     */
    void remove(List<String> keys);

    /**
     * Removes the pattern.
     *
     * @param keysPattern the keys pattern
     */
    void removePattern(String keysPattern);

    /**
     * Exists.
     *
     * @param key the key
     * @return a boolean.
     */
    boolean exists(String key);

    /**
     * Sets the timeout.
     *
     * @param key the key
     * @param timeout the timeout
     * @param timeunit the timeunit
     */
    void setTimeout(String key, Long timeout, TimeUnit timeunit);
    
    /**
     * <p>keys.</p>
     *
     * @param pattern a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    List<String> keys(String pattern);

}
