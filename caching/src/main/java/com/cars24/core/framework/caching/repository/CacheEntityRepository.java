
package com.cars24.core.framework.caching.repository;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * The Interface RedisEntityRepository.
 
 * @author Sohan
 */
public interface CacheEntityRepository<T, ID> extends CacheRepository<T> {

    /**
     * Removes the all rows.
     *
     * @param objects the objects
     */
    void removeAllRows(List<T> objects);

    /**
     * Removes the all by entity.
     *
     * @param type the type
     */
    void removeAllByEntity(Class<?> type);

    /**
     * Removes the by entity id.
     *
     * @param type the type
     * @param id the id
     */
    void removeByEntityId(Class<?> type, Long id);

    /**
     * Save entities.
     *
     * @param object the object
     * @param timeout the timeout
     * @param timeunit the timeunit
     */
    void saveEntities(List<T> object, Long timeout, TimeUnit timeunit);

    /**
     * Save entities.
     *
     * @param object the object
     */
    void saveEntities(List<T> object);

    /**
     * Find entities.
     *
     * @param type the type
     * @param ids the ids
     * @return the list
     */
    List<T> findEntities(Class<T> type, List<ID> ids);

}
