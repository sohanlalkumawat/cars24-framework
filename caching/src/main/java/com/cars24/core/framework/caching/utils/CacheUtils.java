
package com.cars24.core.framework.caching.utils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.lang.NonNull;
import org.springframework.util.Assert;

import com.cars24.core.framework.common.constants.CommonConstants;
import com.cars24.core.framework.common.exception.ConfigurationException;
import com.cars24.core.framework.common.utils.ContextUtils;

/**
 * @author Sohan
 */
public class CacheUtils {

	private static final Logger _LOGGER = LoggerFactory.getLogger(CacheUtils.class);

	/**
	 * Instantiates a new redis utils.
	 */
	private CacheUtils() {
	}

	/**
	 * The entity table mapping. Keeps a mapping of table name for entity class so
	 * next calls after first evaluation do not read annotation of table.
	 */
	private static Map<String, String> entityTableMapping = new HashMap<>();

	/** The cached entity mapping. Cacheable Entities Map */
	private static Map<String, Boolean> cachedEntityMapping = new HashMap<>();

	/**
	 * The Cache Realm on Redis Server.
	 */
	public enum Realm {

		/** The database. - for row caching */
		// db_cache,
		/** The local. local application cache key space */
		local_cache,
		/** The shared. shared application cache key space */
		shared_cache
	}

	/**
	 * Gets the entity table name.
	 *
	 * @param type the type of Entity
	 * @return the entity table name
	 */
	@SuppressWarnings("unused")
	private static String getEntityTableName(Class<?> type) {

		if (entityTableMapping.get(type.getName()) != null) {
			return entityTableMapping.get(type.getName());
		}

		String table = null;
		Annotation[] annotations = type.getDeclaredAnnotations();
		if (annotations != null && annotations.length > 0) {
			for (Annotation annotation : annotations) {
				if (annotation.annotationType().getName().equalsIgnoreCase(CommonConstants.TABLE_ANNOTATION)) {
					try {
						table = (String) annotation.annotationType().getMethod("name", CommonConstants.NO_CLASS)
								.invoke(annotation, CommonConstants.NO_ARGS);

						Assert.notNull(table, "Table must not be nul");
						entityTableMapping.put(type.getName(), table);
						break;
					} catch (Exception e) {
						/**
						 * @FB : REC_CATCH_EXCEPTION:MEDIUM
						 */
						throw new ConfigurationException(
								"Name attribute is not defined in @Table annotation for entity class : "
										+ type.getName());
					}
				}
			}
		}
		return table;
	}

	/**
	 * Gets the entity id.
	 *
	 * @param        <ID> the generic type
	 * @param object the object
	 * @return the entity id
	 */
	@SuppressWarnings({ "unchecked", "unused" })
	private static <ID> ID getEntityId(Object object) {
		ID value = null;
		try {
			/**
			 * @FB : REC_CATCH_EXCEPTION:LOW
			 */
			Method method = object.getClass().getMethod("getId", CommonConstants.NO_CLASS);
			value = (ID) method.invoke(object, CommonConstants.NO_ARGS);
		} catch (Exception e) {
			_LOGGER.error(e.getMessage(), e);
		}
		return value;
	}

	/**
	 * Gets the entity table mapping.
	 *
	 * @return the entity table mapping
	 */
	@SuppressWarnings("unused")
	private static Map<String, String> getEntityTableMapping() {
		return entityTableMapping;
	}

	/**
	 * Gets the cached entity mapping.
	 *
	 * @return the cached entity mapping
	 */
	@SuppressWarnings("unused")
	private static Map<String, Boolean> getCachedEntityMapping() {
		return cachedEntityMapping;
	}

	/**
	 * The Interface Keys- Redis Keys formats available for user.
	 */
	public interface Keys {

		/**
		 * Local Realm Cache key. - This will create a new key in following format and
		 * can be read from the same application writing the value on this key. Key
		 * Format : <b>profile.local_cache.local_cache_id.data_key</b> <br>
		 * Example : <b>dev.local_cache.users_app.sampleKey</b>
		 *
		 * @param dataKey the data key
		 * @return the Redis Key in a local cache key format.
		 */
		public static String newLocalKey(final String dataKey) {
			return new StringBuilder().append(getLocalCacheKeySpace()).append(".").append(dataKey).toString();
		}

		/**
		 * Local Realm Cache key. - This will create a new key in following format and
		 * can be read from the same application writing the value on this key. Key
		 * Format : <b>profile.local_cache.local_cache_id.data_key</b> <br>
		 * Example : <b>dev.local_cache.users_app.sampleKey</b>
		 *
		 * @param dataKey the data key
		 * @return the Redis Key in a local cache key format.
		 */
		public static String newLocalKey(final String dataKey, @NonNull String group) {
			Assert.notNull(group, "Group Cannot be null");
			return new StringBuilder().append(getLocalCacheKeySpace()).append(".").append(group).append(".")
					.append(dataKey).toString();
		}

		/**
		 * Shared Realm Cache Key. This will create a new key in following format and
		 * can be read from all application writing the value on this key. Key Format :
		 * <b>profile.shared_cache.data_key</b> <br>
		 * Example : <b>dev.shared_cache.sampleKey</b>
		 *
		 * @param dataKey the data key
		 * @return the Redis Key in a shared cache key format.
		 */
		public static String newSharedKey(final String dataKey) {
			return new StringBuilder().append(getSharedCacheKeySpace()).append(".").append(dataKey).toString();
		}

		/**
		 * Shared Realm Cache Key. This will create a new key with added group in
		 * following format and can be read from all application writing the value on
		 * this key. Key Format : <b>profile.shared_cache.group.data_key</b> <br>
		 * Example : <b>dev.shared_cache.properties.sampleKey</b>
		 *
		 * @param dataKey the data key
		 * @return the Redis Key in a shared cache key format.
		 */
		public static String newSharedKey(final String dataKey, @NonNull String group) {
			Assert.notNull(group, "Group Cannot be null");
			return new StringBuilder().append(getSharedCacheKeySpace()).append(".").append(group).append(".")
					.append(dataKey).toString();
		}

		/**
		 * Shared Realm Cache Key. This will create a new key in following format and
		 * can be read from the same application writing the value on this key. Key
		 * Format : <b>profile.shared_cache.data_key</b> <br>
		 * Example : <b>dev.shared_cache.sampleKey</b>
		 *
		 * @param dataKey the data key
		 * @return the Redis Key in a shared cache key format.
		 */
		public static String newSessionKey(final String dataKey) {
			return new StringBuilder().append(getSharedCacheKeySpace()).append(".sessions.").append(dataKey).toString();
		}

		/**
		 * Gets the local cache prefix. Example :
		 * <b>dev.local_cache.{application_name}</b>
		 *
		 * @return the local cache prefix
		 */
		public static String getLocalCacheKeySpace() {
			String profile = ContextUtils.getActiveProfile();
			String localId = ContextUtils.getLocalCacheId();
			return new StringBuilder().append(profile).append(".").append(Realm.local_cache.toString()).append(".")
					.append(localId).toString();
		}

		/**
		 * Gets the local cache prefix with group. Example :
		 * <b>dev.local_cache.{application_name}.params</b>
		 *
		 * @return the local cache prefix with group
		 */
		public static String getLocalCacheKeySpace(@NonNull String group) {
			String profile = ContextUtils.getActiveProfile();
			String localCacheId = ContextUtils.getLocalCacheId();
			return new StringBuilder().append(profile).append(".").append(Realm.local_cache.toString()).append(".")
					.append(localCacheId).append(".").append(group).toString();
		}

		/**
		 * Gets the shared cache prefix. Example : <b>dev.shared_cache</b>
		 *
		 * @return the shared cache prefix
		 */
		public static String getSharedCacheKeySpace() {
			String profile = ContextUtils.getActiveProfile();
			return new StringBuilder().append(profile).append(".").append(Realm.shared_cache).toString();
		}

		/**
		 * Gets the shared cache prefix with group. Example : <b>dev.shared_cache</b>
		 *
		 * @return the shared cache prefix
		 */
		public static String getSharedCacheKeySpace(@NonNull String group) {
			String profile = ContextUtils.getActiveProfile();
			return new StringBuilder().append(profile).append(".").append(Realm.shared_cache).append(".").append(group)
					.toString();
		}

		/**
		 * Gets the session cache prefix. Example : <b>dev.shared_cache.sessions</b>
		 *
		 * @return the session cache prefix
		 */
		public static String getSessionKeySpace() {
			String profile = ContextUtils.getActiveProfile();
			return new StringBuilder().append(profile).append(".").append(Realm.shared_cache).append(".")
					.append("sessions").toString();
		}

		/**
		 * Gets the properties cache prefix. Example :
		 * <b>dev.shared_cache.properties</b>
		 *
		 * @return the properties cache prefix
		 */
		public static String getSharedPropertiesKeySpace() {
			String profile = ContextUtils.getActiveProfile();
			return new StringBuilder().append(profile).append(".").append(Realm.shared_cache).append(".")
					.append("properties").toString();
		}

	}
}
