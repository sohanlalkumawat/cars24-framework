package com.cars24.core.framework.file.upload.store;

import java.io.IOException;
import java.io.InputStream;

/**
 * The Interface supported by an Email Operation.
 *
 * @author Sohan
 * @version $Id: $Id
 */
public interface FileStore {
	/**
	 * <p>
	 * uploadFile.
	 * </p>
	 *
	 * @param location    a {@link java.lang.String} object.
	 * @param file        a {@link java.lang.String} object.
	 * @param contentType a {@link java.lang.String} object.
	 * @param inputStream a {@link java.io.InputStream} object.
	 * @return a boolean.
	 */
	boolean uploadFile(String location, String file, String contentType, InputStream inputStream);

	/**
	 * <p>
	 * uploadFile.
	 * </p>
	 *
	 * @param file        a {@link java.lang.String} object.
	 * @param contentType a {@link java.lang.String} object.
	 * @param inputStream a {@link java.io.InputStream} object.
	 * @return a boolean.
	 */
	boolean uploadFile(String file, String contentType, InputStream inputStream);

	/**
	 * <p>
	 * uploadImageFile.
	 * </p>
	 *
	 * @param file        a {@link java.lang.String} object.
	 * @param inputStream a {@link java.io.InputStream} object.
	 * @return a boolean.
	 */
	boolean uploadImageFile(String file, InputStream inputStream);

	/**
	 * <p>
	 * readFile.
	 * </p>
	 *
	 * @param bucketName a {@link java.lang.String} object.
	 * @param key        a {@link java.lang.String} object.
	 * @return a {@link java.io.InputStream} object.
	 * @throws java.io.IOException if any.
	 */
	InputStream readFile(String bucketName, String key) throws IOException;
}
