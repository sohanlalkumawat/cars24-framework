package com.cars24.core.framework.file.upload.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

import lombok.Data;

/**
 * S3 Config Bean
 *
 * @author Sohan
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "amazon.s3")
public class AWSS3Config {

	private String accessKey;
	private String secretKey;
	private String bucketName;
	private String region;

	@Bean
	public AmazonS3 AWSS3Client() {
		BasicAWSCredentials creds = new BasicAWSCredentials(getAccessKey(), getSecretKey());
		AmazonS3 s3client = AmazonS3ClientBuilder.standard().withRegion(Regions.AP_SOUTH_1)
				.withCredentials(new AWSStaticCredentialsProvider(creds)).build();
		return s3client;
	}

}
