package com.cars24.core.framework.file.upload.utils;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.amazonaws.services.s3.AmazonS3;
import com.cars24.core.framework.file.upload.config.AWSS3Config;

@Component
public class AWSUtils {

	static AWSS3Config awsS3Config = null;

	public static AmazonS3 getS3Clent() {
		return awsS3Config.AWSS3Client();
	}

	@PostConstruct
	private void AWSInit(@Autowired AWSS3Config awsS3Config) {
		this.awsS3Config = awsS3Config;
	}

}
