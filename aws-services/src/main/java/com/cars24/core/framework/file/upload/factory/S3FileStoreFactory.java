package com.cars24.core.framework.file.upload.factory;

import org.springframework.stereotype.Component;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

@Component
public class S3FileStoreFactory {

	public AmazonS3 getS3() {
		return AmazonS3ClientBuilder.standard().withRegion(Regions.AP_SOUTH_1).build();
	}
}
