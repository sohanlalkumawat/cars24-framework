package com.cars24.core.framework.file.upload.store;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.S3Object;
import com.cars24.core.framework.file.upload.config.AWSS3Config;
import com.cars24.core.framework.file.upload.factory.S3FileStoreFactory;

/**
 * The Class S3FileStore.
 *
 * @author Sohan
 */
@Component
public class S3FileStore implements FileStore {

	// private static final Logger _LOGGER =
	// LoggerFactory.getLogger(S3FileStore.class);

	@Autowired
	private S3FileStoreFactory s3FileStoreFactory;

	@Autowired
	private AWSS3Config s3Config;

	/** {@inheritDoc} */
	@Override
	public boolean uploadFile(String bucketName, String key, String contentType, InputStream inputStream) {
		PutObjectRequest putObjectRequest = null;
		PutObjectResult putObjectResult = null;
		ObjectMetadata objectMetadata = null;
		boolean isFileSaved = false;
		try {
			// _LOGGER.info("BucketName: " + bucketName + ", Content-Type: " + contentType +
			// ", Key: " + key + ", Modified Date: " + new Date());
			objectMetadata = new ObjectMetadata();
			objectMetadata.setContentType(contentType);
			objectMetadata.setLastModified(new Date());
			putObjectRequest = new PutObjectRequest(bucketName, key, inputStream, objectMetadata);
			putObjectResult = s3FileStoreFactory.getS3().putObject(putObjectRequest);
			if (putObjectResult != null) {
				isFileSaved = true;
			}
		} catch (Exception e) {
			// _LOGGER.error(e.getMessage(), e);
		}
		return isFileSaved;
	}

	/** {@inheritDoc} */
	@Override
	public boolean uploadFile(String key, String contentType, InputStream inputStream) {
		return this.uploadFile(s3Config.getBucketName(), key, contentType, inputStream);
	}

	/** {@inheritDoc} */
	@Override
	public boolean uploadImageFile(String key, InputStream inputStream) {
		return this.uploadFile(s3Config.getBucketName(), key, "image/custom", inputStream);
	}

	/** {@inheritDoc} */
	@Override
	public InputStream readFile(String bucketName, String key) throws IOException {
		S3Object s3Object = s3FileStoreFactory.getS3().getObject(bucketName, key);

		return s3Object.getObjectContent();
	}
}
