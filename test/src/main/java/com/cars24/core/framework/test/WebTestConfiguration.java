package com.cars24.core.framework.test;

import static org.testng.Assert.assertEquals;

import java.io.File;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.util.CollectionUtils;
import org.testng.log4testng.Logger;

import com.cars24.core.framework.common.constants.ApplicationProfiles;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

/**
 * The Class WebTestConfiguration.
 *
 * @author Sohan
 */
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = { WebComponentsConfig.class })
@Profile(value = ApplicationProfiles.TEST)
@ActiveProfiles(value = ApplicationProfiles.TEST)
public class WebTestConfiguration extends AbstractTestNGSpringContextTests {

	/** The Constant _LOGGER. */
	 private static final Logger  _LOGGER =
	 Logger.getLogger(WebTestConfiguration.class);

	/** The Constant APPLICATION_JSON. */
	private static final String APPLICATION_JSON = "application/json";

	/** The Constant MULTIPART_TYPE. */
	private static final String MULTIPART_TYPE = "multipart/form-data";

	/** The port. */
	@LocalServerPort
	private int port;

	/**
	 * Gets the.
	 *
	 * @param handler       the handler
	 * @param headers       the headers
	 * @param requestParams the request params
	 * @param pathParams    the path params
	 * @return the response
	 */
	protected Response get(String handler, Map<String, String> headers, Map<String, String> requestParams,
			Map<String, String> pathParams) {
		final RequestSpecification requestSpecs = prepareRequestSpecs(headers, requestParams, pathParams);
		return requestSpecs.accept(APPLICATION_JSON).when().get(basePath() + handler);
	}

	/**
	 * Post.
	 *
	 * @param handler       the handler
	 * @param payloadObject the payload object
	 * @param headers       the headers
	 * @param requestParams the request params
	 * @param pathParams    the path params
	 * @return the response
	 */
	protected Response post(String handler, Object payloadObject, Map<String, String> headers,
			Map<String, String> requestParams, Map<String, String> pathParams) {
		final RequestSpecification requestSpecs = prepareRequestSpecs(headers, requestParams, pathParams);
		return requestSpecs.accept(APPLICATION_JSON).contentType(APPLICATION_JSON).with().body(asString(payloadObject))
				.post(basePath() + handler);
	}

	/**
	 * Post.
	 *
	 * @param handler       the handler
	 * @param payloadObject the payload object
	 * @param headers       the headers
	 * @param requestParams the request params
	 * @param pathParams    the path params
	 * @return the response
	 */
	protected Response post(String handler, Object payloadObject, Map<String, String> headers,
			Map<String, String> requestParams, Map<String, String> pathParams, File file) {
		final RequestSpecification requestSpecs = prepareRequestSpecs(headers, requestParams, pathParams);
		return requestSpecs.multiPart("file", file).accept("*/*").contentType(MULTIPART_TYPE).with()
				.body(asString(payloadObject)).post(basePath() + handler);
	}

	protected Response postMultipart(String handler, Map<String, String> headers, Map<String, String> requestParams,
			Map<String, String> pathParams, File file) {
		final RequestSpecification requestSpecs = prepareRequestSpecs(headers, requestParams, pathParams);
		return requestSpecs.multiPart("file", file).accept("*/*").contentType(MULTIPART_TYPE).with()
				.post(basePath() + handler);
	}

	/**
	 * Patch.
	 *
	 * @param handler       the handler
	 * @param payloadObject the payload object
	 * @param headers       the headers
	 * @param requestParams the request params
	 * @param pathParams    the path params
	 * @return the response
	 */
	protected Response patch(String handler, Object payloadObject, Map<String, String> headers,
			Map<String, String> requestParams, Map<String, String> pathParams) {
		final RequestSpecification requestSpecs = prepareRequestSpecs(headers, requestParams, pathParams);
		return requestSpecs.accept(APPLICATION_JSON).contentType(APPLICATION_JSON).with().body(asString(payloadObject))
				.patch(basePath() + handler);
	}

	/**
	 * Put.
	 *
	 * @param handler       the handler
	 * @param payloadObject the payload object
	 * @param headers       the headers
	 * @param requestParams the request params
	 * @param pathParams    the path params
	 * @return the response
	 */
	protected Response put(String handler, Object payloadObject, Map<String, String> headers,
			Map<String, String> requestParams, Map<String, String> pathParams) {
		final RequestSpecification requestSpecs = prepareRequestSpecs(headers, requestParams, pathParams);
		return requestSpecs.accept(APPLICATION_JSON).contentType(APPLICATION_JSON).with().body(asString(payloadObject))
				.put(basePath() + handler);
	}

	/**
	 * Delete.
	 *
	 * @param handler       the handler
	 * @param headers       the headers
	 * @param requestParams the request params
	 * @param pathParams    the path params
	 * @return the response
	 */
	protected Response delete(String handler, Map<String, String> headers, Map<String, String> requestParams,
			Map<String, String> pathParams) {
		final RequestSpecification requestSpecs = prepareRequestSpecs(headers, requestParams, pathParams);
		return requestSpecs.accept(APPLICATION_JSON).contentType(APPLICATION_JSON).delete(basePath() + handler);
	}

	/**
	 * Prepare request specs.
	 *
	 * @param headers       the headers
	 * @param requestParams the request params
	 * @param pathParams    the path params
	 * @return the request specification
	 */
	protected RequestSpecification prepareRequestSpecs(Map<String, String> headers, Map<String, String> requestParams,
			Map<String, String> pathParams) {
		RequestSpecification requestSpecs = null;
		if (CollectionUtils.isEmpty(headers)) {
			headers = new HashMap<>();
		}

		headers.put("executor", "rest-assured");
		requestSpecs = RestAssured.given().with().headers(headers);
		if (!CollectionUtils.isEmpty(requestParams)) {
			requestSpecs = requestSpecs.params(requestParams);
		}
		if (!CollectionUtils.isEmpty(pathParams)) {
			requestSpecs = requestSpecs.pathParams(pathParams);
		}
		return requestSpecs;
	}

	/**
	 * Base path.
	 *
	 * @return the string
	 */
	protected String basePath() {
		return "http://localhost:" + port;
	}

	/**
	 * As string.
	 *
	 * @param payload the payload
	 * @return the string
	 */
	protected String asString(Object payload) {
		try {
			final ObjectMapper mapper = new ObjectMapper();
			return mapper.writeValueAsString(payload);
		} catch (final Exception e) {
			_LOGGER.error(e.getMessage(), e);
			return null;
		}
	}

	/**
	 * As object.
	 *
	 * @param         <T> the generic type
	 * @param payload the payload
	 * @param clazz   the clazz
	 * @return the t
	 */
	protected <T> T asObject(String payload, Class<T> clazz) {
		try {
			final ObjectMapper mapper = new ObjectMapper();
			return mapper.readValue(payload, clazz);
		} catch (final Exception e) {
			_LOGGER.error(e.getMessage(), e);
			return null;
		}
	}

	/**
	 * Gets the id path params.
	 *
	 * @param id the id
	 * @return the id path params
	 */
	protected Map<String, String> getIdPathParams(Long id) {
		final Map<String, String> ids = new HashMap<>();
		ids.put("id", id + "");
		return ids;
	}

	/**
	 * Do post.
	 *
	 * @param handler the handler
	 * @param headers the headers
	 * @param payload the payload
	 * @return the response
	 */
	protected Response doPost(String handler, Map<String, String> headers, Object payload) {
		return post(handler, payload, headers, null, null).then().statusCode(HttpStatus.CREATED.value()).extract()
				.response();
	}

	/**
	 * Do post.
	 *
	 * @param handler    the handler
	 * @param headers    the headers
	 * @param payload    the payload
	 * @param pathParams the payload
	 * @return the response
	 */
	protected Response doPost(String handler, Map<String, String> headers, Map<String, String> pathParams,
			Object payload) {
		return post(handler, payload, headers, null, pathParams).then().statusCode(HttpStatus.CREATED.value()).extract()
				.response();
	}

	/**
	 * Do save.
	 *
	 * @param handler the handler
	 * @param headers the headers
	 * @param payload the payload
	 * @return the long
	 */
	protected Long doSave(String handler, Map<String, String> headers, Object payload) {
		// Response response = doPost(handler, headers, payload);
		final Response response = doCreate(handler, headers, payload);
		return asObject(response.body().asString(), Long.class);
	}

	/**
	 * Do bad post.
	 *
	 * @param handler the handler
	 * @param headers the headers
	 * @param payload the payload
	 * @return the response
	 */
	protected Response doBadPost(String handler, Map<String, String> headers, Object payload) {
		return post(handler, payload, headers, null, null).then().statusCode(HttpStatus.BAD_REQUEST.value()).extract()
				.response();
	}

	/**
	 * Do get.
	 *
	 * @param paramHandler the handler
	 * @param headers      the headers
	 * @param id           the id
	 * @return the response
	 */
	protected Response doGet(String paramHandler, Map<String, String> headers, Long id) {
		return doGet(paramHandler, headers, getIdPathParams(id));
	}

	/**
	 * Do get.
	 *
	 * @param paramHandler the handler
	 * @param headers      the headers
	 * @param pathParams   the path params
	 * @return the response
	 */
	protected Response doGet(String paramHandler, Map<String, String> headers, Map<String, String> pathParams) {
		return get(paramHandler, headers, null, pathParams).then().statusCode(HttpStatus.OK.value()).extract()
				.response();
	}

	/**
	 * Do get.
	 *
	 * @param paramHandler  the handler
	 * @param headers       the headers
	 * @param pathParams    the path params
	 * @param requestParams the request Param
	 * @return the response
	 */
	protected Response doGet(String paramHandler, Map<String, String> headers, Map<String, String> pathParams,
			Map<String, String> requestParams) {
		return get(paramHandler, headers, requestParams, pathParams).then().statusCode(HttpStatus.OK.value()).extract()
				.response();
	}

	/**
	 * Do getWithReq.
	 *
	 * @param handler   the handler
	 * @param headers   the headers
	 * @param reqParams the request params
	 * @return the response
	 */
	protected Response doGetWithReq(String handler, Map<String, String> headers, Map<String, String> reqParams) {
		return get(handler, headers, reqParams, null).then().statusCode(HttpStatus.OK.value()).extract().response();
	}

	/**
	 * Do get.
	 *
	 * @param handler    the handler
	 * @param headers    the headers
	 * @param pathParams the path params
	 * @return the response
	 */
	protected Response doGet(String handler, Map<String, String> headers) {
		return get(handler, headers, null, null).then().statusCode(HttpStatus.OK.value()).extract().response();
	}

	/**
	 * Do find.
	 *
	 * @param              <B> the generic type
	 * @param paramHandler the handler
	 * @param headers      the headers
	 * @param id           the id
	 * @param bean         the bean
	 * @return the b
	 */
	protected <B> B doFind(String paramHandler, Map<String, String> headers, Long id, B bean) {
		return doFind(paramHandler, headers, getIdPathParams(id), bean);
	}

	/**
	 * Do find.
	 *
	 * @param               <B> the generic type
	 * @param paramHandler  the handler
	 * @param headers       the headers
	 * @param id            the id the handler
	 * @param requestParams the request Param
	 * @param bean          the bean
	 * @return the b
	 */
	protected <B> B doFind(String paramHandler, Map<String, String> headers, Long id, Map<String, String> requestParams,
			B bean) {
		return doFind(paramHandler, headers, getIdPathParams(id), requestParams, bean);
	}

	/**
	 * Do not found get.
	 *
	 * @param paramHandler the handler
	 * @param headers      the headers
	 * @param pathParams   the path params
	 * @return the response
	 */
	protected Response doGetNotFound(String paramHandler, Map<String, String> headers, Long id) {
		return doGetNotFound(paramHandler, headers, getIdPathParams(id));
	}

	/**
	 * Do not found get.
	 *
	 * @param paramHandler the handler
	 * @param headers      the headers
	 * @param pathParams   the path params
	 * @return the response
	 */
	protected Response doGetNotFound(String paramHandler, Map<String, String> headers, Map<String, String> pathParams) {
		return get(paramHandler, headers, null, pathParams).then().statusCode(HttpStatus.NOT_FOUND.value()).extract()
				.response();
	}

	/**
	 * Do find.
	 *
	 * @param               <B> the generic type
	 * @param paramHandler  the handler
	 * @param headers       the headers
	 * @param pathParams    the path params
	 * @param requestParams the request Params
	 * @param bean          the bean
	 * @return the b
	 */
	@SuppressWarnings("unchecked")
	protected <B> B doFind(String paramHandler, Map<String, String> headers, Map<String, String> pathParams,
			Map<String, String> requestParams, B bean) {
		final Response response = doGet(paramHandler, headers, pathParams, requestParams);
		return asObject(response.body().asString(), (Class<B>) bean.getClass());
	}

	/**
	 * Do find.
	 *
	 * @param              <B> the generic type
	 * @param paramHandler the handler
	 * @param headers      the headers
	 * @param pathParams   the path params
	 * @param bean         the bean
	 * @return the b
	 */
	@SuppressWarnings("unchecked")
	protected <B> B doFind(String paramHandler, Map<String, String> headers, Map<String, String> pathParams, B bean) {
		final Response response = doGet(paramHandler, headers, pathParams);
		return asObject(response.body().asString(), (Class<B>) bean.getClass());
	}

	/**
	 * Do put.
	 *
	 * @param paramHandler the handler
	 * @param headers      the headers
	 * @param pathParams   the path params
	 * @param payload      the payload
	 * @return the response
	 */
	protected Response doPut(String paramHandler, Map<String, String> headers, Map<String, String> pathParams,
			Object payload) {
		// return put(paramHandler, payload, headers, null,
		// pathParams).then().statusCode(HttpStatus.OK.value()).extract().response();
		return put(paramHandler, payload, headers, null, pathParams).then().statusCode(HttpStatus.NO_CONTENT.value())
				.extract().response();

	}

	/**
	 * Do put.
	 *
	 * @param paramHandler the handler
	 * @param headers      the headers
	 * @param id           the id
	 * @param payload      the payload
	 * @return the response
	 */
	protected Response doPut(String paramHandler, Map<String, String> headers, Long id, Object payload) {
		return doPut(paramHandler, headers, getIdPathParams(id), payload);
	}

	/**
	 * Do put.
	 *
	 * @param paramHandler the handler
	 * @param headers      the headers
	 * @param id           the id
	 * @param payload      the payload
	 * @return the response
	 */
	protected Response doPut(String paramHandler, Map<String, String> headers, Object payload) {
		return put(paramHandler, payload, headers, null, null).then().statusCode(HttpStatus.NO_CONTENT.value())
				.extract().response();
	}

	/**
	 * Do bad put.
	 *
	 * @param paramHandler the handler
	 * @param headers      the headers
	 * @param id           the id
	 * @param payload      the payload
	 * @return the response
	 */
	protected Response doBadPut(String paramHandler, Map<String, String> headers, Long id, Object payload) {
		return doBadPut(paramHandler, headers, getIdPathParams(id), payload);
	}

	/**
	 * Do bad put.
	 *
	 * @param paramHandler the handler
	 * @param headers      the headers
	 * @param pathParams   the path params
	 * @param payload      the payload
	 * @return the response
	 */
	protected Response doBadPut(String paramHandler, Map<String, String> headers, Map<String, String> pathParams,
			Object payload) {
		return put(paramHandler, payload, headers, null, pathParams).then().statusCode(HttpStatus.BAD_REQUEST.value())
				.extract().response();
	}

	/**
	 * Do patch.
	 *
	 * @param paramHandler the handler
	 * @param headers      the headers
	 * @param pathParams   the path params
	 * @param payload      the payload
	 * @return the response
	 */
	protected Response doPatch(String paramHandler, Map<String, String> headers, Map<String, String> pathParams,
			Object payload) {
		// return patch(paramHandler, payload, headers, null,
		// pathParams).then().statusCode(HttpStatus.OK.value()).extract().response();
		return patch(paramHandler, payload, headers, null, pathParams).then().statusCode(HttpStatus.NO_CONTENT.value())
				.extract().response();

	}

	/**
	 * Do patch.
	 *
	 * @param paramHandler the handler
	 * @param headers      the headers
	 * @param id           the id
	 * @param payload      the payload
	 * @return the response
	 */
	protected Response doPatch(String paramHandler, Map<String, String> headers, Long id, Object payload) {
		return doPatch(paramHandler, headers, getIdPathParams(id), payload);
	}

	/**
	 * Do not found patch.
	 *
	 * @param paramHandler the handler
	 * @param headers      the headers
	 * @param pathParams   the path params
	 * @return the response
	 */
	protected Response doNotFoundPatch(String paramHandler, Map<String, String> headers, Map<String, String> pathParams,
			Object payload) {
		return patch(paramHandler, payload, headers, null, pathParams).then().statusCode(HttpStatus.NOT_FOUND.value())
				.extract().response();
	}

	/**
	 * Do bad patch.
	 *
	 * @param paramHandler the handler
	 * @param headers      the headers
	 * @param pathParams   the path params
	 * @param payload      the payload
	 * @return the response
	 */
	protected Response doBadPatch(String paramHandler, Map<String, String> headers, Map<String, String> pathParams,
			Object payload) {
		return patch(paramHandler, payload, headers, null, pathParams).then().statusCode(HttpStatus.BAD_REQUEST.value())
				.extract().response();
	}

	/**
	 * Do bad patch.
	 *
	 * @param paramHandler the handler
	 * @param headers      the headers
	 * @param id           the id
	 * @param payload      the payload
	 * @return the response
	 */
	protected Response doBadPatch(String paramHandler, Map<String, String> headers, Long id, Object payload) {
		return doBadPatch(paramHandler, headers, getIdPathParams(id), payload);
	}

	/**
	 * Do delete.
	 *
	 * @param paramHandler the handler
	 * @param headers      the headers
	 * @param pathParams   the path params
	 * @return the response
	 */
	protected Response doDelete(String paramHandler, Map<String, String> headers, Map<String, String> pathParams) {
		return delete(paramHandler, headers, null, pathParams).then().statusCode(HttpStatus.OK.value()).extract()
				.response();
	}

	/**
	 * Do delete.
	 *
	 * @param paramHandler the handler
	 * @param headers      the headers
	 * @param id           the id
	 * @return the response
	 */
	protected Response doDelete(String paramHandler, Map<String, String> headers, Long id) {
		return doDelete(paramHandler, headers, getIdPathParams(id));
	}

	private Response doCreate(String handler, Map<String, String> headers, Object payload) {
		return post(handler, payload, headers, null, null).then().statusCode(HttpStatus.CREATED.value()).extract()
				.response();
	}

	@SuppressWarnings("unchecked")
	protected void generateSwagger(String module, String prefix, String host) {
		final Response response = get("/v2/api-docs", null, null, null).then().statusCode(HttpStatus.OK.value())
				.extract().response();
		final String responseString = response.body().asString();
		try {
			final ObjectMapper mapper = new ObjectMapper();
			final Map<String, Object> swaggerObj = mapper.readValue(responseString, Map.class);
			swaggerObj.put("host", host);
			final String fileName = new StringBuilder().append(prefix + "-").append(module).append(".json").toString();
			final File file = new File(fileName);
			final PrintWriter writer = new PrintWriter(file);
			writer.print(mapper.writeValueAsString(swaggerObj));
			writer.flush();
			writer.close();
		} catch (final Exception e) {
			assertEquals(false, true);
		}
	}

}
