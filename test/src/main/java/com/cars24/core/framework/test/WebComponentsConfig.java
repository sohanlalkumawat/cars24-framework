
package com.cars24.core.framework.test;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.cars24.core.framework.common.constants.ApplicationProfiles;
import com.cars24.core.framework.common.constants.CommonConstants;

/**
 * The Class WebComponentsConfig.
 *
 * @author Sohan
 */
@Configuration
@ComponentScan(basePackages = { CommonConstants.BASE_PACKAGE })
@Profile(ApplicationProfiles.TEST)
@EnableAutoConfiguration(exclude = { DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class })
public class WebComponentsConfig {

}
